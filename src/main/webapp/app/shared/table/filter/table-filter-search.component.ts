import { Component } from '@angular/core';
import { TableFilter } from 'app/shared/table/filter/table-filter';

@Component({
    selector: 'table-filter-search',
    templateUrl: 'table-filter-search.component.html',
    styleUrls: ['table-filter-line.css']
})
export class TableFilterSearchComponent extends TableFilter {
    isSearching: boolean;

    getModelChangedCallback = () => {
        this.modelChangedCallback();
    };

    modelChangedCallback() {
        this.filterChanged();
    }

    toggleSearch() {
        this.isSearching = !this.isSearching;
        if (!this.isSearching) {
            this.filterModel.props.value = '';
            this.filterChanged();
        }
    }
}
