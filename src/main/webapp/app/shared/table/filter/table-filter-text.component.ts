import { Component } from '@angular/core';
import { TableFilter } from 'app/shared/table/filter/table-filter';

@Component({
    selector: 'table-filter-text',
    templateUrl: 'table-filter-text.component.html',
    styleUrls: ['table-filter-line.css']
})
export class TableFilterTextComponent extends TableFilter {
    getModelChangedCallback = () => {
        this.modelChangedCallback();
    };

    modelChangedCallback() {
        this.filterChanged();
    }
}
