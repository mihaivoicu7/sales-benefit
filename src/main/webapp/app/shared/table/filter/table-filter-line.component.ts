import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { IFilterTypeModel } from 'app/shared/model/filter-type.model';
import { FilterTypeEnum } from 'app/shared/model/enums/filter-type.enum';

const defaultProps = {
    value: null,
    from: null,
    to: null,
    fromDate: null,
    toDate: null,
    checked: false
};

@Component({
    selector: 'table-filter',
    templateUrl: 'table-filter-line.component.html',
    styleUrls: ['table-filter-line.css']
})
export class TableFilterLineComponent implements OnInit {
    isSearching: boolean;
    filterTypeEnum = FilterTypeEnum;
    searchFilter: IFilterTypeModel;
    filterArray = new Array<IFilterTypeModel>();
    @Input() inputFilterArray: Array<IFilterTypeModel>;
    @Output() filterChangedEmitter = new EventEmitter<IFilterTypeModel>();

    ngOnInit(): void {
        if (this.inputFilterArray) {
            this.searchFilter = this.inputFilterArray.find(filter => FilterTypeEnum.Search === filter.type);
            this.searchFilter.props = { ...defaultProps };
            this.inputFilterArray
                .filter(filter => FilterTypeEnum.Search !== filter.type)
                .forEach(filter => {
                    filter.props = { ...defaultProps };
                    this.filterArray.push(filter);
                });
        }
    }

    filterValueChanged(filterModel) {
        this.filterChangedEmitter.emit(filterModel);
    }

    filterChecked(filterModel) {
        if (!filterModel.checked) {
            filterModel.props = { ...defaultProps };
            this.filterChangedEmitter.emit(filterModel);
        }
    }
}
