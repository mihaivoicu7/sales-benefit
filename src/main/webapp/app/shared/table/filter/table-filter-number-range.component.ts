import { Component, OnInit } from '@angular/core';
import { TableFilter } from 'app/shared/table/filter/table-filter';

@Component({
    selector: 'table-filter-number-range',
    templateUrl: 'table-filter-number-range.component.html',
    styleUrls: ['table-filter-line.css', 'table-filter-number-range.css']
})
export class TableFilterNumberRangeComponent extends TableFilter implements OnInit {
    tooltipFrom: string;
    tooltipTo: string;

    ngOnInit() {
        this.tooltipFrom = this.filterModel.tooltip + ' de la';
        this.tooltipTo = this.filterModel.tooltip + ' pana la';
    }

    getModelChangedCallback = () => {
        this.modelChangedCallback();
    };

    modelChangedCallback() {
        if (this.filterModel.props.from && this.filterModel.props.from < 0) {
            this.filterModel.props.from = 0;
        }
        if (this.filterModel.props.to && this.filterModel.props.from && this.filterModel.props.to <= this.filterModel.props.from) {
            this.filterModel.props.to = this.filterModel.props.from;
        }
        this.filterChanged();
    }
}
