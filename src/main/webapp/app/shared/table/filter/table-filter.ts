import { EventEmitter, Input, Output } from '@angular/core';
import { IFilterTypeModel } from 'app/shared/model/filter-type.model';

export class TableFilter {
    wasEnterPressed: boolean;
    @Input() filterModel: IFilterTypeModel;
    @Output() filterChangedEmitter = new EventEmitter<IFilterTypeModel>();

    private timeoutValue = 2000;
    private currentTimeoutMillis = new Date().getTime();

    filterChanged() {
        this.filterChangedEmitter.emit(this.filterModel);
    }

    enterPressed(callback: () => void) {
        this.wasEnterPressed = true;
        callback();
    }

    modelChanged(callback: () => void) {
        this.currentTimeoutMillis = new Date().getTime();
        this.wasEnterPressed = false;
        setTimeout(() => {
            const waitMillis = this.currentTimeoutMillis + this.timeoutValue;
            const currentMillis = new Date().getTime();
            if (currentMillis >= waitMillis && !this.wasEnterPressed) {
                callback();
            }
        }, this.timeoutValue);
    }
}
