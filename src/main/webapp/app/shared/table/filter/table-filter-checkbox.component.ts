import { Component } from '@angular/core';
import { TableFilter } from 'app/shared/table/filter/table-filter';

@Component({
    selector: 'table-filter-checkbox',
    templateUrl: 'table-filter-checkbox.component.html',
    styleUrls: ['table-filter-line.css']
})
export class TableFilterCheckboxComponent extends TableFilter {
    checkboxClicked() {
        this.filterChanged();
    }
}
