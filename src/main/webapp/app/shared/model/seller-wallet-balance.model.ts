export interface ISellerWalletBalance {
    availableAmount?: number;
    inPendingAmount?: number;
}

export class SellerWalletBallance implements ISellerWalletBalance {
    constructor(public availableAmount?: number, public inPendingAmount?: number) {}
}
