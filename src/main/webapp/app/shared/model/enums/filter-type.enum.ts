export enum FilterTypeEnum {
    Checkbox,
    Number,
    Text,
    Date,
    Search
}
