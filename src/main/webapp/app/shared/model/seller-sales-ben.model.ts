export interface ISellerSalesBen {
    id?: number;
    isActive?: boolean;
    description?: string;
    userId?: number;
}

export class SellerSalesBen implements ISellerSalesBen {
    constructor(public id?: number, public isActive?: boolean, public description?: string, public userId?: number) {
        this.isActive = this.isActive || false;
    }
}
