export interface ISupplierCampaignProductModel {
    id?: number;
    price?: number;
    isActive?: boolean;
    discount?: number;
    campaignId?: number;
    productId?: number;
    productName?: string;
    productDescription?: string;
    isInCampaign?: boolean;
    isInEditMode?: boolean;
}

export class SupplierCampaignProductModel implements ISupplierCampaignProductModel {
    constructor(
        public id?: number,
        public price?: number,
        public isActive?: boolean,
        public discount?: number,
        public campaignId?: number,
        public productId?: number,
        public productName?: string,
        public productDescription?: string,
        public isInCampaign?: boolean,
        public isInEditMode?: boolean
    ) {
        isInCampaign = campaignId !== null;
        isInEditMode = false;
    }
}
