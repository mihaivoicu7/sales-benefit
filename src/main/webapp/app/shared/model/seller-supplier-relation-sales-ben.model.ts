export interface ISellerSupplierRelationSalesBen {
    id?: number;
    supplierId?: number;
    sellerId?: number;
}

export class SellerSupplierRelationSalesBen implements ISellerSupplierRelationSalesBen {
    constructor(public id?: number, public supplierId?: number, public sellerId?: number) {}
}
