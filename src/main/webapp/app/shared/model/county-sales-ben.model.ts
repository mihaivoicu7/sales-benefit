export interface ICountySalesBen {
    id?: number;
    name?: string;
}

export class CountySalesBen implements ICountySalesBen {
    constructor(public id?: number, public name?: string) {}
}
