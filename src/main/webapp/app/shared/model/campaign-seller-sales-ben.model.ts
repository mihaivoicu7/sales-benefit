export interface ICampaignSellerSalesBen {
    id?: number;
    campaignId?: number;
    sellerId?: number;
}

export class CampaignSellerSalesBen implements ICampaignSellerSalesBen {
    constructor(public id?: number, public campaignId?: number, public sellerId?: number) {}
}
