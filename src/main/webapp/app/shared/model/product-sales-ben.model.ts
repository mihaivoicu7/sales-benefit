import { Moment } from 'moment';

export interface IProductSalesBen {
    id?: number;
    name?: string;
    code?: string;
    price?: number;
    description?: string;
    isActive?: boolean;
    createDate?: Moment;
    updateDate?: Moment;
    supplierId?: number;
}

export class ProductSalesBen implements IProductSalesBen {
    constructor(
        public id?: number,
        public name?: string,
        public code?: string,
        public price?: number,
        public description?: string,
        public isActive?: boolean,
        public createDate?: Moment,
        public updateDate?: Moment,
        public supplierId?: number
    ) {
        this.isActive = this.isActive || false;
    }
}
