export interface IUserNotificationModel {
    id?: number;
    title?: string;
    description?: string;
    type?: string;
    hasBeenRead?: boolean;
    createDate?: Date;
    userId?: number;
}

export class UserNotificationModel implements IUserNotificationModel {
    constructor(
        public id?: number,
        public title?: string,
        public description?: string,
        public type?: string,
        public hasBeenRead?: boolean,
        public createDate?: Date,
        public userId?: number
    ) {}
}
