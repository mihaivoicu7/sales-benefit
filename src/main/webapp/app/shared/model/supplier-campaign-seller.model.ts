export interface ISupplierCampaignSellerModel {
    id?: number;
    campaignId?: number;
    sellerId?: number;
    isActive?: boolean;
    description?: string;
    firstName?: string;
    lastName?: string;
    email?: string;
    imageUrl?: string;
    isInCampaign?: boolean;
    isInEditMode?: boolean;
}

export class SupplierCampaignSellerModel implements ISupplierCampaignSellerModel {
    constructor(
        public id?: number,
        public campaignId?: number,
        public sellerId?: number,
        public isActive?: boolean,
        public description?: string,
        public firstName?: string,
        public lastName?: string,
        public email?: string,
        public imageUrl?: string,
        public isInCampaign?: boolean,
        public isInEditMode?: boolean
    ) {}
}
