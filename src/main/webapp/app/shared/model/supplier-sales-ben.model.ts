export interface ISupplierSalesBen {
    id?: number;
    companyName?: string;
    companyCode?: string;
    description?: string;
    userId?: number;
}

export class SupplierSalesBen implements ISupplierSalesBen {
    constructor(
        public id?: number,
        public companyName?: string,
        public companyCode?: string,
        public description?: string,
        public userId?: number
    ) {}
}
