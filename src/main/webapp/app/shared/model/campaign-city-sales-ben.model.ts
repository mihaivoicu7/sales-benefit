export interface ICampaignCitySalesBen {
    id?: number;
    camapignId?: number;
    cityId?: number;
}

export class CampaignCitySalesBen implements ICampaignCitySalesBen {
    constructor(public id?: number, public camapignId?: number, public cityId?: number) {}
}
