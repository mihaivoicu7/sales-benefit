export interface ISellerCitySalesBen {
    id?: number;
    sellerId?: number;
    cityId?: number;
}

export class SellerCitySalesBen implements ISellerCitySalesBen {
    constructor(public id?: number, public sellerId?: number, public cityId?: number) {}
}
