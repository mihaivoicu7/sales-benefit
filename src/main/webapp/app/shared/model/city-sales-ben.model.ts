export interface ICitySalesBen {
    id?: number;
    name?: string;
}

export class CitySalesBen implements ICitySalesBen {
    constructor(public id?: number, public name?: string) {}
}
