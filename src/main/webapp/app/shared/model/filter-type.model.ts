import { FilterTypeEnum } from 'app/shared/model/enums/filter-type.enum';

export interface IFilterTypeModel {
    type?: FilterTypeEnum;
    name?: string;
    tooltip?: string;
    value?: string;
    placeholder?: string;
    checked?: boolean;
    props?: any;
}

export class FilterTypeModel implements IFilterTypeModel {
    constructor(
        type?: FilterTypeEnum,
        name?: string,
        tooltip?: string,
        value?: string,
        placeholder?: string,
        checked = false,
        props = {}
    ) {}
}
