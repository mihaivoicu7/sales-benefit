export interface ISellerCountySalesBen {
    id?: number;
    sellerId?: number;
    countyId?: number;
}

export class SellerCountySalesBen implements ISellerCountySalesBen {
    constructor(public id?: number, public sellerId?: number, public countyId?: number) {}
}
