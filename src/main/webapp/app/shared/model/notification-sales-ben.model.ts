import { Moment } from 'moment';

export interface INotificationSalesBen {
    id?: number;
    title?: string;
    description?: string;
    type?: string;
    hasBeenRead?: boolean;
    createDate?: Moment;
    userId?: number;
}

export class NotificationSalesBen implements INotificationSalesBen {
    constructor(
        public id?: number,
        public title?: string,
        public description?: string,
        public type?: string,
        public hasBeenRead?: boolean,
        public createDate?: Moment,
        public userId?: number
    ) {
        this.hasBeenRead = this.hasBeenRead || false;
    }
}
