export interface ICampaignCountySalesBen {
    id?: number;
    campaignId?: number;
    countyId?: number;
}

export class CampaignCountySalesBen implements ICampaignCountySalesBen {
    constructor(public id?: number, public campaignId?: number, public countyId?: number) {}
}
