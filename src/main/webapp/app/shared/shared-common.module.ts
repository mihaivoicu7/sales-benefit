import { NgModule } from '@angular/core';

import { SalesBenefitSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent } from './';
import { DateBiggerThanValidator } from './validator/datebigger.directive';
import { ChipsAutocomplete } from './core/chips/chips-autocomplete.component';
import { TableFilterLineComponent } from 'app/shared/table/filter/table-filter-line.component';
import { TableFilterDateRangeComponent } from 'app/shared/table/filter/table-filter-date-range.component';
import { TableFilterNumberRangeComponent } from 'app/shared/table/filter/table-filter-number-range.component';
import { TableFilterTextComponent } from 'app/shared/table/filter/table-filter-text.component';
import { TableFilterSearchComponent } from 'app/shared/table/filter/table-filter-search.component';
import { TableFilterCheckboxComponent } from 'app/shared/table/filter/table-filter-checkbox.component';

@NgModule({
    imports: [SalesBenefitSharedLibsModule],
    declarations: [
        JhiAlertComponent,
        JhiAlertErrorComponent,
        DateBiggerThanValidator,
        ChipsAutocomplete,
        TableFilterLineComponent,
        TableFilterDateRangeComponent,
        TableFilterNumberRangeComponent,
        TableFilterTextComponent,
        TableFilterSearchComponent,
        TableFilterCheckboxComponent
    ],
    exports: [
        SalesBenefitSharedLibsModule,
        JhiAlertComponent,
        JhiAlertErrorComponent,
        DateBiggerThanValidator,
        ChipsAutocomplete,
        TableFilterLineComponent,
        TableFilterDateRangeComponent,
        TableFilterNumberRangeComponent,
        TableFilterTextComponent,
        TableFilterSearchComponent,
        TableFilterCheckboxComponent
    ]
})
export class SalesBenefitSharedCommonModule {}
