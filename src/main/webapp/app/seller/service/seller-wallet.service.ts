import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { ISellerWalletSalesBen } from 'app/shared/model/seller-wallet-sales-ben.model';
import {ISellerWalletBalance} from "app/shared/model/seller-wallet-balance.model";

type EntityResponseType = HttpResponse<ISellerWalletSalesBen>;

@Injectable({ providedIn: 'root' })
export class SellerWalletService {
    private resourceUrl = SERVER_API_URL + 'api/seller/wallet';

    constructor(private http: HttpClient) {}

    getWalletBalance(): Observable<EntityResponseType> {
        return this.http.get<ISellerWalletBalance>(`${this.resourceUrl}/balance`, { observe: 'response' });
    }

}
