import {Injectable} from '@angular/core';
import {HttpResponse} from '@angular/common/http';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot, Routes} from '@angular/router';
import {UserRouteAccessService} from 'app/core';
import {map} from 'rxjs/operators';
import {SellerWalletComponent} from './seller-wallet.component';
import {ISellerWalletBalance} from "app/shared/model/seller-wallet-balance.model";
import {SellerWalletService} from "app/seller/service/seller-wallet.service";

@Injectable({ providedIn: 'root' })
export class SellerWalletResolve implements Resolve<ISellerWalletBalance> {
    constructor(private service: SellerWalletService) {}
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        return this.service.getWalletBalance().pipe(map((sellerWalletBalance: HttpResponse<ISellerWalletBalance>) => sellerWalletBalance.body));
    }
}

export const sellerWalletRoute: Routes = [
    {
        path: 'seller-wallet',
        component: SellerWalletComponent,
        resolve: {
            sellerWalletBalance: SellerWalletResolve
        },
        data: {
            authorities: ['ROLE_SELLER'],
            pageTitle: 'Punctele Tale'
        },
        canActivate: [UserRouteAccessService]
    }
];
