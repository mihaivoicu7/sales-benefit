import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {PreviousState} from 'app/common/previous-state';
import {SupplierCampaignProductService} from "app/supplier/service/supplier-campaign-product.service";
import {ISellerWalletBalance} from "app/shared/model/seller-wallet-balance.model";

@Component({
    selector: 'jhi-seller-wallet',
    templateUrl: './seller-wallet.component.html',
    styleUrls: ['./seller-wallet.component.css']
})
export class SellerWalletComponent extends PreviousState implements OnInit {
    sellerWalletBalance: ISellerWalletBalance;
    constructor(private activatedRoute: ActivatedRoute) {
        super();
    }

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ sellerWalletBalance }) => {
            this.sellerWalletBalance = sellerWalletBalance;
        });
    }

}
