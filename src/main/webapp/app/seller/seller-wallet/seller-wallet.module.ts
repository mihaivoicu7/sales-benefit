import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SalesBenefitSharedModule } from 'app/shared';
import {
    SellerWalletComponent,
} from './';
import {sellerWalletRoute} from "app/seller/seller-wallet/seller-wallet.route";
import {SellerWalletService} from "app/seller/service/seller-wallet.service";

const ENTITY_STATES = [...sellerWalletRoute];

@NgModule({
    imports: [SalesBenefitSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        SellerWalletComponent
    ],
    entryComponents: [
        SellerWalletComponent
    ],
    providers : [
        SellerWalletService
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SellerWalletModule {}
