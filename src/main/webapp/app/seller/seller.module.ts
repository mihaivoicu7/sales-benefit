import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {SellerWalletModule} from "app/seller/seller-wallet/seller-wallet.module";

@NgModule({
    imports: [
        SellerWalletModule
    ],
    declarations: [],
    entryComponents: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SellerModule {

}
