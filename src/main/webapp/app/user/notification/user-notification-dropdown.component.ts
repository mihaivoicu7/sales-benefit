import { Component } from '@angular/core';
import { JhiAlertService } from 'ng-jhipster';
import { UserNotificationWebsocketService } from 'app/user/service/user-notification-websocket.service';
import { Router } from '@angular/router';
import { UserNotificationBase } from './user-notification-base';
import { UserNotificationModel } from 'app/shared/model/user-notification.model';

@Component({
    selector: 'jhi-user-notification-dropdown',
    templateUrl: './user-notification-dropdown.component.html',
    styleUrls: ['./style.css']
})
export class UserNotificationDropdownComponent extends UserNotificationBase {
    constructor(
        protected userNotificationWebsocketService: UserNotificationWebsocketService,
        protected jhiAlertService: JhiAlertService,
        protected router: Router
    ) {
        super(userNotificationWebsocketService, jhiAlertService, router);
    }
}
