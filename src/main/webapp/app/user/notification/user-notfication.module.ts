import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { UserNotificationComponent } from './user-notification.component';
import { UserNotificationDropdownComponent } from './user-notification-dropdown.component';
import { SalesBenefitSharedModule } from 'app/shared/shared.module';
import { RouterModule } from '@angular/router';
import { userNotificationRoute } from './user-notification.route';
import { UserNotificationDetailComponent } from './user-notification-detail.component';

@NgModule({
    imports: [SalesBenefitSharedModule, RouterModule.forChild(userNotificationRoute)],
    declarations: [UserNotificationDropdownComponent, UserNotificationComponent, UserNotificationDetailComponent],
    entryComponents: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA],
    exports: [UserNotificationDropdownComponent]
})
export class UserNotificationModule {}
