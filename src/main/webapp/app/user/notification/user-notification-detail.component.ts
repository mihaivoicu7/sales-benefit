import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { UserNotificationModel } from 'app/shared/model/user-notification.model';

@Component({
    selector: 'jhi-user-notification-detail',
    templateUrl: './user-notification-detail.component.html'
})
export class UserNotificationDetailComponent implements OnInit {
    notification: UserNotificationModel;

    constructor(protected activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ notification }) => {
            this.notification = notification;
        });
    }

    previousState() {
        window.history.back();
    }
}
