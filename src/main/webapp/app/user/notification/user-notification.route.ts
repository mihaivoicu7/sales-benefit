import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { IUserNotificationModel, UserNotificationModel } from 'app/shared/model/user-notification.model';
import { UserNotificationService } from 'app/user/service/user-notification.service';
import { UserNotificationComponent } from './user-notification.component';
import { UserNotificationDetailComponent } from './user-notification-detail.component';
import { filter } from 'rxjs/internal/operators';

@Injectable({ providedIn: 'root' })
export class UserNotificationResolve implements Resolve<IUserNotificationModel> {
    constructor(private notificationService: UserNotificationService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const id = route.params['notificationId'] ? route.params['notificationId'] : null;
        if (id) {
            return this.notificationService.find(id).pipe(
                filter((response: HttpResponse<UserNotificationModel>) => response.ok),
                map((notification: HttpResponse<UserNotificationModel>) => notification.body)
            );
        }
        return of(new UserNotificationModel());
    }
}

export const userNotificationRoute: Routes = [
    {
        path: 'user-notification',
        component: UserNotificationComponent,
        resolve: {
            pagingParams: JhiResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            defaultSort: 'createDate,desc',
            pageTitle: 'Notifications'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'user-notification/:notificationId',
        component: UserNotificationDetailComponent,
        resolve: {
            pagingParams: JhiResolvePagingParams,
            notification: UserNotificationResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Notification'
        },
        canActivate: [UserRouteAccessService]
    }
];
