import { Component, OnInit } from '@angular/core';
import { JhiAlertService } from 'ng-jhipster';

import { UserNotificationService } from 'app/user/service/user-notification.service';
import { ActivatedRoute, Router } from '@angular/router';
import { UserNotificationBase } from './user-notification-base';
import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { UserNotificationModel } from 'app/shared/model/user-notification.model';
import { UserNotificationWebsocketService } from 'app/user/service/user-notification-websocket.service';

@Component({
    selector: 'jhi-user-notification',
    templateUrl: './user-notification.component.html'
})
export class UserNotificationComponent extends UserNotificationBase implements OnInit {
    itemsPerPage: any;
    page: any;
    totalItems: any;
    notifications: UserNotificationModel[];

    constructor(
        protected userNotificationService: UserNotificationService,
        protected userNotificationWebsocketService: UserNotificationWebsocketService,
        protected jhiAlertService: JhiAlertService,
        protected router: Router,
        private activatedRoute: ActivatedRoute
    ) {
        super(userNotificationWebsocketService, jhiAlertService, router);

        this.itemsPerPage = ITEMS_PER_PAGE;
        this.activatedRoute.data.subscribe(data => {
            // this.page = data.pagingParams.page;
            this.setupPage(data.pagingParams.page);
        });
    }

    ngOnInit() {
        super.ngOnInit();

        this.userNotificationService
            .countAllNotifications()
            .subscribe((res: HttpResponse<any>) => (this.totalItems = res.body), (res: HttpErrorResponse) => this.onError(res.message));
        this.loadPage(this.page);
    }

    setupPage(page: number) {
        this.page = page;
    }

    loadPage(page: any) {
        this.userNotificationService
            .findAllByUser({
                page: this.page - 1,
                size: this.itemsPerPage
            })
            .subscribe((res: HttpResponse<any>) => (this.notifications = res.body), (res: HttpErrorResponse) => this.onError(res.message));
    }

    previousState() {
        window.history.back();
    }
}
