import { OnDestroy, OnInit } from '@angular/core';
import { JhiAlertService } from 'ng-jhipster';

import { UserNotificationModel } from 'app/shared/model/user-notification.model';
import { VISIBLE_NOTIFICATION_DESCRIPTION_CHARACTERS } from 'app/shared/constants/pagination.constants';
import { Router } from '@angular/router';
import { UserNotificationWebsocketService } from 'app/user/service/user-notification-websocket.service';

export class UserNotificationBase implements OnInit, OnDestroy {
    public constructor(
        protected userNotificationWebsocketService: UserNotificationWebsocketService,
        protected jhiAlertService: JhiAlertService,
        protected router: Router
    ) {}

    ngOnInit() {}

    ngOnDestroy() {}

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    private trimDescription(description: string) {
        if (description.length > VISIBLE_NOTIFICATION_DESCRIPTION_CHARACTERS) {
            return description.slice(0, VISIBLE_NOTIFICATION_DESCRIPTION_CHARACTERS) + '[...]';
        }

        return description;
    }

    private notificationClick(notification: UserNotificationModel) {
        if (notification.hasBeenRead === false) {
            notification.hasBeenRead = true;
            this.userNotificationWebsocketService.updateNotification(notification);
        }

        this.router.navigate(['/user-notification', notification.id]);
    }
}
