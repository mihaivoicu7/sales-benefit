import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IUserNotificationModel, UserNotificationModel } from 'app/shared/model/user-notification.model';

type EntityResponseType = HttpResponse<IUserNotificationModel>;
type EntityArrayResponseType = HttpResponse<IUserNotificationModel[]>;

@Injectable({ providedIn: 'root' })
export class UserNotificationService {
    private resourceUrl = SERVER_API_URL + 'api/user/notifications';

    constructor(private http: HttpClient) {}

    create(userNotification: UserNotificationModel): Observable<EntityResponseType> {
        return this.http.post<IUserNotificationModel>(this.resourceUrl, userNotification, { observe: 'response' });
    }

    update(userNotification: UserNotificationModel): Observable<EntityResponseType> {
        return this.http.put<IUserNotificationModel>(this.resourceUrl, userNotification, { observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<IUserNotificationModel>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    findAllByUser(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IUserNotificationModel[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    countAllNotifications(req?: any): Observable<HttpResponse<number>> {
        const options = createRequestOption(req);
        return this.http.get<number>(this.resourceUrl + '/count', { params: options, observe: 'response' });
    }

    countUnreadNotifications(req?: any): Observable<HttpResponse<number>> {
        const options = createRequestOption(req);
        return this.http.get<number>(this.resourceUrl + '/countUnread', { params: options, observe: 'response' });
    }
}
