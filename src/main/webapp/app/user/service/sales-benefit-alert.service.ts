import { Injectable } from '@angular/core';
import { Observable, Observer } from 'rxjs/index';

@Injectable({ providedIn: 'root' })
export class SalesBenefitAlertService {
    private listener: Observable<any>;
    private listenerObserver: Observer<any>;

    constructor() {
        this.listener = new Observable(observer => {
            this.listenerObserver = observer;
        });
    }

    public showAlert(textAlert: string, callback?: () => any) {
        this.listenerObserver.next([textAlert, callback]);
    }

    public getAlert(): Observable<any> {
        return this.listener;
    }
}
