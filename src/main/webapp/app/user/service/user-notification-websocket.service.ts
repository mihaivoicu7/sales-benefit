import { Injectable } from '@angular/core';

import { AuthServerProvider } from 'app/core/auth/auth-jwt.service';

import * as SockJS from 'sockjs-client';
import * as Stomp from 'webstomp-client';
import { WindowRef } from 'app/core/window/window.service';
import { UserNotificationModel } from 'app/shared/model/user-notification.model';
import { VISIBLE_NOTIFICATION_DESCRIPTION_CHARACTERS, VISIBLE_NOTIFICATIONS_IN_DROPDOWN } from 'app/shared/constants/pagination.constants';
import { Router } from '@angular/router';
import { SalesBenefitAlertService } from './sales-benefit-alert.service';

@Injectable({ providedIn: 'root' })
export class UserNotificationWebsocketService {
    stompClient = null;
    subscriberList = [];
    connection: Promise<any>;
    connectedPromise: any;

    unreadNotificationsCounter = null;
    notifications: UserNotificationModel[];

    constructor(
        private authServerProvider: AuthServerProvider,
        private $window: WindowRef,
        protected router: Router,
        protected salesBenefitAlertService: SalesBenefitAlertService
    ) {
        this.connection = this.createConnection();
    }

    connect() {
        if (this.connectedPromise === null) {
            this.connection = this.createConnection();
        }
        // building absolute path so that websocket doesn't fail when deploying with a context path
        const loc = this.$window.nativeWindow.location;
        let url;
        url = '//' + loc.host + loc.pathname + 'websocket/userNotifications';
        const authToken = this.authServerProvider.getToken();
        if (authToken) {
            url += '?access_token=' + authToken;
        }
        const socket = new SockJS(url);
        this.stompClient = Stomp.over(socket);
        const headers = {};
        this.stompClient.connect(
            headers,
            () => {
                this.connectedPromise('success');
                this.connectedPromise = null;
            }
        );

        this.connection.then(() => {
            this.subscribe();
            this.callMandatoryRequests();
        });
    }

    disconnect() {
        if (this.isConnected()) {
            this.unsubscribe();
            this.stompClient.disconnect();
            this.stompClient = null;
        }
    }

    updateNotification(notification: UserNotificationModel) {
        this.sendRequestToBackend('/topic/userNotifications/update', { size: VISIBLE_NOTIFICATIONS_IN_DROPDOWN }, notification);
    }

    private isConnected(): boolean {
        return this.stompClient && this.stompClient.connected;
    }

    private callMandatoryRequests() {
        this.sendRequestToBackend('/topic/userNotifications/getAll', { size: VISIBLE_NOTIFICATIONS_IN_DROPDOWN });
        this.sendRequestToBackend('/topic/userNotifications/getUnreadCount');
    }

    private sendRequestToBackend(route: string, header?: any, data?: any) {
        if (this.isConnected()) {
            this.stompClient.send(
                route, // destination
                JSON.stringify(data), // body
                header // header h
            );
        }
    }

    private subscribe() {
        this.connection.then(() => {
            this.subscriberList.push(
                this.stompClient.subscribe('/user/queue/userNotifications/sendAll', data => {
                    this.notifications = JSON.parse(data.body);
                })
            );
            this.subscriberList.push(
                this.stompClient.subscribe('/user/queue/userNotifications/sendUnreadCount', data => {
                    this.unreadNotificationsCounter = JSON.parse(data.body);
                })
            );
            this.subscriberList.push(
                this.stompClient.subscribe('/user/queue/userNotifications/sendNewNotification', data => {
                    this.processNewNotification(JSON.parse(data.body));
                })
            );
        });
    }

    private unsubscribe() {
        this.subscriberList.forEach(subscriber => {
            if (subscriber !== null) {
                subscriber.unsubscribe();
            }
        });
        this.subscriberList.splice(0, this.subscriberList.length);
    }

    private createConnection(): Promise<any> {
        return new Promise((resolve, reject) => (this.connectedPromise = resolve));
    }

    private processNewNotification(notification: UserNotificationModel) {
        const alertText =
            'New notification! ' +
            notification.type +
            ' -- ' +
            notification.title +
            ' -- ' +
            (notification.description.length > VISIBLE_NOTIFICATION_DESCRIPTION_CHARACTERS
                ? notification.description.slice(0, VISIBLE_NOTIFICATION_DESCRIPTION_CHARACTERS) + '[...]'
                : notification.description);

        this.salesBenefitAlertService.showAlert(alertText, () => {
            this.router.navigate(['/user-notification', notification.id]);
        });
    }
}
