import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { UserNotificationModule } from './notification/user-notfication.module';
import { UserNotificationService } from './service/user-notification.service';
import { SalesBenefitAlertModule } from './alert/sales-benefit-alert.module';

@NgModule({
    imports: [UserNotificationModule, SalesBenefitAlertModule],
    declarations: [],
    entryComponents: [],
    providers: [UserNotificationService],
    schemas: [CUSTOM_ELEMENTS_SCHEMA],
    exports: [UserNotificationModule, SalesBenefitAlertModule]
})
export class UserModule {}
