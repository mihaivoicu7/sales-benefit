import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { SalesBenefitSharedModule } from 'app/shared/shared.module';
import { SalesBenefitAlertComponent } from 'app/user/alert/sales-benefit-alert.component';

@NgModule({
    imports: [SalesBenefitSharedModule],
    declarations: [SalesBenefitAlertComponent],
    entryComponents: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA],
    exports: [SalesBenefitAlertComponent]
})
export class SalesBenefitAlertModule {}
