import { Component, OnDestroy, OnInit } from '@angular/core';
import { SalesBenefitAlertService } from 'app/user/service/sales-benefit-alert.service';
import { ALERT_TIMEOUT_IN_MS } from 'app/shared/constants/pagination.constants';

@Component({
    selector: 'jhi-sales-benefit-alert',
    templateUrl: './sales-benefit-alert.component.html'
})
export class SalesBenefitAlertComponent implements OnInit, OnDestroy {
    private alerts: [string, () => any][];

    constructor(protected salesBenefitAlertService: SalesBenefitAlertService) {
        this.alerts = [];
    }

    ngOnInit() {
        this.salesBenefitAlertService.getAlert().subscribe(value => this.showAlert(value));
    }

    ngOnDestroy() {}

    private showAlert(newAlert: [string, () => any]) {
        this.alerts.push(newAlert);
        setTimeout(() => {
            this.removeAlert(newAlert);
        }, ALERT_TIMEOUT_IN_MS);
    }

    private alertClick(alert: [string, () => any]) {
        if (alert[1]) {
            alert[1]();
        }
        this.removeAlert(alert);
    }

    private removeAlert(alertToBeRemoved: [string, () => any]) {
        const index = this.alerts.indexOf(alertToBeRemoved);
        if (index > -1) {
            this.alerts.splice(index, 1);
        }
    }
}
