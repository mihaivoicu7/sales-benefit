import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ISellerSalesBen } from 'app/shared/model/seller-sales-ben.model';
import { AccountService } from 'app/core';
import { SellerSalesBenService } from './seller-sales-ben.service';

@Component({
    selector: 'jhi-seller-sales-ben',
    templateUrl: './seller-sales-ben.component.html'
})
export class SellerSalesBenComponent implements OnInit, OnDestroy {
    sellers: ISellerSalesBen[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        protected sellerService: SellerSalesBenService,
        protected jhiAlertService: JhiAlertService,
        protected eventManager: JhiEventManager,
        protected accountService: AccountService
    ) {}

    loadAll() {
        this.sellerService
            .query()
            .pipe(
                filter((res: HttpResponse<ISellerSalesBen[]>) => res.ok),
                map((res: HttpResponse<ISellerSalesBen[]>) => res.body)
            )
            .subscribe(
                (res: ISellerSalesBen[]) => {
                    this.sellers = res;
                },
                (res: HttpErrorResponse) => this.onError(res.message)
            );
    }

    ngOnInit() {
        this.loadAll();
        this.accountService.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInSellers();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: ISellerSalesBen) {
        return item.id;
    }

    registerChangeInSellers() {
        this.eventSubscriber = this.eventManager.subscribe('sellerListModification', response => this.loadAll());
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
