import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { JhiAlertService } from 'ng-jhipster';
import { IProductSalesBen } from 'app/shared/model/product-sales-ben.model';
import { ProductSalesBenService } from './product-sales-ben.service';
import { ISupplierSalesBen } from 'app/shared/model/supplier-sales-ben.model';
import { SupplierSalesBenService } from 'app/entities/supplier-sales-ben';

@Component({
    selector: 'jhi-product-sales-ben-update',
    templateUrl: './product-sales-ben-update.component.html'
})
export class ProductSalesBenUpdateComponent implements OnInit {
    product: IProductSalesBen;
    isSaving: boolean;

    suppliers: ISupplierSalesBen[];
    createDate: string;
    updateDate: string;

    constructor(
        protected jhiAlertService: JhiAlertService,
        protected productService: ProductSalesBenService,
        protected supplierService: SupplierSalesBenService,
        protected activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ product }) => {
            this.product = product;
            this.createDate = this.product.createDate != null ? this.product.createDate.format(DATE_TIME_FORMAT) : null;
            this.updateDate = this.product.updateDate != null ? this.product.updateDate.format(DATE_TIME_FORMAT) : null;
        });
        this.supplierService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<ISupplierSalesBen[]>) => mayBeOk.ok),
                map((response: HttpResponse<ISupplierSalesBen[]>) => response.body)
            )
            .subscribe((res: ISupplierSalesBen[]) => (this.suppliers = res), (res: HttpErrorResponse) => this.onError(res.message));
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        this.product.createDate = this.createDate != null ? moment(this.createDate, DATE_TIME_FORMAT) : null;
        this.product.updateDate = this.updateDate != null ? moment(this.updateDate, DATE_TIME_FORMAT) : null;
        if (this.product.id !== undefined) {
            this.subscribeToSaveResponse(this.productService.update(this.product));
        } else {
            this.subscribeToSaveResponse(this.productService.create(this.product));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<IProductSalesBen>>) {
        result.subscribe((res: HttpResponse<IProductSalesBen>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackSupplierById(index: number, item: ISupplierSalesBen) {
        return item.id;
    }
}
