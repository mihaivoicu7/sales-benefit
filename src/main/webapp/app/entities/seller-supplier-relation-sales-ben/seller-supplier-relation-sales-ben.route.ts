import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { SellerSupplierRelationSalesBen } from 'app/shared/model/seller-supplier-relation-sales-ben.model';
import { SellerSupplierRelationSalesBenService } from './seller-supplier-relation-sales-ben.service';
import { SellerSupplierRelationSalesBenComponent } from './seller-supplier-relation-sales-ben.component';
import { SellerSupplierRelationSalesBenDetailComponent } from './seller-supplier-relation-sales-ben-detail.component';
import { SellerSupplierRelationSalesBenUpdateComponent } from './seller-supplier-relation-sales-ben-update.component';
import { SellerSupplierRelationSalesBenDeletePopupComponent } from './seller-supplier-relation-sales-ben-delete-dialog.component';
import { ISellerSupplierRelationSalesBen } from 'app/shared/model/seller-supplier-relation-sales-ben.model';

@Injectable({ providedIn: 'root' })
export class SellerSupplierRelationSalesBenResolve implements Resolve<ISellerSupplierRelationSalesBen> {
    constructor(private service: SellerSupplierRelationSalesBenService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ISellerSupplierRelationSalesBen> {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(
                filter((response: HttpResponse<SellerSupplierRelationSalesBen>) => response.ok),
                map((sellerSupplierRelation: HttpResponse<SellerSupplierRelationSalesBen>) => sellerSupplierRelation.body)
            );
        }
        return of(new SellerSupplierRelationSalesBen());
    }
}

export const sellerSupplierRelationRoute: Routes = [
    {
        path: '',
        component: SellerSupplierRelationSalesBenComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'SellerSupplierRelations'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/view',
        component: SellerSupplierRelationSalesBenDetailComponent,
        resolve: {
            sellerSupplierRelation: SellerSupplierRelationSalesBenResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'SellerSupplierRelations'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'new',
        component: SellerSupplierRelationSalesBenUpdateComponent,
        resolve: {
            sellerSupplierRelation: SellerSupplierRelationSalesBenResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'SellerSupplierRelations'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/edit',
        component: SellerSupplierRelationSalesBenUpdateComponent,
        resolve: {
            sellerSupplierRelation: SellerSupplierRelationSalesBenResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'SellerSupplierRelations'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const sellerSupplierRelationPopupRoute: Routes = [
    {
        path: ':id/delete',
        component: SellerSupplierRelationSalesBenDeletePopupComponent,
        resolve: {
            sellerSupplierRelation: SellerSupplierRelationSalesBenResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'SellerSupplierRelations'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
