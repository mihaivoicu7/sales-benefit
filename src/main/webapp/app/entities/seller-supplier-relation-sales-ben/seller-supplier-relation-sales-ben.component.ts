import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ISellerSupplierRelationSalesBen } from 'app/shared/model/seller-supplier-relation-sales-ben.model';
import { AccountService } from 'app/core';
import { SellerSupplierRelationSalesBenService } from './seller-supplier-relation-sales-ben.service';

@Component({
    selector: 'jhi-seller-supplier-relation-sales-ben',
    templateUrl: './seller-supplier-relation-sales-ben.component.html'
})
export class SellerSupplierRelationSalesBenComponent implements OnInit, OnDestroy {
    sellerSupplierRelations: ISellerSupplierRelationSalesBen[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        protected sellerSupplierRelationService: SellerSupplierRelationSalesBenService,
        protected jhiAlertService: JhiAlertService,
        protected eventManager: JhiEventManager,
        protected accountService: AccountService
    ) {}

    loadAll() {
        this.sellerSupplierRelationService
            .query()
            .pipe(
                filter((res: HttpResponse<ISellerSupplierRelationSalesBen[]>) => res.ok),
                map((res: HttpResponse<ISellerSupplierRelationSalesBen[]>) => res.body)
            )
            .subscribe(
                (res: ISellerSupplierRelationSalesBen[]) => {
                    this.sellerSupplierRelations = res;
                },
                (res: HttpErrorResponse) => this.onError(res.message)
            );
    }

    ngOnInit() {
        this.loadAll();
        this.accountService.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInSellerSupplierRelations();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: ISellerSupplierRelationSalesBen) {
        return item.id;
    }

    registerChangeInSellerSupplierRelations() {
        this.eventSubscriber = this.eventManager.subscribe('sellerSupplierRelationListModification', response => this.loadAll());
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
