import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ISellerSupplierRelationSalesBen } from 'app/shared/model/seller-supplier-relation-sales-ben.model';
import { SellerSupplierRelationSalesBenService } from './seller-supplier-relation-sales-ben.service';

@Component({
    selector: 'jhi-seller-supplier-relation-sales-ben-delete-dialog',
    templateUrl: './seller-supplier-relation-sales-ben-delete-dialog.component.html'
})
export class SellerSupplierRelationSalesBenDeleteDialogComponent {
    sellerSupplierRelation: ISellerSupplierRelationSalesBen;

    constructor(
        protected sellerSupplierRelationService: SellerSupplierRelationSalesBenService,
        public activeModal: NgbActiveModal,
        protected eventManager: JhiEventManager
    ) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.sellerSupplierRelationService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'sellerSupplierRelationListModification',
                content: 'Deleted an sellerSupplierRelation'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-seller-supplier-relation-sales-ben-delete-popup',
    template: ''
})
export class SellerSupplierRelationSalesBenDeletePopupComponent implements OnInit, OnDestroy {
    protected ngbModalRef: NgbModalRef;

    constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ sellerSupplierRelation }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(SellerSupplierRelationSalesBenDeleteDialogComponent as Component, {
                    size: 'lg',
                    backdrop: 'static'
                });
                this.ngbModalRef.componentInstance.sellerSupplierRelation = sellerSupplierRelation;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate(['/seller-supplier-relation-sales-ben', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate(['/seller-supplier-relation-sales-ben', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
