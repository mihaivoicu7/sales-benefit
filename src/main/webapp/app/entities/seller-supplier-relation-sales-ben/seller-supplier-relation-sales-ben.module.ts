import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SalesBenefitSharedModule } from 'app/shared';
import {
    SellerSupplierRelationSalesBenComponent,
    SellerSupplierRelationSalesBenDetailComponent,
    SellerSupplierRelationSalesBenUpdateComponent,
    SellerSupplierRelationSalesBenDeletePopupComponent,
    SellerSupplierRelationSalesBenDeleteDialogComponent,
    sellerSupplierRelationRoute,
    sellerSupplierRelationPopupRoute
} from './';

const ENTITY_STATES = [...sellerSupplierRelationRoute, ...sellerSupplierRelationPopupRoute];

@NgModule({
    imports: [SalesBenefitSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        SellerSupplierRelationSalesBenComponent,
        SellerSupplierRelationSalesBenDetailComponent,
        SellerSupplierRelationSalesBenUpdateComponent,
        SellerSupplierRelationSalesBenDeleteDialogComponent,
        SellerSupplierRelationSalesBenDeletePopupComponent
    ],
    entryComponents: [
        SellerSupplierRelationSalesBenComponent,
        SellerSupplierRelationSalesBenUpdateComponent,
        SellerSupplierRelationSalesBenDeleteDialogComponent,
        SellerSupplierRelationSalesBenDeletePopupComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SalesBenefitSellerSupplierRelationSalesBenModule {}
