import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ISellerSupplierRelationSalesBen } from 'app/shared/model/seller-supplier-relation-sales-ben.model';

@Component({
    selector: 'jhi-seller-supplier-relation-sales-ben-detail',
    templateUrl: './seller-supplier-relation-sales-ben-detail.component.html'
})
export class SellerSupplierRelationSalesBenDetailComponent implements OnInit {
    sellerSupplierRelation: ISellerSupplierRelationSalesBen;

    constructor(protected activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ sellerSupplierRelation }) => {
            this.sellerSupplierRelation = sellerSupplierRelation;
        });
    }

    previousState() {
        window.history.back();
    }
}
