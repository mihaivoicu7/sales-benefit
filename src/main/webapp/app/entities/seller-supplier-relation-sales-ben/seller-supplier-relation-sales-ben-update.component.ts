import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { ISellerSupplierRelationSalesBen } from 'app/shared/model/seller-supplier-relation-sales-ben.model';
import { SellerSupplierRelationSalesBenService } from './seller-supplier-relation-sales-ben.service';
import { ISupplierSalesBen } from 'app/shared/model/supplier-sales-ben.model';
import { SupplierSalesBenService } from 'app/entities/supplier-sales-ben';
import { ISellerSalesBen } from 'app/shared/model/seller-sales-ben.model';
import { SellerSalesBenService } from 'app/entities/seller-sales-ben';

@Component({
    selector: 'jhi-seller-supplier-relation-sales-ben-update',
    templateUrl: './seller-supplier-relation-sales-ben-update.component.html'
})
export class SellerSupplierRelationSalesBenUpdateComponent implements OnInit {
    sellerSupplierRelation: ISellerSupplierRelationSalesBen;
    isSaving: boolean;

    suppliers: ISupplierSalesBen[];

    sellers: ISellerSalesBen[];

    constructor(
        protected jhiAlertService: JhiAlertService,
        protected sellerSupplierRelationService: SellerSupplierRelationSalesBenService,
        protected supplierService: SupplierSalesBenService,
        protected sellerService: SellerSalesBenService,
        protected activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ sellerSupplierRelation }) => {
            this.sellerSupplierRelation = sellerSupplierRelation;
        });
        this.supplierService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<ISupplierSalesBen[]>) => mayBeOk.ok),
                map((response: HttpResponse<ISupplierSalesBen[]>) => response.body)
            )
            .subscribe((res: ISupplierSalesBen[]) => (this.suppliers = res), (res: HttpErrorResponse) => this.onError(res.message));
        this.sellerService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<ISellerSalesBen[]>) => mayBeOk.ok),
                map((response: HttpResponse<ISellerSalesBen[]>) => response.body)
            )
            .subscribe((res: ISellerSalesBen[]) => (this.sellers = res), (res: HttpErrorResponse) => this.onError(res.message));
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.sellerSupplierRelation.id !== undefined) {
            this.subscribeToSaveResponse(this.sellerSupplierRelationService.update(this.sellerSupplierRelation));
        } else {
            this.subscribeToSaveResponse(this.sellerSupplierRelationService.create(this.sellerSupplierRelation));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<ISellerSupplierRelationSalesBen>>) {
        result.subscribe(
            (res: HttpResponse<ISellerSupplierRelationSalesBen>) => this.onSaveSuccess(),
            (res: HttpErrorResponse) => this.onSaveError()
        );
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackSupplierById(index: number, item: ISupplierSalesBen) {
        return item.id;
    }

    trackSellerById(index: number, item: ISellerSalesBen) {
        return item.id;
    }
}
