import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { ISellerSupplierRelationSalesBen } from 'app/shared/model/seller-supplier-relation-sales-ben.model';

type EntityResponseType = HttpResponse<ISellerSupplierRelationSalesBen>;
type EntityArrayResponseType = HttpResponse<ISellerSupplierRelationSalesBen[]>;

@Injectable({ providedIn: 'root' })
export class SellerSupplierRelationSalesBenService {
    public resourceUrl = SERVER_API_URL + 'api/seller-supplier-relations';

    constructor(protected http: HttpClient) {}

    create(sellerSupplierRelation: ISellerSupplierRelationSalesBen): Observable<EntityResponseType> {
        return this.http.post<ISellerSupplierRelationSalesBen>(this.resourceUrl, sellerSupplierRelation, { observe: 'response' });
    }

    update(sellerSupplierRelation: ISellerSupplierRelationSalesBen): Observable<EntityResponseType> {
        return this.http.put<ISellerSupplierRelationSalesBen>(this.resourceUrl, sellerSupplierRelation, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<ISellerSupplierRelationSalesBen>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<ISellerSupplierRelationSalesBen[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
}
