import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SalesBenefitSharedModule } from 'app/shared';
import {
    CountySalesBenComponent,
    CountySalesBenDetailComponent,
    CountySalesBenUpdateComponent,
    CountySalesBenDeletePopupComponent,
    CountySalesBenDeleteDialogComponent,
    countyRoute,
    countyPopupRoute
} from './';

const ENTITY_STATES = [...countyRoute, ...countyPopupRoute];

@NgModule({
    imports: [SalesBenefitSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        CountySalesBenComponent,
        CountySalesBenDetailComponent,
        CountySalesBenUpdateComponent,
        CountySalesBenDeleteDialogComponent,
        CountySalesBenDeletePopupComponent
    ],
    entryComponents: [
        CountySalesBenComponent,
        CountySalesBenUpdateComponent,
        CountySalesBenDeleteDialogComponent,
        CountySalesBenDeletePopupComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SalesBenefitCountySalesBenModule {}
