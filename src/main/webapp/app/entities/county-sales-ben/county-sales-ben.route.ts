import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { CountySalesBen } from 'app/shared/model/county-sales-ben.model';
import { CountySalesBenService } from './county-sales-ben.service';
import { CountySalesBenComponent } from './county-sales-ben.component';
import { CountySalesBenDetailComponent } from './county-sales-ben-detail.component';
import { CountySalesBenUpdateComponent } from './county-sales-ben-update.component';
import { CountySalesBenDeletePopupComponent } from './county-sales-ben-delete-dialog.component';
import { ICountySalesBen } from 'app/shared/model/county-sales-ben.model';

@Injectable({ providedIn: 'root' })
export class CountySalesBenResolve implements Resolve<ICountySalesBen> {
    constructor(private service: CountySalesBenService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<CountySalesBen> {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(
                filter((response: HttpResponse<CountySalesBen>) => response.ok),
                map((county: HttpResponse<CountySalesBen>) => county.body)
            );
        }
        return of(new CountySalesBen());
    }
}

export const countyRoute: Routes = [
    {
        path: 'county-sales-ben',
        component: CountySalesBenComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Counties'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'county-sales-ben/:id/view',
        component: CountySalesBenDetailComponent,
        resolve: {
            county: CountySalesBenResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Counties'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'county-sales-ben/new',
        component: CountySalesBenUpdateComponent,
        resolve: {
            county: CountySalesBenResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Counties'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'county-sales-ben/:id/edit',
        component: CountySalesBenUpdateComponent,
        resolve: {
            county: CountySalesBenResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Counties'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const countyPopupRoute: Routes = [
    {
        path: 'county-sales-ben/:id/delete',
        component: CountySalesBenDeletePopupComponent,
        resolve: {
            county: CountySalesBenResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Counties'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
