import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { ICountySalesBen } from 'app/shared/model/county-sales-ben.model';
import { CountySalesBenService } from './county-sales-ben.service';

@Component({
    selector: 'jhi-county-sales-ben-update',
    templateUrl: './county-sales-ben-update.component.html'
})
export class CountySalesBenUpdateComponent implements OnInit {
    county: ICountySalesBen;
    isSaving: boolean;

    constructor(protected countyService: CountySalesBenService, protected activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ county }) => {
            this.county = county;
        });
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.county.id !== undefined) {
            this.subscribeToSaveResponse(this.countyService.update(this.county));
        } else {
            this.subscribeToSaveResponse(this.countyService.create(this.county));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<ICountySalesBen>>) {
        result.subscribe((res: HttpResponse<ICountySalesBen>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }
}
