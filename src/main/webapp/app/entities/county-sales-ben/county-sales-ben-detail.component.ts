import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ICountySalesBen } from 'app/shared/model/county-sales-ben.model';

@Component({
    selector: 'jhi-county-sales-ben-detail',
    templateUrl: './county-sales-ben-detail.component.html'
})
export class CountySalesBenDetailComponent implements OnInit {
    county: ICountySalesBen;

    constructor(protected activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ county }) => {
            this.county = county;
        });
    }

    previousState() {
        window.history.back();
    }
}
