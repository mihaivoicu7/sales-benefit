import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ICountySalesBen } from 'app/shared/model/county-sales-ben.model';
import { AccountService } from 'app/core';
import { CountySalesBenService } from './county-sales-ben.service';

@Component({
    selector: 'jhi-county-sales-ben',
    templateUrl: './county-sales-ben.component.html'
})
export class CountySalesBenComponent implements OnInit, OnDestroy {
    counties: ICountySalesBen[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        protected countyService: CountySalesBenService,
        protected jhiAlertService: JhiAlertService,
        protected eventManager: JhiEventManager,
        protected accountService: AccountService
    ) {}

    loadAll() {
        this.countyService.query().subscribe(
            (res: HttpResponse<ICountySalesBen[]>) => {
                this.counties = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    ngOnInit() {
        this.loadAll();
        this.accountService.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInCounties();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: ICountySalesBen) {
        return item.id;
    }

    registerChangeInCounties() {
        this.eventSubscriber = this.eventManager.subscribe('countyListModification', response => this.loadAll());
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
