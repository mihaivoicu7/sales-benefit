import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { ICountySalesBen } from 'app/shared/model/county-sales-ben.model';

type EntityResponseType = HttpResponse<ICountySalesBen>;
type EntityArrayResponseType = HttpResponse<ICountySalesBen[]>;

@Injectable({ providedIn: 'root' })
export class CountySalesBenService {
    public resourceUrl = SERVER_API_URL + 'api/counties';

    constructor(protected http: HttpClient) {}

    create(county: ICountySalesBen): Observable<EntityResponseType> {
        return this.http.post<ICountySalesBen>(this.resourceUrl, county, { observe: 'response' });
    }

    update(county: ICountySalesBen): Observable<EntityResponseType> {
        return this.http.put<ICountySalesBen>(this.resourceUrl, county, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<ICountySalesBen>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<ICountySalesBen[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
}
