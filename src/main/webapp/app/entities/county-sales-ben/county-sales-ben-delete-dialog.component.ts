import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ICountySalesBen } from 'app/shared/model/county-sales-ben.model';
import { CountySalesBenService } from './county-sales-ben.service';

@Component({
    selector: 'jhi-county-sales-ben-delete-dialog',
    templateUrl: './county-sales-ben-delete-dialog.component.html'
})
export class CountySalesBenDeleteDialogComponent {
    county: ICountySalesBen;

    constructor(
        protected countyService: CountySalesBenService,
        public activeModal: NgbActiveModal,
        protected eventManager: JhiEventManager
    ) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.countyService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'countyListModification',
                content: 'Deleted an county'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-county-sales-ben-delete-popup',
    template: ''
})
export class CountySalesBenDeletePopupComponent implements OnInit, OnDestroy {
    protected ngbModalRef: NgbModalRef;

    constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ county }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(CountySalesBenDeleteDialogComponent as Component, {
                    size: 'lg',
                    backdrop: 'static'
                });
                this.ngbModalRef.componentInstance.county = county;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
