import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SalesBenefitSharedModule } from 'app/shared';
import {
    NotificationSalesBenComponent,
    NotificationSalesBenDetailComponent,
    NotificationSalesBenUpdateComponent,
    NotificationSalesBenDeletePopupComponent,
    NotificationSalesBenDeleteDialogComponent,
    notificationRoute,
    notificationPopupRoute
} from './';

const ENTITY_STATES = [...notificationRoute, ...notificationPopupRoute];

@NgModule({
    imports: [SalesBenefitSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        NotificationSalesBenComponent,
        NotificationSalesBenDetailComponent,
        NotificationSalesBenUpdateComponent,
        NotificationSalesBenDeleteDialogComponent,
        NotificationSalesBenDeletePopupComponent
    ],
    entryComponents: [
        NotificationSalesBenComponent,
        NotificationSalesBenUpdateComponent,
        NotificationSalesBenDeleteDialogComponent,
        NotificationSalesBenDeletePopupComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SalesBenefitNotificationSalesBenModule {}
