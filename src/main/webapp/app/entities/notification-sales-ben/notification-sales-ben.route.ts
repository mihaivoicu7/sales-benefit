import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { NotificationSalesBen } from 'app/shared/model/notification-sales-ben.model';
import { NotificationSalesBenService } from './notification-sales-ben.service';
import { NotificationSalesBenComponent } from './notification-sales-ben.component';
import { NotificationSalesBenDetailComponent } from './notification-sales-ben-detail.component';
import { NotificationSalesBenUpdateComponent } from './notification-sales-ben-update.component';
import { NotificationSalesBenDeletePopupComponent } from './notification-sales-ben-delete-dialog.component';
import { INotificationSalesBen } from 'app/shared/model/notification-sales-ben.model';

@Injectable({ providedIn: 'root' })
export class NotificationSalesBenResolve implements Resolve<INotificationSalesBen> {
    constructor(private service: NotificationSalesBenService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<INotificationSalesBen> {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(
                filter((response: HttpResponse<NotificationSalesBen>) => response.ok),
                map((notification: HttpResponse<NotificationSalesBen>) => notification.body)
            );
        }
        return of(new NotificationSalesBen());
    }
}

export const notificationRoute: Routes = [
    {
        path: '',
        component: NotificationSalesBenComponent,
        resolve: {
            pagingParams: JhiResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            defaultSort: 'id,asc',
            pageTitle: 'Notifications'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/view',
        component: NotificationSalesBenDetailComponent,
        resolve: {
            notification: NotificationSalesBenResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Notifications'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'new',
        component: NotificationSalesBenUpdateComponent,
        resolve: {
            notification: NotificationSalesBenResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Notifications'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/edit',
        component: NotificationSalesBenUpdateComponent,
        resolve: {
            notification: NotificationSalesBenResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Notifications'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const notificationPopupRoute: Routes = [
    {
        path: ':id/delete',
        component: NotificationSalesBenDeletePopupComponent,
        resolve: {
            notification: NotificationSalesBenResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Notifications'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
