import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { INotificationSalesBen } from 'app/shared/model/notification-sales-ben.model';

@Component({
    selector: 'jhi-notification-sales-ben-detail',
    templateUrl: './notification-sales-ben-detail.component.html'
})
export class NotificationSalesBenDetailComponent implements OnInit {
    notification: INotificationSalesBen;

    constructor(protected activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ notification }) => {
            this.notification = notification;
        });
    }

    previousState() {
        window.history.back();
    }
}
