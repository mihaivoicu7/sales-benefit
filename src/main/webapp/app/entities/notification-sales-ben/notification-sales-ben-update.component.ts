import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { JhiAlertService } from 'ng-jhipster';
import { INotificationSalesBen } from 'app/shared/model/notification-sales-ben.model';
import { NotificationSalesBenService } from './notification-sales-ben.service';
import { IUser, UserService } from 'app/core';

@Component({
    selector: 'jhi-notification-sales-ben-update',
    templateUrl: './notification-sales-ben-update.component.html'
})
export class NotificationSalesBenUpdateComponent implements OnInit {
    notification: INotificationSalesBen;
    isSaving: boolean;

    users: IUser[];
    createDate: string;

    constructor(
        protected jhiAlertService: JhiAlertService,
        protected notificationService: NotificationSalesBenService,
        protected userService: UserService,
        protected activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ notification }) => {
            this.notification = notification;
            this.createDate = this.notification.createDate != null ? this.notification.createDate.format(DATE_TIME_FORMAT) : null;
        });
        this.userService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<IUser[]>) => mayBeOk.ok),
                map((response: HttpResponse<IUser[]>) => response.body)
            )
            .subscribe((res: IUser[]) => (this.users = res), (res: HttpErrorResponse) => this.onError(res.message));
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        this.notification.createDate = this.createDate != null ? moment(this.createDate, DATE_TIME_FORMAT) : null;
        if (this.notification.id !== undefined) {
            this.subscribeToSaveResponse(this.notificationService.update(this.notification));
        } else {
            this.subscribeToSaveResponse(this.notificationService.create(this.notification));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<INotificationSalesBen>>) {
        result.subscribe(
            (res: HttpResponse<INotificationSalesBen>) => this.onSaveSuccess(),
            (res: HttpErrorResponse) => this.onSaveError()
        );
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackUserById(index: number, item: IUser) {
        return item.id;
    }
}
