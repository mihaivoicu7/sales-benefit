import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { CampaignCitySalesBen } from 'app/shared/model/campaign-city-sales-ben.model';
import { CampaignCitySalesBenService } from './campaign-city-sales-ben.service';
import { CampaignCitySalesBenComponent } from './campaign-city-sales-ben.component';
import { CampaignCitySalesBenDetailComponent } from './campaign-city-sales-ben-detail.component';
import { CampaignCitySalesBenUpdateComponent } from './campaign-city-sales-ben-update.component';
import { CampaignCitySalesBenDeletePopupComponent } from './campaign-city-sales-ben-delete-dialog.component';
import { ICampaignCitySalesBen } from 'app/shared/model/campaign-city-sales-ben.model';

@Injectable({ providedIn: 'root' })
export class CampaignCitySalesBenResolve implements Resolve<ICampaignCitySalesBen> {
    constructor(private service: CampaignCitySalesBenService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ICampaignCitySalesBen> {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(
                filter((response: HttpResponse<CampaignCitySalesBen>) => response.ok),
                map((campaignCity: HttpResponse<CampaignCitySalesBen>) => campaignCity.body)
            );
        }
        return of(new CampaignCitySalesBen());
    }
}

export const campaignCityRoute: Routes = [
    {
        path: '',
        component: CampaignCitySalesBenComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'CampaignCities'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/view',
        component: CampaignCitySalesBenDetailComponent,
        resolve: {
            campaignCity: CampaignCitySalesBenResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'CampaignCities'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'new',
        component: CampaignCitySalesBenUpdateComponent,
        resolve: {
            campaignCity: CampaignCitySalesBenResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'CampaignCities'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/edit',
        component: CampaignCitySalesBenUpdateComponent,
        resolve: {
            campaignCity: CampaignCitySalesBenResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'CampaignCities'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const campaignCityPopupRoute: Routes = [
    {
        path: ':id/delete',
        component: CampaignCitySalesBenDeletePopupComponent,
        resolve: {
            campaignCity: CampaignCitySalesBenResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'CampaignCities'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
