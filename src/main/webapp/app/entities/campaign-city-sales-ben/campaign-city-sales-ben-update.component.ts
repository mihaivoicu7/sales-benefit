import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { ICampaignCitySalesBen } from 'app/shared/model/campaign-city-sales-ben.model';
import { CampaignCitySalesBenService } from './campaign-city-sales-ben.service';
import { ICampaignSalesBen } from 'app/shared/model/campaign-sales-ben.model';
import { CampaignSalesBenService } from 'app/entities/campaign-sales-ben';
import { ICitySalesBen } from 'app/shared/model/city-sales-ben.model';
import { CitySalesBenService } from 'app/entities/city-sales-ben';

@Component({
    selector: 'jhi-campaign-city-sales-ben-update',
    templateUrl: './campaign-city-sales-ben-update.component.html'
})
export class CampaignCitySalesBenUpdateComponent implements OnInit {
    campaignCity: ICampaignCitySalesBen;
    isSaving: boolean;

    campaigns: ICampaignSalesBen[];

    cities: ICitySalesBen[];

    constructor(
        protected jhiAlertService: JhiAlertService,
        protected campaignCityService: CampaignCitySalesBenService,
        protected campaignService: CampaignSalesBenService,
        protected cityService: CitySalesBenService,
        protected activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ campaignCity }) => {
            this.campaignCity = campaignCity;
        });
        this.campaignService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<ICampaignSalesBen[]>) => mayBeOk.ok),
                map((response: HttpResponse<ICampaignSalesBen[]>) => response.body)
            )
            .subscribe((res: ICampaignSalesBen[]) => (this.campaigns = res), (res: HttpErrorResponse) => this.onError(res.message));
        this.cityService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<ICitySalesBen[]>) => mayBeOk.ok),
                map((response: HttpResponse<ICitySalesBen[]>) => response.body)
            )
            .subscribe((res: ICitySalesBen[]) => (this.cities = res), (res: HttpErrorResponse) => this.onError(res.message));
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.campaignCity.id !== undefined) {
            this.subscribeToSaveResponse(this.campaignCityService.update(this.campaignCity));
        } else {
            this.subscribeToSaveResponse(this.campaignCityService.create(this.campaignCity));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<ICampaignCitySalesBen>>) {
        result.subscribe(
            (res: HttpResponse<ICampaignCitySalesBen>) => this.onSaveSuccess(),
            (res: HttpErrorResponse) => this.onSaveError()
        );
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackCampaignById(index: number, item: ICampaignSalesBen) {
        return item.id;
    }

    trackCityById(index: number, item: ICitySalesBen) {
        return item.id;
    }
}
