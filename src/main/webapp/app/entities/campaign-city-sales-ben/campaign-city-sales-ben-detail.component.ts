import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ICampaignCitySalesBen } from 'app/shared/model/campaign-city-sales-ben.model';

@Component({
    selector: 'jhi-campaign-city-sales-ben-detail',
    templateUrl: './campaign-city-sales-ben-detail.component.html'
})
export class CampaignCitySalesBenDetailComponent implements OnInit {
    campaignCity: ICampaignCitySalesBen;

    constructor(protected activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ campaignCity }) => {
            this.campaignCity = campaignCity;
        });
    }

    previousState() {
        window.history.back();
    }
}
