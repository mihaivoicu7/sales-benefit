import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SalesBenefitSharedModule } from 'app/shared';
import {
    CampaignCitySalesBenComponent,
    CampaignCitySalesBenDetailComponent,
    CampaignCitySalesBenUpdateComponent,
    CampaignCitySalesBenDeletePopupComponent,
    CampaignCitySalesBenDeleteDialogComponent,
    campaignCityRoute,
    campaignCityPopupRoute
} from './';

const ENTITY_STATES = [...campaignCityRoute, ...campaignCityPopupRoute];

@NgModule({
    imports: [SalesBenefitSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        CampaignCitySalesBenComponent,
        CampaignCitySalesBenDetailComponent,
        CampaignCitySalesBenUpdateComponent,
        CampaignCitySalesBenDeleteDialogComponent,
        CampaignCitySalesBenDeletePopupComponent
    ],
    entryComponents: [
        CampaignCitySalesBenComponent,
        CampaignCitySalesBenUpdateComponent,
        CampaignCitySalesBenDeleteDialogComponent,
        CampaignCitySalesBenDeletePopupComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SalesBenefitCampaignCitySalesBenModule {}
