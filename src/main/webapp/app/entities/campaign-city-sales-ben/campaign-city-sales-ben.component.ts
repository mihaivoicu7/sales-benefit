import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ICampaignCitySalesBen } from 'app/shared/model/campaign-city-sales-ben.model';
import { AccountService } from 'app/core';
import { CampaignCitySalesBenService } from './campaign-city-sales-ben.service';

@Component({
    selector: 'jhi-campaign-city-sales-ben',
    templateUrl: './campaign-city-sales-ben.component.html'
})
export class CampaignCitySalesBenComponent implements OnInit, OnDestroy {
    campaignCities: ICampaignCitySalesBen[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        protected campaignCityService: CampaignCitySalesBenService,
        protected jhiAlertService: JhiAlertService,
        protected eventManager: JhiEventManager,
        protected accountService: AccountService
    ) {}

    loadAll() {
        this.campaignCityService
            .query()
            .pipe(
                filter((res: HttpResponse<ICampaignCitySalesBen[]>) => res.ok),
                map((res: HttpResponse<ICampaignCitySalesBen[]>) => res.body)
            )
            .subscribe(
                (res: ICampaignCitySalesBen[]) => {
                    this.campaignCities = res;
                },
                (res: HttpErrorResponse) => this.onError(res.message)
            );
    }

    ngOnInit() {
        this.loadAll();
        this.accountService.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInCampaignCities();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: ICampaignCitySalesBen) {
        return item.id;
    }

    registerChangeInCampaignCities() {
        this.eventSubscriber = this.eventManager.subscribe('campaignCityListModification', response => this.loadAll());
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
