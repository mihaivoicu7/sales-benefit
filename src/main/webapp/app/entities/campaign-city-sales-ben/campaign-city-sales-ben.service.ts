import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { ICampaignCitySalesBen } from 'app/shared/model/campaign-city-sales-ben.model';

type EntityResponseType = HttpResponse<ICampaignCitySalesBen>;
type EntityArrayResponseType = HttpResponse<ICampaignCitySalesBen[]>;

@Injectable({ providedIn: 'root' })
export class CampaignCitySalesBenService {
    public resourceUrl = SERVER_API_URL + 'api/campaign-cities';

    constructor(protected http: HttpClient) {}

    create(campaignCity: ICampaignCitySalesBen): Observable<EntityResponseType> {
        return this.http.post<ICampaignCitySalesBen>(this.resourceUrl, campaignCity, { observe: 'response' });
    }

    update(campaignCity: ICampaignCitySalesBen): Observable<EntityResponseType> {
        return this.http.put<ICampaignCitySalesBen>(this.resourceUrl, campaignCity, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<ICampaignCitySalesBen>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<ICampaignCitySalesBen[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
}
