import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { JhiAlertService } from 'ng-jhipster';
import { ISellerTransactionSalesBen } from 'app/shared/model/seller-transaction-sales-ben.model';
import { SellerTransactionSalesBenService } from './seller-transaction-sales-ben.service';
import { ICampaignProductSalesBen } from 'app/shared/model/campaign-product-sales-ben.model';
import { CampaignProductSalesBenService } from 'app/entities/campaign-product-sales-ben';
import { ISellerSalesBen } from 'app/shared/model/seller-sales-ben.model';
import { SellerSalesBenService } from 'app/entities/seller-sales-ben';

@Component({
    selector: 'jhi-seller-transaction-sales-ben-update',
    templateUrl: './seller-transaction-sales-ben-update.component.html'
})
export class SellerTransactionSalesBenUpdateComponent implements OnInit {
    sellerTransaction: ISellerTransactionSalesBen;
    isSaving: boolean;

    campaignproducts: ICampaignProductSalesBen[];

    sellers: ISellerSalesBen[];
    transactionDate: string;

    constructor(
        protected jhiAlertService: JhiAlertService,
        protected sellerTransactionService: SellerTransactionSalesBenService,
        protected campaignProductService: CampaignProductSalesBenService,
        protected sellerService: SellerSalesBenService,
        protected activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ sellerTransaction }) => {
            this.sellerTransaction = sellerTransaction;
            this.transactionDate =
                this.sellerTransaction.transactionDate != null ? this.sellerTransaction.transactionDate.format(DATE_TIME_FORMAT) : null;
        });
        this.campaignProductService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<ICampaignProductSalesBen[]>) => mayBeOk.ok),
                map((response: HttpResponse<ICampaignProductSalesBen[]>) => response.body)
            )
            .subscribe(
                (res: ICampaignProductSalesBen[]) => (this.campaignproducts = res),
                (res: HttpErrorResponse) => this.onError(res.message)
            );
        this.sellerService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<ISellerSalesBen[]>) => mayBeOk.ok),
                map((response: HttpResponse<ISellerSalesBen[]>) => response.body)
            )
            .subscribe((res: ISellerSalesBen[]) => (this.sellers = res), (res: HttpErrorResponse) => this.onError(res.message));
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        this.sellerTransaction.transactionDate = this.transactionDate != null ? moment(this.transactionDate, DATE_TIME_FORMAT) : null;
        if (this.sellerTransaction.id !== undefined) {
            this.subscribeToSaveResponse(this.sellerTransactionService.update(this.sellerTransaction));
        } else {
            this.subscribeToSaveResponse(this.sellerTransactionService.create(this.sellerTransaction));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<ISellerTransactionSalesBen>>) {
        result.subscribe(
            (res: HttpResponse<ISellerTransactionSalesBen>) => this.onSaveSuccess(),
            (res: HttpErrorResponse) => this.onSaveError()
        );
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackCampaignProductById(index: number, item: ICampaignProductSalesBen) {
        return item.id;
    }

    trackSellerById(index: number, item: ISellerSalesBen) {
        return item.id;
    }
}
