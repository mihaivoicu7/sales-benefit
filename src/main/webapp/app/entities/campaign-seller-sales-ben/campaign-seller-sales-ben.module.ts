import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SalesBenefitSharedModule } from 'app/shared';
import {
    CampaignSellerSalesBenComponent,
    CampaignSellerSalesBenDetailComponent,
    CampaignSellerSalesBenUpdateComponent,
    CampaignSellerSalesBenDeletePopupComponent,
    CampaignSellerSalesBenDeleteDialogComponent,
    campaignSellerRoute,
    campaignSellerPopupRoute
} from './';

const ENTITY_STATES = [...campaignSellerRoute, ...campaignSellerPopupRoute];

@NgModule({
    imports: [SalesBenefitSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        CampaignSellerSalesBenComponent,
        CampaignSellerSalesBenDetailComponent,
        CampaignSellerSalesBenUpdateComponent,
        CampaignSellerSalesBenDeleteDialogComponent,
        CampaignSellerSalesBenDeletePopupComponent
    ],
    entryComponents: [
        CampaignSellerSalesBenComponent,
        CampaignSellerSalesBenUpdateComponent,
        CampaignSellerSalesBenDeleteDialogComponent,
        CampaignSellerSalesBenDeletePopupComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SalesBenefitCampaignSellerSalesBenModule {}
