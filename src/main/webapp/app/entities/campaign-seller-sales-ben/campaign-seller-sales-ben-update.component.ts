import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { ICampaignSellerSalesBen } from 'app/shared/model/campaign-seller-sales-ben.model';
import { CampaignSellerSalesBenService } from './campaign-seller-sales-ben.service';
import { ICampaignSalesBen } from 'app/shared/model/campaign-sales-ben.model';
import { CampaignSalesBenService } from 'app/entities/campaign-sales-ben';
import { ISellerSalesBen } from 'app/shared/model/seller-sales-ben.model';
import { SellerSalesBenService } from 'app/entities/seller-sales-ben';

@Component({
    selector: 'jhi-campaign-seller-sales-ben-update',
    templateUrl: './campaign-seller-sales-ben-update.component.html'
})
export class CampaignSellerSalesBenUpdateComponent implements OnInit {
    campaignSeller: ICampaignSellerSalesBen;
    isSaving: boolean;

    campaigns: ICampaignSalesBen[];

    sellers: ISellerSalesBen[];

    constructor(
        protected jhiAlertService: JhiAlertService,
        protected campaignSellerService: CampaignSellerSalesBenService,
        protected campaignService: CampaignSalesBenService,
        protected sellerService: SellerSalesBenService,
        protected activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ campaignSeller }) => {
            this.campaignSeller = campaignSeller;
        });
        this.campaignService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<ICampaignSalesBen[]>) => mayBeOk.ok),
                map((response: HttpResponse<ICampaignSalesBen[]>) => response.body)
            )
            .subscribe((res: ICampaignSalesBen[]) => (this.campaigns = res), (res: HttpErrorResponse) => this.onError(res.message));
        this.sellerService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<ISellerSalesBen[]>) => mayBeOk.ok),
                map((response: HttpResponse<ISellerSalesBen[]>) => response.body)
            )
            .subscribe((res: ISellerSalesBen[]) => (this.sellers = res), (res: HttpErrorResponse) => this.onError(res.message));
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.campaignSeller.id !== undefined) {
            this.subscribeToSaveResponse(this.campaignSellerService.update(this.campaignSeller));
        } else {
            this.subscribeToSaveResponse(this.campaignSellerService.create(this.campaignSeller));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<ICampaignSellerSalesBen>>) {
        result.subscribe(
            (res: HttpResponse<ICampaignSellerSalesBen>) => this.onSaveSuccess(),
            (res: HttpErrorResponse) => this.onSaveError()
        );
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackCampaignById(index: number, item: ICampaignSalesBen) {
        return item.id;
    }

    trackSellerById(index: number, item: ISellerSalesBen) {
        return item.id;
    }
}
