import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { ICampaignSellerSalesBen } from 'app/shared/model/campaign-seller-sales-ben.model';

type EntityResponseType = HttpResponse<ICampaignSellerSalesBen>;
type EntityArrayResponseType = HttpResponse<ICampaignSellerSalesBen[]>;

@Injectable({ providedIn: 'root' })
export class CampaignSellerSalesBenService {
    public resourceUrl = SERVER_API_URL + 'api/campaign-sellers';

    constructor(protected http: HttpClient) {}

    create(campaignSeller: ICampaignSellerSalesBen): Observable<EntityResponseType> {
        return this.http.post<ICampaignSellerSalesBen>(this.resourceUrl, campaignSeller, { observe: 'response' });
    }

    update(campaignSeller: ICampaignSellerSalesBen): Observable<EntityResponseType> {
        return this.http.put<ICampaignSellerSalesBen>(this.resourceUrl, campaignSeller, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<ICampaignSellerSalesBen>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<ICampaignSellerSalesBen[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
}
