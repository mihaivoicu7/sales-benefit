import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ICampaignSellerSalesBen } from 'app/shared/model/campaign-seller-sales-ben.model';
import { CampaignSellerSalesBenService } from './campaign-seller-sales-ben.service';

@Component({
    selector: 'jhi-campaign-seller-sales-ben-delete-dialog',
    templateUrl: './campaign-seller-sales-ben-delete-dialog.component.html'
})
export class CampaignSellerSalesBenDeleteDialogComponent {
    campaignSeller: ICampaignSellerSalesBen;

    constructor(
        protected campaignSellerService: CampaignSellerSalesBenService,
        public activeModal: NgbActiveModal,
        protected eventManager: JhiEventManager
    ) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.campaignSellerService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'campaignSellerListModification',
                content: 'Deleted an campaignSeller'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-campaign-seller-sales-ben-delete-popup',
    template: ''
})
export class CampaignSellerSalesBenDeletePopupComponent implements OnInit, OnDestroy {
    protected ngbModalRef: NgbModalRef;

    constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ campaignSeller }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(CampaignSellerSalesBenDeleteDialogComponent as Component, {
                    size: 'lg',
                    backdrop: 'static'
                });
                this.ngbModalRef.componentInstance.campaignSeller = campaignSeller;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate(['/campaign-seller-sales-ben', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate(['/campaign-seller-sales-ben', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
