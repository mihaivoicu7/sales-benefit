import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { CampaignSellerSalesBen } from 'app/shared/model/campaign-seller-sales-ben.model';
import { CampaignSellerSalesBenService } from './campaign-seller-sales-ben.service';
import { CampaignSellerSalesBenComponent } from './campaign-seller-sales-ben.component';
import { CampaignSellerSalesBenDetailComponent } from './campaign-seller-sales-ben-detail.component';
import { CampaignSellerSalesBenUpdateComponent } from './campaign-seller-sales-ben-update.component';
import { CampaignSellerSalesBenDeletePopupComponent } from './campaign-seller-sales-ben-delete-dialog.component';
import { ICampaignSellerSalesBen } from 'app/shared/model/campaign-seller-sales-ben.model';

@Injectable({ providedIn: 'root' })
export class CampaignSellerSalesBenResolve implements Resolve<ICampaignSellerSalesBen> {
    constructor(private service: CampaignSellerSalesBenService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ICampaignSellerSalesBen> {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(
                filter((response: HttpResponse<CampaignSellerSalesBen>) => response.ok),
                map((campaignSeller: HttpResponse<CampaignSellerSalesBen>) => campaignSeller.body)
            );
        }
        return of(new CampaignSellerSalesBen());
    }
}

export const campaignSellerRoute: Routes = [
    {
        path: '',
        component: CampaignSellerSalesBenComponent,
        resolve: {
            pagingParams: JhiResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            defaultSort: 'id,asc',
            pageTitle: 'CampaignSellers'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/view',
        component: CampaignSellerSalesBenDetailComponent,
        resolve: {
            campaignSeller: CampaignSellerSalesBenResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'CampaignSellers'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'new',
        component: CampaignSellerSalesBenUpdateComponent,
        resolve: {
            campaignSeller: CampaignSellerSalesBenResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'CampaignSellers'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/edit',
        component: CampaignSellerSalesBenUpdateComponent,
        resolve: {
            campaignSeller: CampaignSellerSalesBenResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'CampaignSellers'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const campaignSellerPopupRoute: Routes = [
    {
        path: ':id/delete',
        component: CampaignSellerSalesBenDeletePopupComponent,
        resolve: {
            campaignSeller: CampaignSellerSalesBenResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'CampaignSellers'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
