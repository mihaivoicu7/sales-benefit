import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ICampaignSellerSalesBen } from 'app/shared/model/campaign-seller-sales-ben.model';

@Component({
    selector: 'jhi-campaign-seller-sales-ben-detail',
    templateUrl: './campaign-seller-sales-ben-detail.component.html'
})
export class CampaignSellerSalesBenDetailComponent implements OnInit {
    campaignSeller: ICampaignSellerSalesBen;

    constructor(protected activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ campaignSeller }) => {
            this.campaignSeller = campaignSeller;
        });
    }

    previousState() {
        window.history.back();
    }
}
