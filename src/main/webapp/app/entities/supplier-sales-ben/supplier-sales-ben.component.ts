import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ISupplierSalesBen } from 'app/shared/model/supplier-sales-ben.model';
import { AccountService } from 'app/core';
import { SupplierSalesBenService } from './supplier-sales-ben.service';

@Component({
    selector: 'jhi-supplier-sales-ben',
    templateUrl: './supplier-sales-ben.component.html'
})
export class SupplierSalesBenComponent implements OnInit, OnDestroy {
    suppliers: ISupplierSalesBen[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        protected supplierService: SupplierSalesBenService,
        protected jhiAlertService: JhiAlertService,
        protected eventManager: JhiEventManager,
        protected accountService: AccountService
    ) {}

    loadAll() {
        this.supplierService
            .query()
            .pipe(
                filter((res: HttpResponse<ISupplierSalesBen[]>) => res.ok),
                map((res: HttpResponse<ISupplierSalesBen[]>) => res.body)
            )
            .subscribe(
                (res: ISupplierSalesBen[]) => {
                    this.suppliers = res;
                },
                (res: HttpErrorResponse) => this.onError(res.message)
            );
    }

    ngOnInit() {
        this.loadAll();
        this.accountService.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInSuppliers();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: ISupplierSalesBen) {
        return item.id;
    }

    registerChangeInSuppliers() {
        this.eventSubscriber = this.eventManager.subscribe('supplierListModification', response => this.loadAll());
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
