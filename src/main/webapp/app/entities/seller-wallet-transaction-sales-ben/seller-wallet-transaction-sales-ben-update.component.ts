import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { JhiAlertService } from 'ng-jhipster';
import { ISellerWalletTransactionSalesBen } from 'app/shared/model/seller-wallet-transaction-sales-ben.model';
import { SellerWalletTransactionSalesBenService } from './seller-wallet-transaction-sales-ben.service';
import { ISellerWalletSalesBen } from 'app/shared/model/seller-wallet-sales-ben.model';
import { SellerWalletSalesBenService } from 'app/entities/seller-wallet-sales-ben';

@Component({
    selector: 'jhi-seller-wallet-transaction-sales-ben-update',
    templateUrl: './seller-wallet-transaction-sales-ben-update.component.html'
})
export class SellerWalletTransactionSalesBenUpdateComponent implements OnInit {
    sellerWalletTransaction: ISellerWalletTransactionSalesBen;
    isSaving: boolean;

    sellerwallets: ISellerWalletSalesBen[];
    transactionDate: string;

    constructor(
        protected jhiAlertService: JhiAlertService,
        protected sellerWalletTransactionService: SellerWalletTransactionSalesBenService,
        protected sellerWalletService: SellerWalletSalesBenService,
        protected activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ sellerWalletTransaction }) => {
            this.sellerWalletTransaction = sellerWalletTransaction;
            this.transactionDate =
                this.sellerWalletTransaction.transactionDate != null
                    ? this.sellerWalletTransaction.transactionDate.format(DATE_TIME_FORMAT)
                    : null;
        });
        this.sellerWalletService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<ISellerWalletSalesBen[]>) => mayBeOk.ok),
                map((response: HttpResponse<ISellerWalletSalesBen[]>) => response.body)
            )
            .subscribe((res: ISellerWalletSalesBen[]) => (this.sellerwallets = res), (res: HttpErrorResponse) => this.onError(res.message));
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        this.sellerWalletTransaction.transactionDate = this.transactionDate != null ? moment(this.transactionDate, DATE_TIME_FORMAT) : null;
        if (this.sellerWalletTransaction.id !== undefined) {
            this.subscribeToSaveResponse(this.sellerWalletTransactionService.update(this.sellerWalletTransaction));
        } else {
            this.subscribeToSaveResponse(this.sellerWalletTransactionService.create(this.sellerWalletTransaction));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<ISellerWalletTransactionSalesBen>>) {
        result.subscribe(
            (res: HttpResponse<ISellerWalletTransactionSalesBen>) => this.onSaveSuccess(),
            (res: HttpErrorResponse) => this.onSaveError()
        );
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackSellerWalletById(index: number, item: ISellerWalletSalesBen) {
        return item.id;
    }
}
