import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { ICampaignProductSalesBen } from 'app/shared/model/campaign-product-sales-ben.model';
import { CampaignProductSalesBenService } from './campaign-product-sales-ben.service';
import { ICampaignSalesBen } from 'app/shared/model/campaign-sales-ben.model';
import { CampaignSalesBenService } from 'app/entities/campaign-sales-ben';
import { IProductSalesBen } from 'app/shared/model/product-sales-ben.model';
import { ProductSalesBenService } from 'app/entities/product-sales-ben';

@Component({
    selector: 'jhi-campaign-product-sales-ben-update',
    templateUrl: './campaign-product-sales-ben-update.component.html'
})
export class CampaignProductSalesBenUpdateComponent implements OnInit {
    campaignProduct: ICampaignProductSalesBen;
    isSaving: boolean;

    campaigns: ICampaignSalesBen[];

    products: IProductSalesBen[];

    constructor(
        protected jhiAlertService: JhiAlertService,
        protected campaignProductService: CampaignProductSalesBenService,
        protected campaignService: CampaignSalesBenService,
        protected productService: ProductSalesBenService,
        protected activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ campaignProduct }) => {
            this.campaignProduct = campaignProduct;
        });
        this.campaignService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<ICampaignSalesBen[]>) => mayBeOk.ok),
                map((response: HttpResponse<ICampaignSalesBen[]>) => response.body)
            )
            .subscribe((res: ICampaignSalesBen[]) => (this.campaigns = res), (res: HttpErrorResponse) => this.onError(res.message));
        this.productService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<IProductSalesBen[]>) => mayBeOk.ok),
                map((response: HttpResponse<IProductSalesBen[]>) => response.body)
            )
            .subscribe((res: IProductSalesBen[]) => (this.products = res), (res: HttpErrorResponse) => this.onError(res.message));
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.campaignProduct.id !== undefined) {
            this.subscribeToSaveResponse(this.campaignProductService.update(this.campaignProduct));
        } else {
            this.subscribeToSaveResponse(this.campaignProductService.create(this.campaignProduct));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<ICampaignProductSalesBen>>) {
        result.subscribe(
            (res: HttpResponse<ICampaignProductSalesBen>) => this.onSaveSuccess(),
            (res: HttpErrorResponse) => this.onSaveError()
        );
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackCampaignById(index: number, item: ICampaignSalesBen) {
        return item.id;
    }

    trackProductById(index: number, item: IProductSalesBen) {
        return item.id;
    }
}
