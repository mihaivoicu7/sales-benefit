import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { CampaignProductSalesBen } from 'app/shared/model/campaign-product-sales-ben.model';
import { CampaignProductSalesBenService } from './campaign-product-sales-ben.service';
import { CampaignProductSalesBenComponent } from './campaign-product-sales-ben.component';
import { CampaignProductSalesBenDetailComponent } from './campaign-product-sales-ben-detail.component';
import { CampaignProductSalesBenUpdateComponent } from './campaign-product-sales-ben-update.component';
import { CampaignProductSalesBenDeletePopupComponent } from './campaign-product-sales-ben-delete-dialog.component';
import { ICampaignProductSalesBen } from 'app/shared/model/campaign-product-sales-ben.model';

@Injectable({ providedIn: 'root' })
export class CampaignProductSalesBenResolve implements Resolve<ICampaignProductSalesBen> {
    constructor(private service: CampaignProductSalesBenService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ICampaignProductSalesBen> {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(
                filter((response: HttpResponse<CampaignProductSalesBen>) => response.ok),
                map((campaignProduct: HttpResponse<CampaignProductSalesBen>) => campaignProduct.body)
            );
        }
        return of(new CampaignProductSalesBen());
    }
}

export const campaignProductRoute: Routes = [
    {
        path: '',
        component: CampaignProductSalesBenComponent,
        resolve: {
            pagingParams: JhiResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            defaultSort: 'id,asc',
            pageTitle: 'CampaignProducts'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/view',
        component: CampaignProductSalesBenDetailComponent,
        resolve: {
            campaignProduct: CampaignProductSalesBenResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'CampaignProducts'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'new',
        component: CampaignProductSalesBenUpdateComponent,
        resolve: {
            campaignProduct: CampaignProductSalesBenResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'CampaignProducts'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/edit',
        component: CampaignProductSalesBenUpdateComponent,
        resolve: {
            campaignProduct: CampaignProductSalesBenResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'CampaignProducts'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const campaignProductPopupRoute: Routes = [
    {
        path: ':id/delete',
        component: CampaignProductSalesBenDeletePopupComponent,
        resolve: {
            campaignProduct: CampaignProductSalesBenResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'CampaignProducts'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
