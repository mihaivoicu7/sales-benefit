import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { CitySalesBen } from 'app/shared/model/city-sales-ben.model';
import { CitySalesBenService } from './city-sales-ben.service';
import { CitySalesBenComponent } from './city-sales-ben.component';
import { CitySalesBenDetailComponent } from './city-sales-ben-detail.component';
import { CitySalesBenUpdateComponent } from './city-sales-ben-update.component';
import { CitySalesBenDeletePopupComponent } from './city-sales-ben-delete-dialog.component';
import { ICitySalesBen } from 'app/shared/model/city-sales-ben.model';

@Injectable({ providedIn: 'root' })
export class CitySalesBenResolve implements Resolve<ICitySalesBen> {
    constructor(private service: CitySalesBenService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<CitySalesBen> {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(
                filter((response: HttpResponse<CitySalesBen>) => response.ok),
                map((city: HttpResponse<CitySalesBen>) => city.body)
            );
        }
        return of(new CitySalesBen());
    }
}

export const cityRoute: Routes = [
    {
        path: 'city-sales-ben',
        component: CitySalesBenComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Cities'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'city-sales-ben/:id/view',
        component: CitySalesBenDetailComponent,
        resolve: {
            city: CitySalesBenResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Cities'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'city-sales-ben/new',
        component: CitySalesBenUpdateComponent,
        resolve: {
            city: CitySalesBenResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Cities'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'city-sales-ben/:id/edit',
        component: CitySalesBenUpdateComponent,
        resolve: {
            city: CitySalesBenResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Cities'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const cityPopupRoute: Routes = [
    {
        path: 'city-sales-ben/:id/delete',
        component: CitySalesBenDeletePopupComponent,
        resolve: {
            city: CitySalesBenResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Cities'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
