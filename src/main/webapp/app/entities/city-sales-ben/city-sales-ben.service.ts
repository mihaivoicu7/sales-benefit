import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { ICitySalesBen } from 'app/shared/model/city-sales-ben.model';

type EntityResponseType = HttpResponse<ICitySalesBen>;
type EntityArrayResponseType = HttpResponse<ICitySalesBen[]>;

@Injectable({ providedIn: 'root' })
export class CitySalesBenService {
    public resourceUrl = SERVER_API_URL + 'api/cities';

    constructor(protected http: HttpClient) {}

    create(city: ICitySalesBen): Observable<EntityResponseType> {
        return this.http.post<ICitySalesBen>(this.resourceUrl, city, { observe: 'response' });
    }

    update(city: ICitySalesBen): Observable<EntityResponseType> {
        return this.http.put<ICitySalesBen>(this.resourceUrl, city, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<ICitySalesBen>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<ICitySalesBen[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
}
