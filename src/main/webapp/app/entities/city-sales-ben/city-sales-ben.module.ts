import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SalesBenefitSharedModule } from 'app/shared';
import {
    CitySalesBenComponent,
    CitySalesBenDetailComponent,
    CitySalesBenUpdateComponent,
    CitySalesBenDeletePopupComponent,
    CitySalesBenDeleteDialogComponent,
    cityRoute,
    cityPopupRoute
} from './';

const ENTITY_STATES = [...cityRoute, ...cityPopupRoute];

@NgModule({
    imports: [SalesBenefitSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        CitySalesBenComponent,
        CitySalesBenDetailComponent,
        CitySalesBenUpdateComponent,
        CitySalesBenDeleteDialogComponent,
        CitySalesBenDeletePopupComponent
    ],
    entryComponents: [
        CitySalesBenComponent,
        CitySalesBenUpdateComponent,
        CitySalesBenDeleteDialogComponent,
        CitySalesBenDeletePopupComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SalesBenefitCitySalesBenModule {}
