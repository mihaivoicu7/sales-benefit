import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ICitySalesBen } from 'app/shared/model/city-sales-ben.model';

@Component({
    selector: 'jhi-city-sales-ben-detail',
    templateUrl: './city-sales-ben-detail.component.html'
})
export class CitySalesBenDetailComponent implements OnInit {
    city: ICitySalesBen;

    constructor(protected activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ city }) => {
            this.city = city;
        });
    }

    previousState() {
        window.history.back();
    }
}
