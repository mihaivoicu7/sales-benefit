import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ICitySalesBen } from 'app/shared/model/city-sales-ben.model';
import { AccountService } from 'app/core';
import { CitySalesBenService } from './city-sales-ben.service';

@Component({
    selector: 'jhi-city-sales-ben',
    templateUrl: './city-sales-ben.component.html'
})
export class CitySalesBenComponent implements OnInit, OnDestroy {
    cities: ICitySalesBen[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        protected cityService: CitySalesBenService,
        protected jhiAlertService: JhiAlertService,
        protected eventManager: JhiEventManager,
        protected accountService: AccountService
    ) {}

    loadAll() {
        this.cityService.query().subscribe(
            (res: HttpResponse<ICitySalesBen[]>) => {
                this.cities = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    ngOnInit() {
        this.loadAll();
        this.accountService.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInCities();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: ICitySalesBen) {
        return item.id;
    }

    registerChangeInCities() {
        this.eventSubscriber = this.eventManager.subscribe('cityListModification', response => this.loadAll());
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
