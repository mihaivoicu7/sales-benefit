import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { ICitySalesBen } from 'app/shared/model/city-sales-ben.model';
import { CitySalesBenService } from './city-sales-ben.service';

@Component({
    selector: 'jhi-city-sales-ben-update',
    templateUrl: './city-sales-ben-update.component.html'
})
export class CitySalesBenUpdateComponent implements OnInit {
    city: ICitySalesBen;
    isSaving: boolean;

    constructor(protected cityService: CitySalesBenService, protected activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ city }) => {
            this.city = city;
        });
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.city.id !== undefined) {
            this.subscribeToSaveResponse(this.cityService.update(this.city));
        } else {
            this.subscribeToSaveResponse(this.cityService.create(this.city));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<ICitySalesBen>>) {
        result.subscribe((res: HttpResponse<ICitySalesBen>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }
}
