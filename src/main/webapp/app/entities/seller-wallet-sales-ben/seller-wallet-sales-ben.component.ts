import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ISellerWalletSalesBen } from 'app/shared/model/seller-wallet-sales-ben.model';
import { AccountService } from 'app/core';
import { SellerWalletSalesBenService } from './seller-wallet-sales-ben.service';

@Component({
    selector: 'jhi-seller-wallet-sales-ben',
    templateUrl: './seller-wallet-sales-ben.component.html'
})
export class SellerWalletSalesBenComponent implements OnInit, OnDestroy {
    sellerWallets: ISellerWalletSalesBen[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        protected sellerWalletService: SellerWalletSalesBenService,
        protected jhiAlertService: JhiAlertService,
        protected eventManager: JhiEventManager,
        protected accountService: AccountService
    ) {}

    loadAll() {
        this.sellerWalletService
            .query()
            .pipe(
                filter((res: HttpResponse<ISellerWalletSalesBen[]>) => res.ok),
                map((res: HttpResponse<ISellerWalletSalesBen[]>) => res.body)
            )
            .subscribe(
                (res: ISellerWalletSalesBen[]) => {
                    this.sellerWallets = res;
                },
                (res: HttpErrorResponse) => this.onError(res.message)
            );
    }

    ngOnInit() {
        this.loadAll();
        this.accountService.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInSellerWallets();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: ISellerWalletSalesBen) {
        return item.id;
    }

    registerChangeInSellerWallets() {
        this.eventSubscriber = this.eventManager.subscribe('sellerWalletListModification', response => this.loadAll());
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
