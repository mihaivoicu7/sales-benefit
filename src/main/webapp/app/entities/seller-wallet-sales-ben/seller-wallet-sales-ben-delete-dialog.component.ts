import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ISellerWalletSalesBen } from 'app/shared/model/seller-wallet-sales-ben.model';
import { SellerWalletSalesBenService } from './seller-wallet-sales-ben.service';

@Component({
    selector: 'jhi-seller-wallet-sales-ben-delete-dialog',
    templateUrl: './seller-wallet-sales-ben-delete-dialog.component.html'
})
export class SellerWalletSalesBenDeleteDialogComponent {
    sellerWallet: ISellerWalletSalesBen;

    constructor(
        protected sellerWalletService: SellerWalletSalesBenService,
        public activeModal: NgbActiveModal,
        protected eventManager: JhiEventManager
    ) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.sellerWalletService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'sellerWalletListModification',
                content: 'Deleted an sellerWallet'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-seller-wallet-sales-ben-delete-popup',
    template: ''
})
export class SellerWalletSalesBenDeletePopupComponent implements OnInit, OnDestroy {
    protected ngbModalRef: NgbModalRef;

    constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ sellerWallet }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(SellerWalletSalesBenDeleteDialogComponent as Component, {
                    size: 'lg',
                    backdrop: 'static'
                });
                this.ngbModalRef.componentInstance.sellerWallet = sellerWallet;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate(['/seller-wallet-sales-ben', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate(['/seller-wallet-sales-ben', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
