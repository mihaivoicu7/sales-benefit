import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { ISellerCitySalesBen } from 'app/shared/model/seller-city-sales-ben.model';
import { SellerCitySalesBenService } from './seller-city-sales-ben.service';
import { ISellerSalesBen } from 'app/shared/model/seller-sales-ben.model';
import { SellerSalesBenService } from 'app/entities/seller-sales-ben';
import { ICitySalesBen } from 'app/shared/model/city-sales-ben.model';
import { CitySalesBenService } from 'app/entities/city-sales-ben';

@Component({
    selector: 'jhi-seller-city-sales-ben-update',
    templateUrl: './seller-city-sales-ben-update.component.html'
})
export class SellerCitySalesBenUpdateComponent implements OnInit {
    sellerCity: ISellerCitySalesBen;
    isSaving: boolean;

    sellers: ISellerSalesBen[];

    cities: ICitySalesBen[];

    constructor(
        protected jhiAlertService: JhiAlertService,
        protected sellerCityService: SellerCitySalesBenService,
        protected sellerService: SellerSalesBenService,
        protected cityService: CitySalesBenService,
        protected activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ sellerCity }) => {
            this.sellerCity = sellerCity;
        });
        this.sellerService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<ISellerSalesBen[]>) => mayBeOk.ok),
                map((response: HttpResponse<ISellerSalesBen[]>) => response.body)
            )
            .subscribe((res: ISellerSalesBen[]) => (this.sellers = res), (res: HttpErrorResponse) => this.onError(res.message));
        this.cityService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<ICitySalesBen[]>) => mayBeOk.ok),
                map((response: HttpResponse<ICitySalesBen[]>) => response.body)
            )
            .subscribe((res: ICitySalesBen[]) => (this.cities = res), (res: HttpErrorResponse) => this.onError(res.message));
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.sellerCity.id !== undefined) {
            this.subscribeToSaveResponse(this.sellerCityService.update(this.sellerCity));
        } else {
            this.subscribeToSaveResponse(this.sellerCityService.create(this.sellerCity));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<ISellerCitySalesBen>>) {
        result.subscribe((res: HttpResponse<ISellerCitySalesBen>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackSellerById(index: number, item: ISellerSalesBen) {
        return item.id;
    }

    trackCityById(index: number, item: ICitySalesBen) {
        return item.id;
    }
}
