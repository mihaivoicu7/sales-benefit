import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ISellerCitySalesBen } from 'app/shared/model/seller-city-sales-ben.model';

@Component({
    selector: 'jhi-seller-city-sales-ben-detail',
    templateUrl: './seller-city-sales-ben-detail.component.html'
})
export class SellerCitySalesBenDetailComponent implements OnInit {
    sellerCity: ISellerCitySalesBen;

    constructor(protected activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ sellerCity }) => {
            this.sellerCity = sellerCity;
        });
    }

    previousState() {
        window.history.back();
    }
}
