import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SalesBenefitSharedModule } from 'app/shared';
import {
    SellerCitySalesBenComponent,
    SellerCitySalesBenDetailComponent,
    SellerCitySalesBenUpdateComponent,
    SellerCitySalesBenDeletePopupComponent,
    SellerCitySalesBenDeleteDialogComponent,
    sellerCityRoute,
    sellerCityPopupRoute
} from './';

const ENTITY_STATES = [...sellerCityRoute, ...sellerCityPopupRoute];

@NgModule({
    imports: [SalesBenefitSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        SellerCitySalesBenComponent,
        SellerCitySalesBenDetailComponent,
        SellerCitySalesBenUpdateComponent,
        SellerCitySalesBenDeleteDialogComponent,
        SellerCitySalesBenDeletePopupComponent
    ],
    entryComponents: [
        SellerCitySalesBenComponent,
        SellerCitySalesBenUpdateComponent,
        SellerCitySalesBenDeleteDialogComponent,
        SellerCitySalesBenDeletePopupComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SalesBenefitSellerCitySalesBenModule {}
