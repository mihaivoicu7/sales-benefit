import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ISellerCitySalesBen } from 'app/shared/model/seller-city-sales-ben.model';
import { SellerCitySalesBenService } from './seller-city-sales-ben.service';

@Component({
    selector: 'jhi-seller-city-sales-ben-delete-dialog',
    templateUrl: './seller-city-sales-ben-delete-dialog.component.html'
})
export class SellerCitySalesBenDeleteDialogComponent {
    sellerCity: ISellerCitySalesBen;

    constructor(
        protected sellerCityService: SellerCitySalesBenService,
        public activeModal: NgbActiveModal,
        protected eventManager: JhiEventManager
    ) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.sellerCityService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'sellerCityListModification',
                content: 'Deleted an sellerCity'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-seller-city-sales-ben-delete-popup',
    template: ''
})
export class SellerCitySalesBenDeletePopupComponent implements OnInit, OnDestroy {
    protected ngbModalRef: NgbModalRef;

    constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ sellerCity }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(SellerCitySalesBenDeleteDialogComponent as Component, {
                    size: 'lg',
                    backdrop: 'static'
                });
                this.ngbModalRef.componentInstance.sellerCity = sellerCity;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate(['/seller-city-sales-ben', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate(['/seller-city-sales-ben', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
