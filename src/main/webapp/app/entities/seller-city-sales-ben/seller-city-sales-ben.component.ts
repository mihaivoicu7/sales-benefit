import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ISellerCitySalesBen } from 'app/shared/model/seller-city-sales-ben.model';
import { AccountService } from 'app/core';
import { SellerCitySalesBenService } from './seller-city-sales-ben.service';

@Component({
    selector: 'jhi-seller-city-sales-ben',
    templateUrl: './seller-city-sales-ben.component.html'
})
export class SellerCitySalesBenComponent implements OnInit, OnDestroy {
    sellerCities: ISellerCitySalesBen[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        protected sellerCityService: SellerCitySalesBenService,
        protected jhiAlertService: JhiAlertService,
        protected eventManager: JhiEventManager,
        protected accountService: AccountService
    ) {}

    loadAll() {
        this.sellerCityService
            .query()
            .pipe(
                filter((res: HttpResponse<ISellerCitySalesBen[]>) => res.ok),
                map((res: HttpResponse<ISellerCitySalesBen[]>) => res.body)
            )
            .subscribe(
                (res: ISellerCitySalesBen[]) => {
                    this.sellerCities = res;
                },
                (res: HttpErrorResponse) => this.onError(res.message)
            );
    }

    ngOnInit() {
        this.loadAll();
        this.accountService.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInSellerCities();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: ISellerCitySalesBen) {
        return item.id;
    }

    registerChangeInSellerCities() {
        this.eventSubscriber = this.eventManager.subscribe('sellerCityListModification', response => this.loadAll());
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
