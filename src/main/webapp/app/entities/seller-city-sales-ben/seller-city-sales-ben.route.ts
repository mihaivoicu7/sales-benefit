import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { SellerCitySalesBen } from 'app/shared/model/seller-city-sales-ben.model';
import { SellerCitySalesBenService } from './seller-city-sales-ben.service';
import { SellerCitySalesBenComponent } from './seller-city-sales-ben.component';
import { SellerCitySalesBenDetailComponent } from './seller-city-sales-ben-detail.component';
import { SellerCitySalesBenUpdateComponent } from './seller-city-sales-ben-update.component';
import { SellerCitySalesBenDeletePopupComponent } from './seller-city-sales-ben-delete-dialog.component';
import { ISellerCitySalesBen } from 'app/shared/model/seller-city-sales-ben.model';

@Injectable({ providedIn: 'root' })
export class SellerCitySalesBenResolve implements Resolve<ISellerCitySalesBen> {
    constructor(private service: SellerCitySalesBenService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ISellerCitySalesBen> {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(
                filter((response: HttpResponse<SellerCitySalesBen>) => response.ok),
                map((sellerCity: HttpResponse<SellerCitySalesBen>) => sellerCity.body)
            );
        }
        return of(new SellerCitySalesBen());
    }
}

export const sellerCityRoute: Routes = [
    {
        path: '',
        component: SellerCitySalesBenComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'SellerCities'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/view',
        component: SellerCitySalesBenDetailComponent,
        resolve: {
            sellerCity: SellerCitySalesBenResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'SellerCities'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'new',
        component: SellerCitySalesBenUpdateComponent,
        resolve: {
            sellerCity: SellerCitySalesBenResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'SellerCities'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/edit',
        component: SellerCitySalesBenUpdateComponent,
        resolve: {
            sellerCity: SellerCitySalesBenResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'SellerCities'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const sellerCityPopupRoute: Routes = [
    {
        path: ':id/delete',
        component: SellerCitySalesBenDeletePopupComponent,
        resolve: {
            sellerCity: SellerCitySalesBenResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'SellerCities'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
