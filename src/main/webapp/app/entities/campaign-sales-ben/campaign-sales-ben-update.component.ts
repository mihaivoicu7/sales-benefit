import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { JhiAlertService } from 'ng-jhipster';
import { ICampaignSalesBen } from 'app/shared/model/campaign-sales-ben.model';
import { CampaignSalesBenService } from './campaign-sales-ben.service';
import { ISupplierSalesBen } from 'app/shared/model/supplier-sales-ben.model';
import { SupplierSalesBenService } from 'app/entities/supplier-sales-ben';

@Component({
    selector: 'jhi-campaign-sales-ben-update',
    templateUrl: './campaign-sales-ben-update.component.html'
})
export class CampaignSalesBenUpdateComponent implements OnInit {
    campaign: ICampaignSalesBen;
    isSaving: boolean;

    suppliers: ISupplierSalesBen[];
    fromDate: string;
    toDate: string;

    constructor(
        protected jhiAlertService: JhiAlertService,
        protected campaignService: CampaignSalesBenService,
        protected supplierService: SupplierSalesBenService,
        protected activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ campaign }) => {
            this.campaign = campaign;
            this.fromDate = this.campaign.fromDate != null ? this.campaign.fromDate.format(DATE_TIME_FORMAT) : null;
            this.toDate = this.campaign.toDate != null ? this.campaign.toDate.format(DATE_TIME_FORMAT) : null;
        });
        this.supplierService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<ISupplierSalesBen[]>) => mayBeOk.ok),
                map((response: HttpResponse<ISupplierSalesBen[]>) => response.body)
            )
            .subscribe((res: ISupplierSalesBen[]) => (this.suppliers = res), (res: HttpErrorResponse) => this.onError(res.message));
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        this.campaign.fromDate = this.fromDate != null ? moment(this.fromDate, DATE_TIME_FORMAT) : null;
        this.campaign.toDate = this.toDate != null ? moment(this.toDate, DATE_TIME_FORMAT) : null;
        if (this.campaign.id !== undefined) {
            this.subscribeToSaveResponse(this.campaignService.update(this.campaign));
        } else {
            this.subscribeToSaveResponse(this.campaignService.create(this.campaign));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<ICampaignSalesBen>>) {
        result.subscribe((res: HttpResponse<ICampaignSalesBen>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackSupplierById(index: number, item: ISupplierSalesBen) {
        return item.id;
    }
}
