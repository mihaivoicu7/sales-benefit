import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { ICampaignCountySalesBen } from 'app/shared/model/campaign-county-sales-ben.model';

type EntityResponseType = HttpResponse<ICampaignCountySalesBen>;
type EntityArrayResponseType = HttpResponse<ICampaignCountySalesBen[]>;

@Injectable({ providedIn: 'root' })
export class CampaignCountySalesBenService {
    public resourceUrl = SERVER_API_URL + 'api/campaign-counties';

    constructor(protected http: HttpClient) {}

    create(campaignCounty: ICampaignCountySalesBen): Observable<EntityResponseType> {
        return this.http.post<ICampaignCountySalesBen>(this.resourceUrl, campaignCounty, { observe: 'response' });
    }

    update(campaignCounty: ICampaignCountySalesBen): Observable<EntityResponseType> {
        return this.http.put<ICampaignCountySalesBen>(this.resourceUrl, campaignCounty, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<ICampaignCountySalesBen>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<ICampaignCountySalesBen[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
}
