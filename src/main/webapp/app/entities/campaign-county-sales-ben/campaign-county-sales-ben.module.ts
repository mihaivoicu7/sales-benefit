import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SalesBenefitSharedModule } from 'app/shared';
import {
    CampaignCountySalesBenComponent,
    CampaignCountySalesBenDetailComponent,
    CampaignCountySalesBenUpdateComponent,
    CampaignCountySalesBenDeletePopupComponent,
    CampaignCountySalesBenDeleteDialogComponent,
    campaignCountyRoute,
    campaignCountyPopupRoute
} from './';

const ENTITY_STATES = [...campaignCountyRoute, ...campaignCountyPopupRoute];

@NgModule({
    imports: [SalesBenefitSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        CampaignCountySalesBenComponent,
        CampaignCountySalesBenDetailComponent,
        CampaignCountySalesBenUpdateComponent,
        CampaignCountySalesBenDeleteDialogComponent,
        CampaignCountySalesBenDeletePopupComponent
    ],
    entryComponents: [
        CampaignCountySalesBenComponent,
        CampaignCountySalesBenUpdateComponent,
        CampaignCountySalesBenDeleteDialogComponent,
        CampaignCountySalesBenDeletePopupComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SalesBenefitCampaignCountySalesBenModule {}
