import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { ICampaignCountySalesBen } from 'app/shared/model/campaign-county-sales-ben.model';
import { CampaignCountySalesBenService } from './campaign-county-sales-ben.service';
import { ICampaignSalesBen } from 'app/shared/model/campaign-sales-ben.model';
import { CampaignSalesBenService } from 'app/entities/campaign-sales-ben';
import { ICountySalesBen } from 'app/shared/model/county-sales-ben.model';
import { CountySalesBenService } from 'app/entities/county-sales-ben';

@Component({
    selector: 'jhi-campaign-county-sales-ben-update',
    templateUrl: './campaign-county-sales-ben-update.component.html'
})
export class CampaignCountySalesBenUpdateComponent implements OnInit {
    campaignCounty: ICampaignCountySalesBen;
    isSaving: boolean;

    campaigns: ICampaignSalesBen[];

    counties: ICountySalesBen[];

    constructor(
        protected jhiAlertService: JhiAlertService,
        protected campaignCountyService: CampaignCountySalesBenService,
        protected campaignService: CampaignSalesBenService,
        protected countyService: CountySalesBenService,
        protected activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ campaignCounty }) => {
            this.campaignCounty = campaignCounty;
        });
        this.campaignService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<ICampaignSalesBen[]>) => mayBeOk.ok),
                map((response: HttpResponse<ICampaignSalesBen[]>) => response.body)
            )
            .subscribe((res: ICampaignSalesBen[]) => (this.campaigns = res), (res: HttpErrorResponse) => this.onError(res.message));
        this.countyService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<ICountySalesBen[]>) => mayBeOk.ok),
                map((response: HttpResponse<ICountySalesBen[]>) => response.body)
            )
            .subscribe((res: ICountySalesBen[]) => (this.counties = res), (res: HttpErrorResponse) => this.onError(res.message));
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.campaignCounty.id !== undefined) {
            this.subscribeToSaveResponse(this.campaignCountyService.update(this.campaignCounty));
        } else {
            this.subscribeToSaveResponse(this.campaignCountyService.create(this.campaignCounty));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<ICampaignCountySalesBen>>) {
        result.subscribe(
            (res: HttpResponse<ICampaignCountySalesBen>) => this.onSaveSuccess(),
            (res: HttpErrorResponse) => this.onSaveError()
        );
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackCampaignById(index: number, item: ICampaignSalesBen) {
        return item.id;
    }

    trackCountyById(index: number, item: ICountySalesBen) {
        return item.id;
    }
}
