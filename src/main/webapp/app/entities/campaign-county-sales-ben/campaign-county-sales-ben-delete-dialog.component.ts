import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ICampaignCountySalesBen } from 'app/shared/model/campaign-county-sales-ben.model';
import { CampaignCountySalesBenService } from './campaign-county-sales-ben.service';

@Component({
    selector: 'jhi-campaign-county-sales-ben-delete-dialog',
    templateUrl: './campaign-county-sales-ben-delete-dialog.component.html'
})
export class CampaignCountySalesBenDeleteDialogComponent {
    campaignCounty: ICampaignCountySalesBen;

    constructor(
        protected campaignCountyService: CampaignCountySalesBenService,
        public activeModal: NgbActiveModal,
        protected eventManager: JhiEventManager
    ) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.campaignCountyService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'campaignCountyListModification',
                content: 'Deleted an campaignCounty'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-campaign-county-sales-ben-delete-popup',
    template: ''
})
export class CampaignCountySalesBenDeletePopupComponent implements OnInit, OnDestroy {
    protected ngbModalRef: NgbModalRef;

    constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ campaignCounty }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(CampaignCountySalesBenDeleteDialogComponent as Component, {
                    size: 'lg',
                    backdrop: 'static'
                });
                this.ngbModalRef.componentInstance.campaignCounty = campaignCounty;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate(['/campaign-county-sales-ben', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate(['/campaign-county-sales-ben', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
