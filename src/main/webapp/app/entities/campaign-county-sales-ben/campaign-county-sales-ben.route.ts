import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { CampaignCountySalesBen } from 'app/shared/model/campaign-county-sales-ben.model';
import { CampaignCountySalesBenService } from './campaign-county-sales-ben.service';
import { CampaignCountySalesBenComponent } from './campaign-county-sales-ben.component';
import { CampaignCountySalesBenDetailComponent } from './campaign-county-sales-ben-detail.component';
import { CampaignCountySalesBenUpdateComponent } from './campaign-county-sales-ben-update.component';
import { CampaignCountySalesBenDeletePopupComponent } from './campaign-county-sales-ben-delete-dialog.component';
import { ICampaignCountySalesBen } from 'app/shared/model/campaign-county-sales-ben.model';

@Injectable({ providedIn: 'root' })
export class CampaignCountySalesBenResolve implements Resolve<ICampaignCountySalesBen> {
    constructor(private service: CampaignCountySalesBenService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ICampaignCountySalesBen> {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(
                filter((response: HttpResponse<CampaignCountySalesBen>) => response.ok),
                map((campaignCounty: HttpResponse<CampaignCountySalesBen>) => campaignCounty.body)
            );
        }
        return of(new CampaignCountySalesBen());
    }
}

export const campaignCountyRoute: Routes = [
    {
        path: '',
        component: CampaignCountySalesBenComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'CampaignCounties'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/view',
        component: CampaignCountySalesBenDetailComponent,
        resolve: {
            campaignCounty: CampaignCountySalesBenResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'CampaignCounties'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'new',
        component: CampaignCountySalesBenUpdateComponent,
        resolve: {
            campaignCounty: CampaignCountySalesBenResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'CampaignCounties'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/edit',
        component: CampaignCountySalesBenUpdateComponent,
        resolve: {
            campaignCounty: CampaignCountySalesBenResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'CampaignCounties'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const campaignCountyPopupRoute: Routes = [
    {
        path: ':id/delete',
        component: CampaignCountySalesBenDeletePopupComponent,
        resolve: {
            campaignCounty: CampaignCountySalesBenResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'CampaignCounties'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
