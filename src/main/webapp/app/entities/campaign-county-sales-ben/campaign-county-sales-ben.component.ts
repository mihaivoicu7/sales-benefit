import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ICampaignCountySalesBen } from 'app/shared/model/campaign-county-sales-ben.model';
import { AccountService } from 'app/core';
import { CampaignCountySalesBenService } from './campaign-county-sales-ben.service';

@Component({
    selector: 'jhi-campaign-county-sales-ben',
    templateUrl: './campaign-county-sales-ben.component.html'
})
export class CampaignCountySalesBenComponent implements OnInit, OnDestroy {
    campaignCounties: ICampaignCountySalesBen[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        protected campaignCountyService: CampaignCountySalesBenService,
        protected jhiAlertService: JhiAlertService,
        protected eventManager: JhiEventManager,
        protected accountService: AccountService
    ) {}

    loadAll() {
        this.campaignCountyService
            .query()
            .pipe(
                filter((res: HttpResponse<ICampaignCountySalesBen[]>) => res.ok),
                map((res: HttpResponse<ICampaignCountySalesBen[]>) => res.body)
            )
            .subscribe(
                (res: ICampaignCountySalesBen[]) => {
                    this.campaignCounties = res;
                },
                (res: HttpErrorResponse) => this.onError(res.message)
            );
    }

    ngOnInit() {
        this.loadAll();
        this.accountService.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInCampaignCounties();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: ICampaignCountySalesBen) {
        return item.id;
    }

    registerChangeInCampaignCounties() {
        this.eventSubscriber = this.eventManager.subscribe('campaignCountyListModification', response => this.loadAll());
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
