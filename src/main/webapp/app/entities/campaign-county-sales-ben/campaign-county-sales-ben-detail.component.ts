import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ICampaignCountySalesBen } from 'app/shared/model/campaign-county-sales-ben.model';

@Component({
    selector: 'jhi-campaign-county-sales-ben-detail',
    templateUrl: './campaign-county-sales-ben-detail.component.html'
})
export class CampaignCountySalesBenDetailComponent implements OnInit {
    campaignCounty: ICampaignCountySalesBen;

    constructor(protected activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ campaignCounty }) => {
            this.campaignCounty = campaignCounty;
        });
    }

    previousState() {
        window.history.back();
    }
}
