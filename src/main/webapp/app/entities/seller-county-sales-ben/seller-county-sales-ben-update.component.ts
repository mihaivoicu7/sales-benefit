import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { ISellerCountySalesBen } from 'app/shared/model/seller-county-sales-ben.model';
import { SellerCountySalesBenService } from './seller-county-sales-ben.service';
import { ISellerSalesBen } from 'app/shared/model/seller-sales-ben.model';
import { SellerSalesBenService } from 'app/entities/seller-sales-ben';
import { ICountySalesBen } from 'app/shared/model/county-sales-ben.model';
import { CountySalesBenService } from 'app/entities/county-sales-ben';

@Component({
    selector: 'jhi-seller-county-sales-ben-update',
    templateUrl: './seller-county-sales-ben-update.component.html'
})
export class SellerCountySalesBenUpdateComponent implements OnInit {
    sellerCounty: ISellerCountySalesBen;
    isSaving: boolean;

    sellers: ISellerSalesBen[];

    counties: ICountySalesBen[];

    constructor(
        protected jhiAlertService: JhiAlertService,
        protected sellerCountyService: SellerCountySalesBenService,
        protected sellerService: SellerSalesBenService,
        protected countyService: CountySalesBenService,
        protected activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ sellerCounty }) => {
            this.sellerCounty = sellerCounty;
        });
        this.sellerService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<ISellerSalesBen[]>) => mayBeOk.ok),
                map((response: HttpResponse<ISellerSalesBen[]>) => response.body)
            )
            .subscribe((res: ISellerSalesBen[]) => (this.sellers = res), (res: HttpErrorResponse) => this.onError(res.message));
        this.countyService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<ICountySalesBen[]>) => mayBeOk.ok),
                map((response: HttpResponse<ICountySalesBen[]>) => response.body)
            )
            .subscribe((res: ICountySalesBen[]) => (this.counties = res), (res: HttpErrorResponse) => this.onError(res.message));
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.sellerCounty.id !== undefined) {
            this.subscribeToSaveResponse(this.sellerCountyService.update(this.sellerCounty));
        } else {
            this.subscribeToSaveResponse(this.sellerCountyService.create(this.sellerCounty));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<ISellerCountySalesBen>>) {
        result.subscribe(
            (res: HttpResponse<ISellerCountySalesBen>) => this.onSaveSuccess(),
            (res: HttpErrorResponse) => this.onSaveError()
        );
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackSellerById(index: number, item: ISellerSalesBen) {
        return item.id;
    }

    trackCountyById(index: number, item: ICountySalesBen) {
        return item.id;
    }
}
