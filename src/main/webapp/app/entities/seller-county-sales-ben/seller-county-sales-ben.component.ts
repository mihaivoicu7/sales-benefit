import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ISellerCountySalesBen } from 'app/shared/model/seller-county-sales-ben.model';
import { AccountService } from 'app/core';
import { SellerCountySalesBenService } from './seller-county-sales-ben.service';

@Component({
    selector: 'jhi-seller-county-sales-ben',
    templateUrl: './seller-county-sales-ben.component.html'
})
export class SellerCountySalesBenComponent implements OnInit, OnDestroy {
    sellerCounties: ISellerCountySalesBen[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        protected sellerCountyService: SellerCountySalesBenService,
        protected jhiAlertService: JhiAlertService,
        protected eventManager: JhiEventManager,
        protected accountService: AccountService
    ) {}

    loadAll() {
        this.sellerCountyService
            .query()
            .pipe(
                filter((res: HttpResponse<ISellerCountySalesBen[]>) => res.ok),
                map((res: HttpResponse<ISellerCountySalesBen[]>) => res.body)
            )
            .subscribe(
                (res: ISellerCountySalesBen[]) => {
                    this.sellerCounties = res;
                },
                (res: HttpErrorResponse) => this.onError(res.message)
            );
    }

    ngOnInit() {
        this.loadAll();
        this.accountService.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInSellerCounties();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: ISellerCountySalesBen) {
        return item.id;
    }

    registerChangeInSellerCounties() {
        this.eventSubscriber = this.eventManager.subscribe('sellerCountyListModification', response => this.loadAll());
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
