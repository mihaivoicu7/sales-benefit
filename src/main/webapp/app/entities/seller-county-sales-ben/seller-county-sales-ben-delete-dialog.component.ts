import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ISellerCountySalesBen } from 'app/shared/model/seller-county-sales-ben.model';
import { SellerCountySalesBenService } from './seller-county-sales-ben.service';

@Component({
    selector: 'jhi-seller-county-sales-ben-delete-dialog',
    templateUrl: './seller-county-sales-ben-delete-dialog.component.html'
})
export class SellerCountySalesBenDeleteDialogComponent {
    sellerCounty: ISellerCountySalesBen;

    constructor(
        protected sellerCountyService: SellerCountySalesBenService,
        public activeModal: NgbActiveModal,
        protected eventManager: JhiEventManager
    ) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.sellerCountyService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'sellerCountyListModification',
                content: 'Deleted an sellerCounty'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-seller-county-sales-ben-delete-popup',
    template: ''
})
export class SellerCountySalesBenDeletePopupComponent implements OnInit, OnDestroy {
    protected ngbModalRef: NgbModalRef;

    constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ sellerCounty }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(SellerCountySalesBenDeleteDialogComponent as Component, {
                    size: 'lg',
                    backdrop: 'static'
                });
                this.ngbModalRef.componentInstance.sellerCounty = sellerCounty;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate(['/seller-county-sales-ben', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate(['/seller-county-sales-ben', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
