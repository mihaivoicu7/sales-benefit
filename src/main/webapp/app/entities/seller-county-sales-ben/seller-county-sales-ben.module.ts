import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SalesBenefitSharedModule } from 'app/shared';
import {
    SellerCountySalesBenComponent,
    SellerCountySalesBenDetailComponent,
    SellerCountySalesBenUpdateComponent,
    SellerCountySalesBenDeletePopupComponent,
    SellerCountySalesBenDeleteDialogComponent,
    sellerCountyRoute,
    sellerCountyPopupRoute
} from './';

const ENTITY_STATES = [...sellerCountyRoute, ...sellerCountyPopupRoute];

@NgModule({
    imports: [SalesBenefitSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        SellerCountySalesBenComponent,
        SellerCountySalesBenDetailComponent,
        SellerCountySalesBenUpdateComponent,
        SellerCountySalesBenDeleteDialogComponent,
        SellerCountySalesBenDeletePopupComponent
    ],
    entryComponents: [
        SellerCountySalesBenComponent,
        SellerCountySalesBenUpdateComponent,
        SellerCountySalesBenDeleteDialogComponent,
        SellerCountySalesBenDeletePopupComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SalesBenefitSellerCountySalesBenModule {}
