import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { SellerCountySalesBen } from 'app/shared/model/seller-county-sales-ben.model';
import { SellerCountySalesBenService } from './seller-county-sales-ben.service';
import { SellerCountySalesBenComponent } from './seller-county-sales-ben.component';
import { SellerCountySalesBenDetailComponent } from './seller-county-sales-ben-detail.component';
import { SellerCountySalesBenUpdateComponent } from './seller-county-sales-ben-update.component';
import { SellerCountySalesBenDeletePopupComponent } from './seller-county-sales-ben-delete-dialog.component';
import { ISellerCountySalesBen } from 'app/shared/model/seller-county-sales-ben.model';

@Injectable({ providedIn: 'root' })
export class SellerCountySalesBenResolve implements Resolve<ISellerCountySalesBen> {
    constructor(private service: SellerCountySalesBenService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ISellerCountySalesBen> {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(
                filter((response: HttpResponse<SellerCountySalesBen>) => response.ok),
                map((sellerCounty: HttpResponse<SellerCountySalesBen>) => sellerCounty.body)
            );
        }
        return of(new SellerCountySalesBen());
    }
}

export const sellerCountyRoute: Routes = [
    {
        path: '',
        component: SellerCountySalesBenComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'SellerCounties'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/view',
        component: SellerCountySalesBenDetailComponent,
        resolve: {
            sellerCounty: SellerCountySalesBenResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'SellerCounties'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'new',
        component: SellerCountySalesBenUpdateComponent,
        resolve: {
            sellerCounty: SellerCountySalesBenResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'SellerCounties'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/edit',
        component: SellerCountySalesBenUpdateComponent,
        resolve: {
            sellerCounty: SellerCountySalesBenResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'SellerCounties'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const sellerCountyPopupRoute: Routes = [
    {
        path: ':id/delete',
        component: SellerCountySalesBenDeletePopupComponent,
        resolve: {
            sellerCounty: SellerCountySalesBenResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'SellerCounties'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
