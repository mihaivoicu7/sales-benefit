import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ISellerCountySalesBen } from 'app/shared/model/seller-county-sales-ben.model';

@Component({
    selector: 'jhi-seller-county-sales-ben-detail',
    templateUrl: './seller-county-sales-ben-detail.component.html'
})
export class SellerCountySalesBenDetailComponent implements OnInit {
    sellerCounty: ISellerCountySalesBen;

    constructor(protected activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ sellerCounty }) => {
            this.sellerCounty = sellerCounty;
        });
    }

    previousState() {
        window.history.back();
    }
}
