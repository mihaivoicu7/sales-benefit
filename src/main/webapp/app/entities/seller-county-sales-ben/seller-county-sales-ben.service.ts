import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { ISellerCountySalesBen } from 'app/shared/model/seller-county-sales-ben.model';

type EntityResponseType = HttpResponse<ISellerCountySalesBen>;
type EntityArrayResponseType = HttpResponse<ISellerCountySalesBen[]>;

@Injectable({ providedIn: 'root' })
export class SellerCountySalesBenService {
    public resourceUrl = SERVER_API_URL + 'api/seller-counties';

    constructor(protected http: HttpClient) {}

    create(sellerCounty: ISellerCountySalesBen): Observable<EntityResponseType> {
        return this.http.post<ISellerCountySalesBen>(this.resourceUrl, sellerCounty, { observe: 'response' });
    }

    update(sellerCounty: ISellerCountySalesBen): Observable<EntityResponseType> {
        return this.http.put<ISellerCountySalesBen>(this.resourceUrl, sellerCounty, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<ISellerCountySalesBen>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<ISellerCountySalesBen[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
}
