import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { ISupplierCampaignSellerModel, SupplierCampaignSellerModel } from '../../shared/model/supplier-campaign-seller.model';

type EntityResponseType = HttpResponse<ISupplierCampaignSellerModel>;
type EntityArrayResponseType = HttpResponse<ISupplierCampaignSellerModel[]>;

@Injectable({ providedIn: 'root' })
export class SupplierCampaignSellerService {
    private resourceUrl = SERVER_API_URL + 'api/supplier/campaign-sellers';

    constructor(private http: HttpClient) {}

    create(campaignSeller: SupplierCampaignSellerModel): Observable<EntityResponseType> {
        return this.http.post<ISupplierCampaignSellerModel>(this.resourceUrl, campaignSeller, { observe: 'response' });
    }

    update(campaignSeller: SupplierCampaignSellerModel): Observable<EntityResponseType> {
        return this.http.put<ISupplierCampaignSellerModel>(this.resourceUrl, campaignSeller, { observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<ISupplierCampaignSellerModel>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    findAllCampaignsSellers(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<ISupplierCampaignSellerModel[]>(this.resourceUrl + '/campaign', { params: options, observe: 'response' });
    }

    findAllSellersByCampaignId(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<ISupplierCampaignSellerModel[]>(this.resourceUrl, { params: options, observe: 'response' });
    }
}
