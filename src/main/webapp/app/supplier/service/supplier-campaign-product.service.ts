import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { ICampaignProductSalesBen } from 'app/shared/model/campaign-product-sales-ben.model';
import { ISupplierCampaignProductModel, SupplierCampaignProductModel } from 'app/shared/model/supplier-campaign-product.model';

type EntityResponseType = HttpResponse<ICampaignProductSalesBen>;
type EntityArrayResponseType = HttpResponse<ICampaignProductSalesBen[]>;

@Injectable({ providedIn: 'root' })
export class SupplierCampaignProductService {
    private resourceUrl = SERVER_API_URL + 'api/supplier/campaign-products';

    constructor(private http: HttpClient) {}

    create(campaignProduct: SupplierCampaignProductModel): Observable<EntityResponseType> {
        return this.http.post<ISupplierCampaignProductModel>(this.resourceUrl, campaignProduct, { observe: 'response' });
    }

    update(campaignProduct: SupplierCampaignProductModel): Observable<EntityResponseType> {
        return this.http.put<ISupplierCampaignProductModel>(this.resourceUrl, campaignProduct, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<ISupplierCampaignProductModel>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<ISupplierCampaignProductModel[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    findAllCampaignsProducts(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<ISupplierCampaignProductModel[]>(this.resourceUrl + '/campaign', {
            params: options,
            observe: 'response'
        });
    }

    findAllProductsByCampaignId(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<ISupplierCampaignProductModel[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
}
