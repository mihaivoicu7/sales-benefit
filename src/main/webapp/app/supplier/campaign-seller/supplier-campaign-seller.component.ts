import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiAlertService, JhiParseLinks } from 'ng-jhipster';

import { ICampaignSellerSalesBen } from 'app/shared/model/campaign-seller-sales-ben.model';
import { AccountService } from 'app/core';
import { SupplierCampaignSellerModel } from 'app/shared/model/supplier-campaign-seller.model';
import { SupplierCampaignSellerService } from 'app/supplier/service/supplier-campaing-seller.service';
import { ICampaignSalesBen } from 'app/shared/model/campaign-sales-ben.model';
import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
    selector: 'jhi-supplier-campaign-seller',
    templateUrl: './supplier-campaign-seller.component.html'
})
export class SupplierCampaignSellerComponent implements OnInit, OnDestroy {
    campaignSellers: SupplierCampaignSellerModel[];
    // used for those cases where CampaignSeller.id == null, user edits and then cancels
    campaignSellersUnderEditing: SupplierCampaignSellerModel[];
    currentAccount: any;
    eventSubscriber: Subscription;
    itemsPerPage: any;
    page: any;
    campaign: ICampaignSalesBen;
    links: any;
    totalItems: any;
    routeData: any;
    previousPage: any;
    reverse: any;
    predicate: any;

    constructor(
        private campaignSellerService: SupplierCampaignSellerService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private accountService: AccountService,
        private parseLinks: JhiParseLinks,
        private activatedRoute: ActivatedRoute,
        private router: Router
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe(data => {
            this.page = data.pagingParams.page;
            this.previousPage = data.pagingParams.page;
            this.reverse = data.pagingParams.ascending;
            this.predicate = data.pagingParams.predicate;
            this.campaign = data.campaign;
        });
    }

    loadAll() {
        this.campaignSellerService
            .findAllSellersByCampaignId({
                page: this.page - 1,
                size: this.itemsPerPage,
                sort: this.sort(),
                campaignId: this.campaign.id
            })
            .subscribe(
                (res: HttpResponse<any>) => this.paginateCampaignSellers(res.body, res.headers),
                (res: HttpErrorResponse) => this.onError(res.message)
            );
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    transition() {
        this.router.navigate(['/campaign-seller-sales-ben', this.campaign.id], {
            queryParams: {
                page: this.page,
                size: this.itemsPerPage,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    ngOnInit() {
        this.loadAll();
        this.accountService.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInCampaignSellers();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: ICampaignSellerSalesBen) {
        return item.id;
    }

    registerChangeInCampaignSellers() {
        this.eventSubscriber = this.eventManager.subscribe('campaignSellerListModification', response => this.loadAll());
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    sort() {
        return [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
    }

    private paginateCampaignSellers(data: any, headers: HttpHeaders) {
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = parseInt(headers.get('X-Total-Count'), 10);
        this.campaignSellers = data;
        this.campaignSellersUnderEditing = [];
    }

    hasCampaign(campaignSeller: SupplierCampaignSellerModel) {
        return campaignSeller.campaignId != null;
    }

    addSellerInCampaign(added: boolean, campaignSeller: SupplierCampaignSellerModel) {
        campaignSeller.isInCampaign = added;
        campaignSeller.campaignId = added ? this.campaign.id : null;
    }

    editCampaignSeller(campaignSeller: SupplierCampaignSellerModel) {
        this.campaignSellersUnderEditing.push({ ...campaignSeller });
        campaignSeller.isInEditMode = true;
    }

    saveCampaignSeller(campaignSeller: SupplierCampaignSellerModel) {
        campaignSeller.isInEditMode = false;
        this.campaignSellersUnderEditing.splice(
            this.campaignSellersUnderEditing.findIndex(iterator => iterator.sellerId === campaignSeller.sellerId),
            1
        );

        if (campaignSeller.campaignId !== null) {
            if (campaignSeller.id == null) {
                this.campaignSellerService.create({ ...campaignSeller }).subscribe(response => {
                    campaignSeller.id = response.body.id;
                });
            } else {
                this.campaignSellerService.update({ ...campaignSeller }).subscribe();
            }
        } else if (campaignSeller.id !== null) {
            this.campaignSellerService.delete(campaignSeller.id).subscribe(() => {
                campaignSeller.id = null;
            });
        }
    }

    cancelEditingCampaignSeller(campaignSeller: SupplierCampaignSellerModel) {
        campaignSeller.isInEditMode = false;
        if (campaignSeller.id !== null) {
            this.campaignSellerService.find(campaignSeller.id).subscribe(response => {
                campaignSeller.campaignId = response.body.campaignId;
            });
        } else {
            // remove campaignSeller from our container
            this.campaignSellers.splice(this.campaignSellers.findIndex(iterator => iterator.sellerId === campaignSeller.sellerId), 1);

            // get the stashed campaignSeller before being edited
            const campaignSellerUnderEditing = this.campaignSellersUnderEditing.find(
                iterator => iterator.sellerId === campaignSeller.sellerId
            );
            // clone the version of campaignSeller before editing
            const unchangedCampaignSeller = { ...campaignSellerUnderEditing };
            // store the unchanged campaignSeller in our container
            this.campaignSellers.push(unchangedCampaignSeller);
        }

        // remove the campaignSeller from our stash
        this.campaignSellersUnderEditing.splice(
            this.campaignSellersUnderEditing.findIndex(iterator => iterator.sellerId === campaignSeller.sellerId),
            1
        );
    }
}
