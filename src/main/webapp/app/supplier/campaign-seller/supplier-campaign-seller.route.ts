import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { ICampaignSellerSalesBen } from 'app/shared/model/campaign-seller-sales-ben.model';
import { CampaignSalesBen } from '../../shared/model/campaign-sales-ben.model';
import { CampaignService } from '../service/campaign.service';
import { SupplierCampaignSellerComponent } from './supplier-campaign-seller.component';
import { JhiResolvePagingParams } from 'ng-jhipster';

@Injectable({ providedIn: 'root' })
export class SupplierCampaignSellerResolve implements Resolve<ICampaignSellerSalesBen> {
    constructor(private service: CampaignService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const id = route.params['campaignId'] ? route.params['campaignId'] : null;
        if (id) {
            return this.service.find(id).pipe(map((campaign: HttpResponse<CampaignSalesBen>) => campaign.body));
        }
        return of(new CampaignSalesBen());
    }
}

export const supplierCampaignSellerRoute: Routes = [
    {
        path: 'campaign-seller/:campaignId',
        component: SupplierCampaignSellerComponent,
        resolve: {
            pagingParams: JhiResolvePagingParams,
            campaign: SupplierCampaignSellerResolve
        },
        data: {
            authorities: ['ROLE_SUPPLIER'],
            defaultSort: 'id,asc',
            pageTitle: 'Campaign Sellers'
        },
        canActivate: [UserRouteAccessService]
    }
];
