import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SalesBenefitSharedModule } from 'app/shared';
import { supplierCampaignSellerRoute, SupplierCampaignSellerComponent } from './';

const ENTITY_STATES = [...supplierCampaignSellerRoute];

@NgModule({
    imports: [SalesBenefitSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [SupplierCampaignSellerComponent],
    entryComponents: [SupplierCampaignSellerComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SupplierCampaignSellerModule {}
