import { Component, OnDestroy, OnInit } from '@angular/core';
import { HttpErrorResponse, HttpHeaders, HttpResponse } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { JhiAlertService, JhiEventManager, JhiParseLinks } from 'ng-jhipster';

import { ITEMS_PER_PAGE } from 'app/shared';
import { SupplierCampaignProductService } from 'app/supplier/service/supplier-campaign-product.service';
import { ICampaignSalesBen } from 'app/shared/model/campaign-sales-ben.model';
import { SupplierCampaignProductModel } from 'app/shared/model/supplier-campaign-product.model';
import { AccountService } from 'app/core';
import { IFilterTypeModel } from 'app/shared/model/filter-type.model';
import { FilterTypeEnum } from 'app/shared/model/enums/filter-type.enum';

@Component({
    selector: 'jhi-supplier-campaign-product',
    templateUrl: './supplier-campaign-product.component.html'
})
export class SupplierCampaignProductComponent implements OnInit, OnDestroy {
    currentAccount: any;
    campaignProducts: SupplierCampaignProductModel[];
    // used for those cases where CampaignProduct.id == null, user edits and then cancels
    campaignProductsUnderEditing: SupplierCampaignProductModel[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    productSearch = '';
    searchInCampaign = false;
    campaign: ICampaignSalesBen;
    inputFilterArray: Array<IFilterTypeModel>;

    constructor(
        private campaignProductService: SupplierCampaignProductService,
        private parseLinks: JhiParseLinks,
        private jhiAlertService: JhiAlertService,
        private accountService: AccountService,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private eventManager: JhiEventManager
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe(data => {
            this.page = data.pagingParams.page;
            this.previousPage = data.pagingParams.page;
            this.reverse = data.pagingParams.ascending;
            this.predicate = data.pagingParams.predicate;
            this.campaign = data.campaign;
        });
    }

    loadAll() {
        this.campaignProductService
            .findAllProductsByCampaignId({
                page: this.page - 1,
                size: this.itemsPerPage,
                sort: this.sort(),
                campaignId: this.campaign.id,
                search: this.productSearch,
                inCampaign: this.searchInCampaign
            })
            .subscribe(
                (res: HttpResponse<any>) => this.paginateCampaignProducts(res.body, res.headers),
                (res: HttpErrorResponse) => this.onError(res.message)
            );
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    transition() {
        this.router.navigate(['/campaign-product-sales-ben', this.campaign.id], {
            queryParams: {
                page: this.page,
                size: this.itemsPerPage,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.router.navigate([
            '/campaign-product',
            this.campaign.id,
            {
                page: this.page,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        ]);
        this.loadAll();
    }

    filterChanged(filterModel: IFilterTypeModel) {
        switch (filterModel.type) {
            case FilterTypeEnum.Checkbox:
                this.searchInCampaign = filterModel.props.checked;
                break;
            case FilterTypeEnum.Search:
                this.productSearch = filterModel.props.value;
                break;
        }
        this.loadAll();
    }

    ngOnInit() {
        this.loadAll();
        this.accountService.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInCampaignProducts();
        this.inputFilterArray = new Array<IFilterTypeModel>();
        this.inputFilterArray.push({
            type: FilterTypeEnum.Search,
            name: 'search',
            placeholder: 'Nume Produs',
            tooltip: 'Nume Produs'
        });
        this.inputFilterArray.push({
            type: FilterTypeEnum.Checkbox,
            name: 'inCampaign',
            placeholder: 'In Campanie'
        });
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: SupplierCampaignProductModel) {
        return item.id;
    }

    registerChangeInCampaignProducts() {
        this.eventSubscriber = this.eventManager.subscribe('campaignProductListModification', response => this.loadAll());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'name') {
            result.push('name,asc');
        }
        return result;
    }

    private paginateCampaignProducts(data: any, headers: HttpHeaders) {
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = parseInt(headers.get('X-Total-Count'), 10);
        this.queryCount = this.totalItems;
        this.campaignProducts = data;
        this.campaignProductsUnderEditing = [];
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    hasCampaign(campaignProduct: SupplierCampaignProductModel) {
        return campaignProduct.campaignId != null;
    }

    editProduct(campaignProduct: SupplierCampaignProductModel) {
        this.campaignProductsUnderEditing.push({ ...campaignProduct });
        campaignProduct.isInEditMode = true;
    }

    addProductInCampaign(added: boolean, campaignProduct: SupplierCampaignProductModel) {
        campaignProduct.isInCampaign = added;
        if (added) {
            campaignProduct.campaignId = this.campaign.id;
        } else {
            campaignProduct.campaignId = null;
            campaignProduct.discount = null;
            campaignProduct.price = null;
        }
    }

    saveProduct(campaignProduct: SupplierCampaignProductModel) {
        campaignProduct.isInEditMode = false;
        this.campaignProductsUnderEditing.splice(
            this.campaignProductsUnderEditing.findIndex(iterator => iterator.productId === campaignProduct.productId),
            1
        );

        if (campaignProduct.campaignId !== null) {
            campaignProduct.campaignId = this.campaign.id;
            if (campaignProduct.id == null) {
                this.campaignProductService.create({ ...campaignProduct }).subscribe(response => {
                    campaignProduct.id = response.body.id;
                });
            } else {
                this.campaignProductService.update({ ...campaignProduct }).subscribe(response => {
                    campaignProduct.price = response.body.price;
                    campaignProduct.discount = response.body.discount;
                });
            }
        } else if (campaignProduct.id !== null) {
            this.campaignProductService.delete(campaignProduct.id).subscribe(() => {
                campaignProduct.id = null;
                campaignProduct.price = null;
                campaignProduct.discount = null;
            });
        }
    }

    cancelEditingCampaignProduct(campaignProduct: SupplierCampaignProductModel) {
        campaignProduct.isInEditMode = false;
        if (campaignProduct.id !== null) {
            this.campaignProductService.find(campaignProduct.id).subscribe(response => {
                campaignProduct.campaignId = response.body.campaignId;
                campaignProduct.price = response.body.price;
                campaignProduct.discount = response.body.discount;
            });
        } else {
            // remove campaignProduct from our container
            this.campaignProducts.splice(this.campaignProducts.findIndex(iterator => iterator.productId === campaignProduct.productId), 1);

            // get the stashed campaignProduct before being edited
            const campaignProductUnderEditing = this.campaignProductsUnderEditing.find(
                iterator => iterator.productId === campaignProduct.productId
            );
            // clone the version of campaignProduct before editing
            const unchangedCampaignProduct = { ...campaignProductUnderEditing };
            // store the unchanged campaignProduct in our container
            this.campaignProducts.push(unchangedCampaignProduct);
        }

        // remove the campaignProduct from our stash
        this.campaignProductsUnderEditing.splice(
            this.campaignProductsUnderEditing.findIndex(iterator => iterator.productId === campaignProduct.productId),
            1
        );
    }
}
