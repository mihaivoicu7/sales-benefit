import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ICampaignSalesBen } from 'app/shared/model/campaign-sales-ben.model';
import {PreviousState} from 'app/common/previous-state';
import {SupplierCampaignProductService} from "app/supplier/service/supplier-campaign-product.service";
import {CampaignProductSalesBen, ICampaignProductSalesBen} from "app/shared/model/campaign-product-sales-ben.model";

@Component({
    selector: 'jhi-campaign-detail',
    templateUrl: './supplier-campaign-detail.component.html',
    styleUrls: ['./supplier-campaign-detail.component.css']
})
export class SupplierCampaignDetailComponent extends PreviousState implements OnInit {
    campaign: ICampaignSalesBen;
    campaignProducts: ICampaignProductSalesBen[];
    constructor(private activatedRoute: ActivatedRoute, private campaignProductService: SupplierCampaignProductService) {
        super();
    }

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ campaign }) => {
            this.campaign = campaign;
            this.campaignProductService.findAllCampaignsProducts({
                campaignId:this.campaign.id
            }).subscribe(resp=> {
                this.campaignProducts = resp.body;
            });
        });
    }

}
