import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_FORMAT, DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { JhiAlertService } from 'ng-jhipster';
import { Router } from '@angular/router';

import { ICampaignSalesBen } from 'app/shared/model/campaign-sales-ben.model';
import { CampaignService } from '../service/campaign.service';
import { ISupplierSalesBen } from 'app/shared/model/supplier-sales-ben.model';
import { SupplierSalesBenService } from 'app/entities/supplier-sales-ben';
import { PreviousState } from 'app/common/previous-state';
import { Moment } from 'moment';

@Component({
    selector: 'jhi-campaign-update',
    templateUrl: './supplier-campaign-update.component.html'
})
export class SupplierCampaignUpdateComponent extends PreviousState implements OnInit {
    private _campaign: ICampaignSalesBen;
    isSaving: boolean;
    goToProducts: boolean;
    goToSellers: boolean;
    suppliers: ISupplierSalesBen[];
    fromDate: Moment;
    toDate: Moment;

    constructor(
        private jhiAlertService: JhiAlertService,
        private campaignService: CampaignService,
        private supplierService: SupplierSalesBenService,
        private activatedRoute: ActivatedRoute,
        private router: Router
    ) {
        super();
    }

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ campaign }) => {
            this.campaign = campaign;
        });
        this.supplierService.query().subscribe(
            (res: HttpResponse<ISupplierSalesBen[]>) => {
                this.suppliers = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    previousState() {
        window.history.back();
    }

    deleteCampaign() {
        if (this.campaign.id) {
            this.campaignService.delete(this.campaign.id).subscribe(() => {
                this.router.navigate(['/campaign']);
            });
        } else {
            this.router.navigate(['/campaign']);
        }
    }

    addProducts() {
        this.goToProducts = true;
        this.save();
    }

    addSellers() {
        this.goToSellers = true;
        this.save();
    }

    save() {
        this.isSaving = true;
        this.campaign.fromDate = this.fromDate;
        this.campaign.toDate = this.toDate;
        this.subscribeToSaveResponse(
            this.campaign.id !== undefined ? this.campaignService.update(this.campaign) : this.campaignService.create(this.campaign)
        );
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<ICampaignSalesBen>>) {
        result.subscribe((res: HttpResponse<ICampaignSalesBen>) => this.onSaveSuccess(res), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(res: any) {
        this.isSaving = false;
        this.router.navigate(
            this.goToProducts ? ['/campaign-product', res.body.id] : this.goToSellers ? ['/campaign-seller', res.body.id] : ['/campaign']
        );
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    get campaign() {
        return this._campaign;
    }

    set campaign(campaign: ICampaignSalesBen) {
        this._campaign = campaign;
        if (campaign.id) {
            this.fromDate = campaign.fromDate;
            this.toDate = campaign.toDate;
        } else {
            const currentDate = new Date();
            this.fromDate = moment(currentDate);
        }
    }

    ialamue($event: string) {
        console.log($event);
        const allFruits = ['Apple', 'Lemon', 'Lime', 'Orange', 'Strawberry'];
        return allFruits;
    }
}
