package com.mvoicu.sales.benefits.user.service;


import com.mvoicu.sales.benefits.domain.Notification;
import com.mvoicu.sales.benefits.repository.NotificationRepository;
import com.mvoicu.sales.benefits.service.dto.NotificationDTO;
import com.mvoicu.sales.benefits.service.mapper.NotificationMapper;
import com.mvoicu.sales.benefits.web.websocket.UserNotificationWebsocketService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing Notification.
 */
@Service
@Transactional
public class UserNotificationService {
    private final Logger log = LoggerFactory.getLogger(UserNotificationService.class);

    private final NotificationRepository notificationRepository;

    private final NotificationMapper notificationMapper;

    private UserNotificationWebsocketService notificationWebsocketService;

    public UserNotificationService(NotificationRepository notificationRepository, NotificationMapper notificationMapper) {
        this.notificationRepository = notificationRepository;
        this.notificationMapper = notificationMapper;
    }

    @Autowired
    public void setNotificationWebsocketService(UserNotificationWebsocketService notificationWebsocketService) {
        this.notificationWebsocketService = notificationWebsocketService;
    }

    /**
     * Save a new notification.
     *
     * @param notificationDTO the entity to save
     * @return the persisted entity
     */
    public NotificationDTO save(NotificationDTO notificationDTO) {
        log.debug("Request to update Notification : {}", notificationDTO);
        Notification notification = notificationMapper.toEntity(notificationDTO);
        notification = notificationRepository.save(notification);

        NotificationDTO savedNotificationDTO = notificationMapper.toDto(notification);
        notificationWebsocketService.alertUserAboutNewNotification(savedNotificationDTO);

        return savedNotificationDTO;
    }

    /**
     * Update an existing notification.
     *
     * @param notificationDTO the entity to update
     * @return the persisted entity
     */
    public NotificationDTO update(NotificationDTO notificationDTO) {
        log.debug("Request to save Notification : {}", notificationDTO);
        Notification notification = notificationMapper.toEntity(notificationDTO);
        notification = notificationRepository.save(notification);
        return notificationMapper.toDto(notification);
    }

    /**
     * Get all the notifications belonging to the current user.
     *
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<NotificationDTO> findAllByUserLogin(Pageable pageable) {
        log.debug("Request to get all Notifications");
        return notificationRepository.findByUserIsCurrentUser(pageable).stream()
            .map(notificationMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Count all the notifications belonging to the current user.
     *
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Integer countAllNotifications(String login) {
        log.debug("Request to count all Notifications");
        return notificationRepository.countNotificationsByUserLogin(login);
    }

    /**
     * Count all the unread notifications belonging to the current user.
     *
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Integer countUnreadNotifications(String login) {
        log.debug("Request to count all unread Notifications");
        return notificationRepository.countNotificationsByUserLoginAndHasBeenReadFalse(login);
    }


    /**
     * Get one notification by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public Optional<NotificationDTO> findOne(Long id) {
        log.debug("Request to get Notification : {}", id);
        return notificationRepository.findById(id)
            .map(notificationMapper::toDto);
    }

    /**
     * Delete the notification by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Notification : {}", id);
        notificationRepository.deleteById(id);
    }
}
