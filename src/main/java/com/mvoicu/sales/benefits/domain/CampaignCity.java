package com.mvoicu.sales.benefits.domain;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A CampaignCity.
 */
@Entity
@Table(name = "campaign_city")
public class CampaignCity implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @ManyToOne
    @JsonIgnoreProperties("campaignCities")
    private Campaign camapign;

    @ManyToOne
    @JsonIgnoreProperties("campaignCities")
    private City city;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Campaign getCamapign() {
        return camapign;
    }

    public CampaignCity camapign(Campaign campaign) {
        this.camapign = campaign;
        return this;
    }

    public void setCamapign(Campaign campaign) {
        this.camapign = campaign;
    }

    public City getCity() {
        return city;
    }

    public CampaignCity city(City city) {
        this.city = city;
        return this;
    }

    public void setCity(City city) {
        this.city = city;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CampaignCity campaignCity = (CampaignCity) o;
        if (campaignCity.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), campaignCity.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "CampaignCity{" +
            "id=" + getId() +
            "}";
    }
}
