package com.mvoicu.sales.benefits.domain;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A SellerSupplierRelation.
 */
@Entity
@Table(name = "seller_supplier_relation")
public class SellerSupplierRelation implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @ManyToOne
    @JsonIgnoreProperties("sellerSupplierRelations")
    private Supplier supplier;

    @ManyToOne
    @JsonIgnoreProperties("sellerSupplierRelations")
    private Seller seller;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Supplier getSupplier() {
        return supplier;
    }

    public SellerSupplierRelation supplier(Supplier supplier) {
        this.supplier = supplier;
        return this;
    }

    public void setSupplier(Supplier supplier) {
        this.supplier = supplier;
    }

    public Seller getSeller() {
        return seller;
    }

    public SellerSupplierRelation seller(Seller seller) {
        this.seller = seller;
        return this;
    }

    public void setSeller(Seller seller) {
        this.seller = seller;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        SellerSupplierRelation sellerSupplierRelation = (SellerSupplierRelation) o;
        if (sellerSupplierRelation.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), sellerSupplierRelation.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "SellerSupplierRelation{" +
            "id=" + getId() +
            "}";
    }
}
