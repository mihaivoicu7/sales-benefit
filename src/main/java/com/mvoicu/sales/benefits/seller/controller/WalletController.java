package com.mvoicu.sales.benefits.seller.controller;

import com.codahale.metrics.annotation.Timed;
import com.mvoicu.sales.benefits.seller.service.SellerWalletService;
import com.mvoicu.sales.benefits.service.dto.SellerWalletBalanceDTO;
import com.mvoicu.sales.benefits.service.dto.SellerWalletDTO;
import com.mvoicu.sales.benefits.web.rest.SellerWalletResource;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
@RequestMapping("/api/seller/wallet")
public class WalletController {

    private final Logger log = LoggerFactory.getLogger(SellerWalletResource.class);

    private final SellerWalletService sellerWalletService;

    public WalletController(SellerWalletService sellerWalletService) {
        this.sellerWalletService = sellerWalletService;
    }

    @GetMapping("/balance")
    @Timed
    public ResponseEntity<SellerWalletBalanceDTO> getSellerBalance() {
        return ResponseEntity.ok(sellerWalletService.getSellerWalletBallance());
    }
}
