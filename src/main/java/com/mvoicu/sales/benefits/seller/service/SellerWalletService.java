package com.mvoicu.sales.benefits.seller.service;

import com.mvoicu.sales.benefits.domain.SellerWallet;
import com.mvoicu.sales.benefits.repository.SellerWalletRepository;
import com.mvoicu.sales.benefits.security.SecurityUtils;
import com.mvoicu.sales.benefits.service.dto.SellerWalletBalanceDTO;
import org.springframework.stereotype.Service;

@Service
public class SellerWalletService {

    private final SellerWalletRepository repository;

    public SellerWalletService(SellerWalletRepository repository) {
        this.repository = repository;
    }

    public SellerWalletBalanceDTO getSellerWalletBallance() {
        SellerWallet sellerWallet = repository.findBySellerUserLogin(SecurityUtils.getCurrentUserLogin().get()).orElseGet(()->new SellerWallet(0d, 0d));
        return new SellerWalletBalanceDTO(sellerWallet.getAvailableAmount(), sellerWallet.getInPendingAmount());
    }
}
