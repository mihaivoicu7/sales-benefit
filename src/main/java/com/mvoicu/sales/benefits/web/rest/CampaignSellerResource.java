package com.mvoicu.sales.benefits.web.rest;
import com.mvoicu.sales.benefits.service.CampaignSellerService;
import com.mvoicu.sales.benefits.web.rest.errors.BadRequestAlertException;
import com.mvoicu.sales.benefits.web.rest.util.HeaderUtil;
import com.mvoicu.sales.benefits.web.rest.util.PaginationUtil;
import com.mvoicu.sales.benefits.service.dto.CampaignSellerDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing CampaignSeller.
 */
@RestController
@RequestMapping("/api")
public class CampaignSellerResource {

    private final Logger log = LoggerFactory.getLogger(CampaignSellerResource.class);

    private static final String ENTITY_NAME = "campaignSeller";

    private final CampaignSellerService campaignSellerService;

    public CampaignSellerResource(CampaignSellerService campaignSellerService) {
        this.campaignSellerService = campaignSellerService;
    }

    /**
     * POST  /campaign-sellers : Create a new campaignSeller.
     *
     * @param campaignSellerDTO the campaignSellerDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new campaignSellerDTO, or with status 400 (Bad Request) if the campaignSeller has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/campaign-sellers")
    public ResponseEntity<CampaignSellerDTO> createCampaignSeller(@RequestBody CampaignSellerDTO campaignSellerDTO) throws URISyntaxException {
        log.debug("REST request to save CampaignSeller : {}", campaignSellerDTO);
        if (campaignSellerDTO.getId() != null) {
            throw new BadRequestAlertException("A new campaignSeller cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CampaignSellerDTO result = campaignSellerService.save(campaignSellerDTO);
        return ResponseEntity.created(new URI("/api/campaign-sellers/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /campaign-sellers : Updates an existing campaignSeller.
     *
     * @param campaignSellerDTO the campaignSellerDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated campaignSellerDTO,
     * or with status 400 (Bad Request) if the campaignSellerDTO is not valid,
     * or with status 500 (Internal Server Error) if the campaignSellerDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/campaign-sellers")
    public ResponseEntity<CampaignSellerDTO> updateCampaignSeller(@RequestBody CampaignSellerDTO campaignSellerDTO) throws URISyntaxException {
        log.debug("REST request to update CampaignSeller : {}", campaignSellerDTO);
        if (campaignSellerDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        CampaignSellerDTO result = campaignSellerService.save(campaignSellerDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, campaignSellerDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /campaign-sellers : get all the campaignSellers.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of campaignSellers in body
     */
    @GetMapping("/campaign-sellers")
    public ResponseEntity<List<CampaignSellerDTO>> getAllCampaignSellers(Pageable pageable) {
        log.debug("REST request to get a page of CampaignSellers");
        Page<CampaignSellerDTO> page = campaignSellerService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/campaign-sellers");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * GET  /campaign-sellers/:id : get the "id" campaignSeller.
     *
     * @param id the id of the campaignSellerDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the campaignSellerDTO, or with status 404 (Not Found)
     */
    @GetMapping("/campaign-sellers/{id}")
    public ResponseEntity<CampaignSellerDTO> getCampaignSeller(@PathVariable Long id) {
        log.debug("REST request to get CampaignSeller : {}", id);
        Optional<CampaignSellerDTO> campaignSellerDTO = campaignSellerService.findOne(id);
        return ResponseUtil.wrapOrNotFound(campaignSellerDTO);
    }

    /**
     * DELETE  /campaign-sellers/:id : delete the "id" campaignSeller.
     *
     * @param id the id of the campaignSellerDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/campaign-sellers/{id}")
    public ResponseEntity<Void> deleteCampaignSeller(@PathVariable Long id) {
        log.debug("REST request to delete CampaignSeller : {}", id);
        campaignSellerService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
