package com.mvoicu.sales.benefits.web.rest;
import com.mvoicu.sales.benefits.service.CampaignCityService;
import com.mvoicu.sales.benefits.web.rest.errors.BadRequestAlertException;
import com.mvoicu.sales.benefits.web.rest.util.HeaderUtil;
import com.mvoicu.sales.benefits.service.dto.CampaignCityDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing CampaignCity.
 */
@RestController
@RequestMapping("/api")
public class CampaignCityResource {

    private final Logger log = LoggerFactory.getLogger(CampaignCityResource.class);

    private static final String ENTITY_NAME = "campaignCity";

    private final CampaignCityService campaignCityService;

    public CampaignCityResource(CampaignCityService campaignCityService) {
        this.campaignCityService = campaignCityService;
    }

    /**
     * POST  /campaign-cities : Create a new campaignCity.
     *
     * @param campaignCityDTO the campaignCityDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new campaignCityDTO, or with status 400 (Bad Request) if the campaignCity has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/campaign-cities")
    public ResponseEntity<CampaignCityDTO> createCampaignCity(@RequestBody CampaignCityDTO campaignCityDTO) throws URISyntaxException {
        log.debug("REST request to save CampaignCity : {}", campaignCityDTO);
        if (campaignCityDTO.getId() != null) {
            throw new BadRequestAlertException("A new campaignCity cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CampaignCityDTO result = campaignCityService.save(campaignCityDTO);
        return ResponseEntity.created(new URI("/api/campaign-cities/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /campaign-cities : Updates an existing campaignCity.
     *
     * @param campaignCityDTO the campaignCityDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated campaignCityDTO,
     * or with status 400 (Bad Request) if the campaignCityDTO is not valid,
     * or with status 500 (Internal Server Error) if the campaignCityDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/campaign-cities")
    public ResponseEntity<CampaignCityDTO> updateCampaignCity(@RequestBody CampaignCityDTO campaignCityDTO) throws URISyntaxException {
        log.debug("REST request to update CampaignCity : {}", campaignCityDTO);
        if (campaignCityDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        CampaignCityDTO result = campaignCityService.save(campaignCityDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, campaignCityDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /campaign-cities : get all the campaignCities.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of campaignCities in body
     */
    @GetMapping("/campaign-cities")
    public List<CampaignCityDTO> getAllCampaignCities() {
        log.debug("REST request to get all CampaignCities");
        return campaignCityService.findAll();
    }

    /**
     * GET  /campaign-cities/:id : get the "id" campaignCity.
     *
     * @param id the id of the campaignCityDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the campaignCityDTO, or with status 404 (Not Found)
     */
    @GetMapping("/campaign-cities/{id}")
    public ResponseEntity<CampaignCityDTO> getCampaignCity(@PathVariable Long id) {
        log.debug("REST request to get CampaignCity : {}", id);
        Optional<CampaignCityDTO> campaignCityDTO = campaignCityService.findOne(id);
        return ResponseUtil.wrapOrNotFound(campaignCityDTO);
    }

    /**
     * DELETE  /campaign-cities/:id : delete the "id" campaignCity.
     *
     * @param id the id of the campaignCityDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/campaign-cities/{id}")
    public ResponseEntity<Void> deleteCampaignCity(@PathVariable Long id) {
        log.debug("REST request to delete CampaignCity : {}", id);
        campaignCityService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
