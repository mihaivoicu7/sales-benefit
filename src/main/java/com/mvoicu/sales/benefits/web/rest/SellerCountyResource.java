package com.mvoicu.sales.benefits.web.rest;
import com.mvoicu.sales.benefits.service.SellerCountyService;
import com.mvoicu.sales.benefits.web.rest.errors.BadRequestAlertException;
import com.mvoicu.sales.benefits.web.rest.util.HeaderUtil;
import com.mvoicu.sales.benefits.service.dto.SellerCountyDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing SellerCounty.
 */
@RestController
@RequestMapping("/api")
public class SellerCountyResource {

    private final Logger log = LoggerFactory.getLogger(SellerCountyResource.class);

    private static final String ENTITY_NAME = "sellerCounty";

    private final SellerCountyService sellerCountyService;

    public SellerCountyResource(SellerCountyService sellerCountyService) {
        this.sellerCountyService = sellerCountyService;
    }

    /**
     * POST  /seller-counties : Create a new sellerCounty.
     *
     * @param sellerCountyDTO the sellerCountyDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new sellerCountyDTO, or with status 400 (Bad Request) if the sellerCounty has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/seller-counties")
    public ResponseEntity<SellerCountyDTO> createSellerCounty(@RequestBody SellerCountyDTO sellerCountyDTO) throws URISyntaxException {
        log.debug("REST request to save SellerCounty : {}", sellerCountyDTO);
        if (sellerCountyDTO.getId() != null) {
            throw new BadRequestAlertException("A new sellerCounty cannot already have an ID", ENTITY_NAME, "idexists");
        }
        SellerCountyDTO result = sellerCountyService.save(sellerCountyDTO);
        return ResponseEntity.created(new URI("/api/seller-counties/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /seller-counties : Updates an existing sellerCounty.
     *
     * @param sellerCountyDTO the sellerCountyDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated sellerCountyDTO,
     * or with status 400 (Bad Request) if the sellerCountyDTO is not valid,
     * or with status 500 (Internal Server Error) if the sellerCountyDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/seller-counties")
    public ResponseEntity<SellerCountyDTO> updateSellerCounty(@RequestBody SellerCountyDTO sellerCountyDTO) throws URISyntaxException {
        log.debug("REST request to update SellerCounty : {}", sellerCountyDTO);
        if (sellerCountyDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        SellerCountyDTO result = sellerCountyService.save(sellerCountyDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, sellerCountyDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /seller-counties : get all the sellerCounties.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of sellerCounties in body
     */
    @GetMapping("/seller-counties")
    public List<SellerCountyDTO> getAllSellerCounties() {
        log.debug("REST request to get all SellerCounties");
        return sellerCountyService.findAll();
    }

    /**
     * GET  /seller-counties/:id : get the "id" sellerCounty.
     *
     * @param id the id of the sellerCountyDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the sellerCountyDTO, or with status 404 (Not Found)
     */
    @GetMapping("/seller-counties/{id}")
    public ResponseEntity<SellerCountyDTO> getSellerCounty(@PathVariable Long id) {
        log.debug("REST request to get SellerCounty : {}", id);
        Optional<SellerCountyDTO> sellerCountyDTO = sellerCountyService.findOne(id);
        return ResponseUtil.wrapOrNotFound(sellerCountyDTO);
    }

    /**
     * DELETE  /seller-counties/:id : delete the "id" sellerCounty.
     *
     * @param id the id of the sellerCountyDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/seller-counties/{id}")
    public ResponseEntity<Void> deleteSellerCounty(@PathVariable Long id) {
        log.debug("REST request to delete SellerCounty : {}", id);
        sellerCountyService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
