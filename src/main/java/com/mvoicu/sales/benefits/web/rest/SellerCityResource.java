package com.mvoicu.sales.benefits.web.rest;
import com.mvoicu.sales.benefits.service.SellerCityService;
import com.mvoicu.sales.benefits.web.rest.errors.BadRequestAlertException;
import com.mvoicu.sales.benefits.web.rest.util.HeaderUtil;
import com.mvoicu.sales.benefits.service.dto.SellerCityDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing SellerCity.
 */
@RestController
@RequestMapping("/api")
public class SellerCityResource {

    private final Logger log = LoggerFactory.getLogger(SellerCityResource.class);

    private static final String ENTITY_NAME = "sellerCity";

    private final SellerCityService sellerCityService;

    public SellerCityResource(SellerCityService sellerCityService) {
        this.sellerCityService = sellerCityService;
    }

    /**
     * POST  /seller-cities : Create a new sellerCity.
     *
     * @param sellerCityDTO the sellerCityDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new sellerCityDTO, or with status 400 (Bad Request) if the sellerCity has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/seller-cities")
    public ResponseEntity<SellerCityDTO> createSellerCity(@RequestBody SellerCityDTO sellerCityDTO) throws URISyntaxException {
        log.debug("REST request to save SellerCity : {}", sellerCityDTO);
        if (sellerCityDTO.getId() != null) {
            throw new BadRequestAlertException("A new sellerCity cannot already have an ID", ENTITY_NAME, "idexists");
        }
        SellerCityDTO result = sellerCityService.save(sellerCityDTO);
        return ResponseEntity.created(new URI("/api/seller-cities/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /seller-cities : Updates an existing sellerCity.
     *
     * @param sellerCityDTO the sellerCityDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated sellerCityDTO,
     * or with status 400 (Bad Request) if the sellerCityDTO is not valid,
     * or with status 500 (Internal Server Error) if the sellerCityDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/seller-cities")
    public ResponseEntity<SellerCityDTO> updateSellerCity(@RequestBody SellerCityDTO sellerCityDTO) throws URISyntaxException {
        log.debug("REST request to update SellerCity : {}", sellerCityDTO);
        if (sellerCityDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        SellerCityDTO result = sellerCityService.save(sellerCityDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, sellerCityDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /seller-cities : get all the sellerCities.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of sellerCities in body
     */
    @GetMapping("/seller-cities")
    public List<SellerCityDTO> getAllSellerCities() {
        log.debug("REST request to get all SellerCities");
        return sellerCityService.findAll();
    }

    /**
     * GET  /seller-cities/:id : get the "id" sellerCity.
     *
     * @param id the id of the sellerCityDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the sellerCityDTO, or with status 404 (Not Found)
     */
    @GetMapping("/seller-cities/{id}")
    public ResponseEntity<SellerCityDTO> getSellerCity(@PathVariable Long id) {
        log.debug("REST request to get SellerCity : {}", id);
        Optional<SellerCityDTO> sellerCityDTO = sellerCityService.findOne(id);
        return ResponseUtil.wrapOrNotFound(sellerCityDTO);
    }

    /**
     * DELETE  /seller-cities/:id : delete the "id" sellerCity.
     *
     * @param id the id of the sellerCityDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/seller-cities/{id}")
    public ResponseEntity<Void> deleteSellerCity(@PathVariable Long id) {
        log.debug("REST request to delete SellerCity : {}", id);
        sellerCityService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
