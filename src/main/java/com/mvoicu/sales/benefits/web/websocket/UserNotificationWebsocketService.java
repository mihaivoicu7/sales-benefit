package com.mvoicu.sales.benefits.web.websocket;

import com.mvoicu.sales.benefits.service.dto.NotificationDTO;
import com.mvoicu.sales.benefits.user.service.UserNotificationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;

import java.security.Principal;
import java.util.List;

@Controller
public class UserNotificationWebsocketService {

    private static final Logger log = LoggerFactory.getLogger(UserNotificationWebsocketService.class);

    private final SimpMessagingTemplate messagingTemplate;

    private UserNotificationService userNotificationService;

    private Integer xFirstNotificationsToGet = 4;

    public UserNotificationWebsocketService(SimpMessagingTemplate messagingTemplate) {
        this.messagingTemplate = messagingTemplate;
    }

    @Autowired
    public void setUserNotificationService(UserNotificationService userNotificationService) {
        this.userNotificationService = userNotificationService;
    }

    @MessageMapping("/topic/userNotifications/getAll")
    public void getAllUserNotifications(@Header("size") Integer size, Principal principal) {
        List<NotificationDTO> notifications =
            userNotificationService.findAllByUserLogin(PageRequest.of(0, size));
        log.debug("Sending user notifications to user: " + principal.getName());

        xFirstNotificationsToGet = size;

        messagingTemplate.convertAndSendToUser(
            principal.getName(), "/queue/userNotifications/sendAll", notifications
        );
    }

    @MessageMapping("/topic/userNotifications/getUnreadCount")
    public void getAllUnreadUserNotificationsCount(Principal principal) {
        Integer unreadNotifications = userNotificationService.countUnreadNotifications(principal.getName());
        log.debug("Sending unread user notifications count to user: " + principal.getName());

        messagingTemplate.convertAndSendToUser(
            principal.getName(), "/queue/userNotifications/sendUnreadCount", unreadNotifications
        );
    }

    @MessageMapping("/topic/userNotifications/update")
    public void updateNotification(@Header("size") Integer size, @Payload NotificationDTO notificationDTO,
                                   Principal principal) {
        userNotificationService.save(notificationDTO);

        getAllUnreadUserNotificationsCount(principal);
        getAllUserNotifications(size, principal);
    }

    public void alertUserAboutNewNotification(NotificationDTO notificationDTO) {
        Principal principal = SecurityContextHolder.getContext().getAuthentication();

        messagingTemplate.convertAndSendToUser(
            principal.getName(), "/queue/userNotifications/sendNewNotification", notificationDTO
        );

        getAllUnreadUserNotificationsCount(principal);
        getAllUserNotifications(xFirstNotificationsToGet, principal);
    }
}
