package com.mvoicu.sales.benefits.web.rest;
import com.mvoicu.sales.benefits.service.CampaignCountyService;
import com.mvoicu.sales.benefits.web.rest.errors.BadRequestAlertException;
import com.mvoicu.sales.benefits.web.rest.util.HeaderUtil;
import com.mvoicu.sales.benefits.service.dto.CampaignCountyDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing CampaignCounty.
 */
@RestController
@RequestMapping("/api")
public class CampaignCountyResource {

    private final Logger log = LoggerFactory.getLogger(CampaignCountyResource.class);

    private static final String ENTITY_NAME = "campaignCounty";

    private final CampaignCountyService campaignCountyService;

    public CampaignCountyResource(CampaignCountyService campaignCountyService) {
        this.campaignCountyService = campaignCountyService;
    }

    /**
     * POST  /campaign-counties : Create a new campaignCounty.
     *
     * @param campaignCountyDTO the campaignCountyDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new campaignCountyDTO, or with status 400 (Bad Request) if the campaignCounty has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/campaign-counties")
    public ResponseEntity<CampaignCountyDTO> createCampaignCounty(@RequestBody CampaignCountyDTO campaignCountyDTO) throws URISyntaxException {
        log.debug("REST request to save CampaignCounty : {}", campaignCountyDTO);
        if (campaignCountyDTO.getId() != null) {
            throw new BadRequestAlertException("A new campaignCounty cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CampaignCountyDTO result = campaignCountyService.save(campaignCountyDTO);
        return ResponseEntity.created(new URI("/api/campaign-counties/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /campaign-counties : Updates an existing campaignCounty.
     *
     * @param campaignCountyDTO the campaignCountyDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated campaignCountyDTO,
     * or with status 400 (Bad Request) if the campaignCountyDTO is not valid,
     * or with status 500 (Internal Server Error) if the campaignCountyDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/campaign-counties")
    public ResponseEntity<CampaignCountyDTO> updateCampaignCounty(@RequestBody CampaignCountyDTO campaignCountyDTO) throws URISyntaxException {
        log.debug("REST request to update CampaignCounty : {}", campaignCountyDTO);
        if (campaignCountyDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        CampaignCountyDTO result = campaignCountyService.save(campaignCountyDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, campaignCountyDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /campaign-counties : get all the campaignCounties.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of campaignCounties in body
     */
    @GetMapping("/campaign-counties")
    public List<CampaignCountyDTO> getAllCampaignCounties() {
        log.debug("REST request to get all CampaignCounties");
        return campaignCountyService.findAll();
    }

    /**
     * GET  /campaign-counties/:id : get the "id" campaignCounty.
     *
     * @param id the id of the campaignCountyDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the campaignCountyDTO, or with status 404 (Not Found)
     */
    @GetMapping("/campaign-counties/{id}")
    public ResponseEntity<CampaignCountyDTO> getCampaignCounty(@PathVariable Long id) {
        log.debug("REST request to get CampaignCounty : {}", id);
        Optional<CampaignCountyDTO> campaignCountyDTO = campaignCountyService.findOne(id);
        return ResponseUtil.wrapOrNotFound(campaignCountyDTO);
    }

    /**
     * DELETE  /campaign-counties/:id : delete the "id" campaignCounty.
     *
     * @param id the id of the campaignCountyDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/campaign-counties/{id}")
    public ResponseEntity<Void> deleteCampaignCounty(@PathVariable Long id) {
        log.debug("REST request to delete CampaignCounty : {}", id);
        campaignCountyService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
