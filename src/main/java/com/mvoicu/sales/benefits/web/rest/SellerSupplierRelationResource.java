package com.mvoicu.sales.benefits.web.rest;
import com.mvoicu.sales.benefits.service.SellerSupplierRelationService;
import com.mvoicu.sales.benefits.web.rest.errors.BadRequestAlertException;
import com.mvoicu.sales.benefits.web.rest.util.HeaderUtil;
import com.mvoicu.sales.benefits.service.dto.SellerSupplierRelationDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing SellerSupplierRelation.
 */
@RestController
@RequestMapping("/api")
public class SellerSupplierRelationResource {

    private final Logger log = LoggerFactory.getLogger(SellerSupplierRelationResource.class);

    private static final String ENTITY_NAME = "sellerSupplierRelation";

    private final SellerSupplierRelationService sellerSupplierRelationService;

    public SellerSupplierRelationResource(SellerSupplierRelationService sellerSupplierRelationService) {
        this.sellerSupplierRelationService = sellerSupplierRelationService;
    }

    /**
     * POST  /seller-supplier-relations : Create a new sellerSupplierRelation.
     *
     * @param sellerSupplierRelationDTO the sellerSupplierRelationDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new sellerSupplierRelationDTO, or with status 400 (Bad Request) if the sellerSupplierRelation has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/seller-supplier-relations")
    public ResponseEntity<SellerSupplierRelationDTO> createSellerSupplierRelation(@RequestBody SellerSupplierRelationDTO sellerSupplierRelationDTO) throws URISyntaxException {
        log.debug("REST request to save SellerSupplierRelation : {}", sellerSupplierRelationDTO);
        if (sellerSupplierRelationDTO.getId() != null) {
            throw new BadRequestAlertException("A new sellerSupplierRelation cannot already have an ID", ENTITY_NAME, "idexists");
        }
        SellerSupplierRelationDTO result = sellerSupplierRelationService.save(sellerSupplierRelationDTO);
        return ResponseEntity.created(new URI("/api/seller-supplier-relations/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /seller-supplier-relations : Updates an existing sellerSupplierRelation.
     *
     * @param sellerSupplierRelationDTO the sellerSupplierRelationDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated sellerSupplierRelationDTO,
     * or with status 400 (Bad Request) if the sellerSupplierRelationDTO is not valid,
     * or with status 500 (Internal Server Error) if the sellerSupplierRelationDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/seller-supplier-relations")
    public ResponseEntity<SellerSupplierRelationDTO> updateSellerSupplierRelation(@RequestBody SellerSupplierRelationDTO sellerSupplierRelationDTO) throws URISyntaxException {
        log.debug("REST request to update SellerSupplierRelation : {}", sellerSupplierRelationDTO);
        if (sellerSupplierRelationDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        SellerSupplierRelationDTO result = sellerSupplierRelationService.save(sellerSupplierRelationDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, sellerSupplierRelationDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /seller-supplier-relations : get all the sellerSupplierRelations.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of sellerSupplierRelations in body
     */
    @GetMapping("/seller-supplier-relations")
    public List<SellerSupplierRelationDTO> getAllSellerSupplierRelations() {
        log.debug("REST request to get all SellerSupplierRelations");
        return sellerSupplierRelationService.findAll();
    }

    /**
     * GET  /seller-supplier-relations/:id : get the "id" sellerSupplierRelation.
     *
     * @param id the id of the sellerSupplierRelationDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the sellerSupplierRelationDTO, or with status 404 (Not Found)
     */
    @GetMapping("/seller-supplier-relations/{id}")
    public ResponseEntity<SellerSupplierRelationDTO> getSellerSupplierRelation(@PathVariable Long id) {
        log.debug("REST request to get SellerSupplierRelation : {}", id);
        Optional<SellerSupplierRelationDTO> sellerSupplierRelationDTO = sellerSupplierRelationService.findOne(id);
        return ResponseUtil.wrapOrNotFound(sellerSupplierRelationDTO);
    }

    /**
     * DELETE  /seller-supplier-relations/:id : delete the "id" sellerSupplierRelation.
     *
     * @param id the id of the sellerSupplierRelationDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/seller-supplier-relations/{id}")
    public ResponseEntity<Void> deleteSellerSupplierRelation(@PathVariable Long id) {
        log.debug("REST request to delete SellerSupplierRelation : {}", id);
        sellerSupplierRelationService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
