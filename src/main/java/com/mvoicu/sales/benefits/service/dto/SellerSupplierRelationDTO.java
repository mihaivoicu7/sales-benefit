package com.mvoicu.sales.benefits.service.dto;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the SellerSupplierRelation entity.
 */
public class SellerSupplierRelationDTO implements Serializable {

    private Long id;


    private Long supplierId;

    private Long sellerId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(Long supplierId) {
        this.supplierId = supplierId;
    }

    public Long getSellerId() {
        return sellerId;
    }

    public void setSellerId(Long sellerId) {
        this.sellerId = sellerId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SellerSupplierRelationDTO sellerSupplierRelationDTO = (SellerSupplierRelationDTO) o;
        if (sellerSupplierRelationDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), sellerSupplierRelationDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "SellerSupplierRelationDTO{" +
            "id=" + getId() +
            ", supplier=" + getSupplierId() +
            ", seller=" + getSellerId() +
            "}";
    }
}
