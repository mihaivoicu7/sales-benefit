package com.mvoicu.sales.benefits.service.mapper;

import com.mvoicu.sales.benefits.domain.CampaignProduct;
import com.mvoicu.sales.benefits.service.dto.CampaignProductDTO;
import com.mvoicu.sales.benefits.service.dto.SupplierCampaignProductDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Mapper for the entity CampaignProduct and its DTO CampaignProductDTO.
 */
@Mapper(componentModel = "spring", uses = {CampaignMapper.class, ProductMapper.class})
public interface SupplierCampaignProductMapper extends EntityMapper<SupplierCampaignProductDTO, CampaignProduct> {

    @Mapping(source = "campaign.id", target = "campaignId")
    @Mapping(source = "product.id", target = "productId")
    @Mapping(source = "product.name", target = "productName")
    @Mapping(source = "product.description", target = "productDescription")
    SupplierCampaignProductDTO toDto(CampaignProduct campaignProduct);

    @Mapping(source = "campaignId", target = "campaign")
    @Mapping(source = "productId", target = "product")
    CampaignProduct toEntity(SupplierCampaignProductDTO campaignProductDTO);

    default CampaignProduct fromId(Long id) {
        if (id == null) {
            return null;
        }
        CampaignProduct campaignProduct = new CampaignProduct();
        campaignProduct.setId(id);
        return campaignProduct;
    }
}
