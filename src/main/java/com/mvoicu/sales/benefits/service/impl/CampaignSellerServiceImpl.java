package com.mvoicu.sales.benefits.service.impl;

import com.mvoicu.sales.benefits.service.CampaignSellerService;
import com.mvoicu.sales.benefits.domain.CampaignSeller;
import com.mvoicu.sales.benefits.repository.CampaignSellerRepository;
import com.mvoicu.sales.benefits.service.dto.CampaignSellerDTO;
import com.mvoicu.sales.benefits.service.mapper.CampaignSellerMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing CampaignSeller.
 */
@Service
@Transactional
public class CampaignSellerServiceImpl implements CampaignSellerService {

    private final Logger log = LoggerFactory.getLogger(CampaignSellerServiceImpl.class);

    private final CampaignSellerRepository campaignSellerRepository;

    private final CampaignSellerMapper campaignSellerMapper;

    public CampaignSellerServiceImpl(CampaignSellerRepository campaignSellerRepository, CampaignSellerMapper campaignSellerMapper) {
        this.campaignSellerRepository = campaignSellerRepository;
        this.campaignSellerMapper = campaignSellerMapper;
    }

    /**
     * Save a campaignSeller.
     *
     * @param campaignSellerDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public CampaignSellerDTO save(CampaignSellerDTO campaignSellerDTO) {
        log.debug("Request to save CampaignSeller : {}", campaignSellerDTO);
        CampaignSeller campaignSeller = campaignSellerMapper.toEntity(campaignSellerDTO);
        campaignSeller = campaignSellerRepository.save(campaignSeller);
        return campaignSellerMapper.toDto(campaignSeller);
    }

    /**
     * Get all the campaignSellers.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<CampaignSellerDTO> findAll(Pageable pageable) {
        log.debug("Request to get all CampaignSellers");
        return campaignSellerRepository.findAll(pageable)
            .map(campaignSellerMapper::toDto);
    }


    /**
     * Get one campaignSeller by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<CampaignSellerDTO> findOne(Long id) {
        log.debug("Request to get CampaignSeller : {}", id);
        return campaignSellerRepository.findById(id)
            .map(campaignSellerMapper::toDto);
    }

    /**
     * Delete the campaignSeller by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete CampaignSeller : {}", id);        campaignSellerRepository.deleteById(id);
    }
}
