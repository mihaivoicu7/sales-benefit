package com.mvoicu.sales.benefits.service.mapper;

import com.mvoicu.sales.benefits.domain.*;
import com.mvoicu.sales.benefits.service.dto.CampaignCountyDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity CampaignCounty and its DTO CampaignCountyDTO.
 */
@Mapper(componentModel = "spring", uses = {CampaignMapper.class, CountyMapper.class})
public interface CampaignCountyMapper extends EntityMapper<CampaignCountyDTO, CampaignCounty> {

    @Mapping(source = "campaign.id", target = "campaignId")
    @Mapping(source = "county.id", target = "countyId")
    CampaignCountyDTO toDto(CampaignCounty campaignCounty);

    @Mapping(source = "campaignId", target = "campaign")
    @Mapping(source = "countyId", target = "county")
    CampaignCounty toEntity(CampaignCountyDTO campaignCountyDTO);

    default CampaignCounty fromId(Long id) {
        if (id == null) {
            return null;
        }
        CampaignCounty campaignCounty = new CampaignCounty();
        campaignCounty.setId(id);
        return campaignCounty;
    }
}
