package com.mvoicu.sales.benefits.service.impl;

import com.mvoicu.sales.benefits.service.SellerCityService;
import com.mvoicu.sales.benefits.domain.SellerCity;
import com.mvoicu.sales.benefits.repository.SellerCityRepository;
import com.mvoicu.sales.benefits.service.dto.SellerCityDTO;
import com.mvoicu.sales.benefits.service.mapper.SellerCityMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing SellerCity.
 */
@Service
@Transactional
public class SellerCityServiceImpl implements SellerCityService {

    private final Logger log = LoggerFactory.getLogger(SellerCityServiceImpl.class);

    private final SellerCityRepository sellerCityRepository;

    private final SellerCityMapper sellerCityMapper;

    public SellerCityServiceImpl(SellerCityRepository sellerCityRepository, SellerCityMapper sellerCityMapper) {
        this.sellerCityRepository = sellerCityRepository;
        this.sellerCityMapper = sellerCityMapper;
    }

    /**
     * Save a sellerCity.
     *
     * @param sellerCityDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public SellerCityDTO save(SellerCityDTO sellerCityDTO) {
        log.debug("Request to save SellerCity : {}", sellerCityDTO);
        SellerCity sellerCity = sellerCityMapper.toEntity(sellerCityDTO);
        sellerCity = sellerCityRepository.save(sellerCity);
        return sellerCityMapper.toDto(sellerCity);
    }

    /**
     * Get all the sellerCities.
     *
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<SellerCityDTO> findAll() {
        log.debug("Request to get all SellerCities");
        return sellerCityRepository.findAll().stream()
            .map(sellerCityMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    /**
     * Get one sellerCity by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<SellerCityDTO> findOne(Long id) {
        log.debug("Request to get SellerCity : {}", id);
        return sellerCityRepository.findById(id)
            .map(sellerCityMapper::toDto);
    }

    /**
     * Delete the sellerCity by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete SellerCity : {}", id);        sellerCityRepository.deleteById(id);
    }
}
