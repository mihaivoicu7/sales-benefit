package com.mvoicu.sales.benefits.service;

import com.mvoicu.sales.benefits.service.dto.SellerCountyDTO;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing SellerCounty.
 */
public interface SellerCountyService {

    /**
     * Save a sellerCounty.
     *
     * @param sellerCountyDTO the entity to save
     * @return the persisted entity
     */
    SellerCountyDTO save(SellerCountyDTO sellerCountyDTO);

    /**
     * Get all the sellerCounties.
     *
     * @return the list of entities
     */
    List<SellerCountyDTO> findAll();


    /**
     * Get the "id" sellerCounty.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<SellerCountyDTO> findOne(Long id);

    /**
     * Delete the "id" sellerCounty.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
