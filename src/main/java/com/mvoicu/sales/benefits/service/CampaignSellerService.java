package com.mvoicu.sales.benefits.service;

import com.mvoicu.sales.benefits.service.dto.CampaignSellerDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing CampaignSeller.
 */
public interface CampaignSellerService {

    /**
     * Save a campaignSeller.
     *
     * @param campaignSellerDTO the entity to save
     * @return the persisted entity
     */
    CampaignSellerDTO save(CampaignSellerDTO campaignSellerDTO);

    /**
     * Get all the campaignSellers.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<CampaignSellerDTO> findAll(Pageable pageable);


    /**
     * Get the "id" campaignSeller.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<CampaignSellerDTO> findOne(Long id);

    /**
     * Delete the "id" campaignSeller.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
