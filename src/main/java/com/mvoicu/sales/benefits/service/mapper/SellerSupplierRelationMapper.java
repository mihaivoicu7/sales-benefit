package com.mvoicu.sales.benefits.service.mapper;

import com.mvoicu.sales.benefits.domain.*;
import com.mvoicu.sales.benefits.service.dto.SellerSupplierRelationDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity SellerSupplierRelation and its DTO SellerSupplierRelationDTO.
 */
@Mapper(componentModel = "spring", uses = {SupplierMapper.class, SellerMapper.class})
public interface SellerSupplierRelationMapper extends EntityMapper<SellerSupplierRelationDTO, SellerSupplierRelation> {

    @Mapping(source = "supplier.id", target = "supplierId")
    @Mapping(source = "seller.id", target = "sellerId")
    SellerSupplierRelationDTO toDto(SellerSupplierRelation sellerSupplierRelation);

    @Mapping(source = "supplierId", target = "supplier")
    @Mapping(source = "sellerId", target = "seller")
    SellerSupplierRelation toEntity(SellerSupplierRelationDTO sellerSupplierRelationDTO);

    default SellerSupplierRelation fromId(Long id) {
        if (id == null) {
            return null;
        }
        SellerSupplierRelation sellerSupplierRelation = new SellerSupplierRelation();
        sellerSupplierRelation.setId(id);
        return sellerSupplierRelation;
    }
}
