package com.mvoicu.sales.benefits.service.impl;

import com.mvoicu.sales.benefits.service.CampaignCountyService;
import com.mvoicu.sales.benefits.domain.CampaignCounty;
import com.mvoicu.sales.benefits.repository.CampaignCountyRepository;
import com.mvoicu.sales.benefits.service.dto.CampaignCountyDTO;
import com.mvoicu.sales.benefits.service.mapper.CampaignCountyMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing CampaignCounty.
 */
@Service
@Transactional
public class CampaignCountyServiceImpl implements CampaignCountyService {

    private final Logger log = LoggerFactory.getLogger(CampaignCountyServiceImpl.class);

    private final CampaignCountyRepository campaignCountyRepository;

    private final CampaignCountyMapper campaignCountyMapper;

    public CampaignCountyServiceImpl(CampaignCountyRepository campaignCountyRepository, CampaignCountyMapper campaignCountyMapper) {
        this.campaignCountyRepository = campaignCountyRepository;
        this.campaignCountyMapper = campaignCountyMapper;
    }

    /**
     * Save a campaignCounty.
     *
     * @param campaignCountyDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public CampaignCountyDTO save(CampaignCountyDTO campaignCountyDTO) {
        log.debug("Request to save CampaignCounty : {}", campaignCountyDTO);
        CampaignCounty campaignCounty = campaignCountyMapper.toEntity(campaignCountyDTO);
        campaignCounty = campaignCountyRepository.save(campaignCounty);
        return campaignCountyMapper.toDto(campaignCounty);
    }

    /**
     * Get all the campaignCounties.
     *
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<CampaignCountyDTO> findAll() {
        log.debug("Request to get all CampaignCounties");
        return campaignCountyRepository.findAll().stream()
            .map(campaignCountyMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    /**
     * Get one campaignCounty by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<CampaignCountyDTO> findOne(Long id) {
        log.debug("Request to get CampaignCounty : {}", id);
        return campaignCountyRepository.findById(id)
            .map(campaignCountyMapper::toDto);
    }

    /**
     * Delete the campaignCounty by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete CampaignCounty : {}", id);        campaignCountyRepository.deleteById(id);
    }
}
