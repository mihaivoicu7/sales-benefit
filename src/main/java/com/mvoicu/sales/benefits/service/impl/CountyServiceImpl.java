package com.mvoicu.sales.benefits.service.impl;

import com.mvoicu.sales.benefits.service.CountyService;
import com.mvoicu.sales.benefits.domain.County;
import com.mvoicu.sales.benefits.repository.CountyRepository;
import com.mvoicu.sales.benefits.service.dto.CountyDTO;
import com.mvoicu.sales.benefits.service.mapper.CountyMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing County.
 */
@Service
@Transactional
public class CountyServiceImpl implements CountyService {

    private final Logger log = LoggerFactory.getLogger(CountyServiceImpl.class);

    private final CountyRepository countyRepository;

    private final CountyMapper countyMapper;

    public CountyServiceImpl(CountyRepository countyRepository, CountyMapper countyMapper) {
        this.countyRepository = countyRepository;
        this.countyMapper = countyMapper;
    }

    /**
     * Save a county.
     *
     * @param countyDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public CountyDTO save(CountyDTO countyDTO) {
        log.debug("Request to save County : {}", countyDTO);

        County county = countyMapper.toEntity(countyDTO);
        county = countyRepository.save(county);
        return countyMapper.toDto(county);
    }

    /**
     * Get all the counties.
     *
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<CountyDTO> findAll() {
        log.debug("Request to get all Counties");
        return countyRepository.findAll().stream()
            .map(countyMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    /**
     * Get one county by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<CountyDTO> findOne(Long id) {
        log.debug("Request to get County : {}", id);
        return countyRepository.findById(id)
            .map(countyMapper::toDto);
    }

    /**
     * Delete the county by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete County : {}", id);
        countyRepository.deleteById(id);
    }
}
