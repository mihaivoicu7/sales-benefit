package com.mvoicu.sales.benefits.service.dto;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the CampaignSeller entity.
 */
public class CampaignSellerDTO implements Serializable {

    private Long id;


    private Long campaignId;

    private Long sellerId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCampaignId() {
        return campaignId;
    }

    public void setCampaignId(Long campaignId) {
        this.campaignId = campaignId;
    }

    public Long getSellerId() {
        return sellerId;
    }

    public void setSellerId(Long sellerId) {
        this.sellerId = sellerId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        CampaignSellerDTO campaignSellerDTO = (CampaignSellerDTO) o;
        if (campaignSellerDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), campaignSellerDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "CampaignSellerDTO{" +
            "id=" + getId() +
            ", campaign=" + getCampaignId() +
            ", seller=" + getSellerId() +
            "}";
    }
}
