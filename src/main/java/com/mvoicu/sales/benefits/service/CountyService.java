package com.mvoicu.sales.benefits.service;

import com.mvoicu.sales.benefits.service.dto.CountyDTO;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing County.
 */
public interface CountyService {

    /**
     * Save a county.
     *
     * @param countyDTO the entity to save
     * @return the persisted entity
     */
    CountyDTO save(CountyDTO countyDTO);

    /**
     * Get all the counties.
     *
     * @return the list of entities
     */
    List<CountyDTO> findAll();


    /**
     * Get the "id" county.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<CountyDTO> findOne(Long id);

    /**
     * Delete the "id" county.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
