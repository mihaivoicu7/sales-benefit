package com.mvoicu.sales.benefits.service.impl;

import com.mvoicu.sales.benefits.service.SellerCountyService;
import com.mvoicu.sales.benefits.domain.SellerCounty;
import com.mvoicu.sales.benefits.repository.SellerCountyRepository;
import com.mvoicu.sales.benefits.service.dto.SellerCountyDTO;
import com.mvoicu.sales.benefits.service.mapper.SellerCountyMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing SellerCounty.
 */
@Service
@Transactional
public class SellerCountyServiceImpl implements SellerCountyService {

    private final Logger log = LoggerFactory.getLogger(SellerCountyServiceImpl.class);

    private final SellerCountyRepository sellerCountyRepository;

    private final SellerCountyMapper sellerCountyMapper;

    public SellerCountyServiceImpl(SellerCountyRepository sellerCountyRepository, SellerCountyMapper sellerCountyMapper) {
        this.sellerCountyRepository = sellerCountyRepository;
        this.sellerCountyMapper = sellerCountyMapper;
    }

    /**
     * Save a sellerCounty.
     *
     * @param sellerCountyDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public SellerCountyDTO save(SellerCountyDTO sellerCountyDTO) {
        log.debug("Request to save SellerCounty : {}", sellerCountyDTO);
        SellerCounty sellerCounty = sellerCountyMapper.toEntity(sellerCountyDTO);
        sellerCounty = sellerCountyRepository.save(sellerCounty);
        return sellerCountyMapper.toDto(sellerCounty);
    }

    /**
     * Get all the sellerCounties.
     *
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<SellerCountyDTO> findAll() {
        log.debug("Request to get all SellerCounties");
        return sellerCountyRepository.findAll().stream()
            .map(sellerCountyMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    /**
     * Get one sellerCounty by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<SellerCountyDTO> findOne(Long id) {
        log.debug("Request to get SellerCounty : {}", id);
        return sellerCountyRepository.findById(id)
            .map(sellerCountyMapper::toDto);
    }

    /**
     * Delete the sellerCounty by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete SellerCounty : {}", id);        sellerCountyRepository.deleteById(id);
    }
}
