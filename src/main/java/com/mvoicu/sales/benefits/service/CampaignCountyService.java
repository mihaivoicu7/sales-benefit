package com.mvoicu.sales.benefits.service;

import com.mvoicu.sales.benefits.service.dto.CampaignCountyDTO;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing CampaignCounty.
 */
public interface CampaignCountyService {

    /**
     * Save a campaignCounty.
     *
     * @param campaignCountyDTO the entity to save
     * @return the persisted entity
     */
    CampaignCountyDTO save(CampaignCountyDTO campaignCountyDTO);

    /**
     * Get all the campaignCounties.
     *
     * @return the list of entities
     */
    List<CampaignCountyDTO> findAll();


    /**
     * Get the "id" campaignCounty.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<CampaignCountyDTO> findOne(Long id);

    /**
     * Delete the "id" campaignCounty.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
