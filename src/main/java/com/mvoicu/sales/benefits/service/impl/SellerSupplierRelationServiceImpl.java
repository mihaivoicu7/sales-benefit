package com.mvoicu.sales.benefits.service.impl;

import com.mvoicu.sales.benefits.service.SellerSupplierRelationService;
import com.mvoicu.sales.benefits.domain.SellerSupplierRelation;
import com.mvoicu.sales.benefits.repository.SellerSupplierRelationRepository;
import com.mvoicu.sales.benefits.service.dto.SellerSupplierRelationDTO;
import com.mvoicu.sales.benefits.service.mapper.SellerSupplierRelationMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing SellerSupplierRelation.
 */
@Service
@Transactional
public class SellerSupplierRelationServiceImpl implements SellerSupplierRelationService {

    private final Logger log = LoggerFactory.getLogger(SellerSupplierRelationServiceImpl.class);

    private final SellerSupplierRelationRepository sellerSupplierRelationRepository;

    private final SellerSupplierRelationMapper sellerSupplierRelationMapper;

    public SellerSupplierRelationServiceImpl(SellerSupplierRelationRepository sellerSupplierRelationRepository, SellerSupplierRelationMapper sellerSupplierRelationMapper) {
        this.sellerSupplierRelationRepository = sellerSupplierRelationRepository;
        this.sellerSupplierRelationMapper = sellerSupplierRelationMapper;
    }

    /**
     * Save a sellerSupplierRelation.
     *
     * @param sellerSupplierRelationDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public SellerSupplierRelationDTO save(SellerSupplierRelationDTO sellerSupplierRelationDTO) {
        log.debug("Request to save SellerSupplierRelation : {}", sellerSupplierRelationDTO);
        SellerSupplierRelation sellerSupplierRelation = sellerSupplierRelationMapper.toEntity(sellerSupplierRelationDTO);
        sellerSupplierRelation = sellerSupplierRelationRepository.save(sellerSupplierRelation);
        return sellerSupplierRelationMapper.toDto(sellerSupplierRelation);
    }

    /**
     * Get all the sellerSupplierRelations.
     *
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<SellerSupplierRelationDTO> findAll() {
        log.debug("Request to get all SellerSupplierRelations");
        return sellerSupplierRelationRepository.findAll().stream()
            .map(sellerSupplierRelationMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    /**
     * Get one sellerSupplierRelation by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<SellerSupplierRelationDTO> findOne(Long id) {
        log.debug("Request to get SellerSupplierRelation : {}", id);
        return sellerSupplierRelationRepository.findById(id)
            .map(sellerSupplierRelationMapper::toDto);
    }

    /**
     * Delete the sellerSupplierRelation by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete SellerSupplierRelation : {}", id);        sellerSupplierRelationRepository.deleteById(id);
    }
}
