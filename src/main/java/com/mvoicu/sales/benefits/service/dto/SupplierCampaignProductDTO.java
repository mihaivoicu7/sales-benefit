package com.mvoicu.sales.benefits.service.dto;

import java.io.Serializable;

/**
 * A DTO for the CampaignProduct entity.
 */
public class SupplierCampaignProductDTO implements Serializable {

    private Long id;

    private Double price;

    private Boolean isActive;

    private Double discount;

    private Long campaignId;

    private Long productId;

    private String productName;

    private String productDescription;

    public SupplierCampaignProductDTO() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Boolean isIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public Double getDiscount() {
        return discount;
    }

    public void setDiscount(Double discount) {
        this.discount = discount;
    }

    public Long getCampaignId() {
        return campaignId;
    }

    public void setCampaignId(Long campaignId) {
        this.campaignId = campaignId;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String description) {
        this.productDescription = description;
    }

    public SupplierCampaignProductDTO(Long id, Double price, Double discount, Long campaignId,
                                      Long productId, String productName, String productDescription) {
        this.id = id;
        this.price = price;
        this.discount = discount;
        this.campaignId = campaignId;
        this.productId = productId;
        this.productName = productName;
        this.productDescription = productDescription;
    }
}
