package com.mvoicu.sales.benefits.service.mapper;

import com.mvoicu.sales.benefits.domain.*;
import com.mvoicu.sales.benefits.service.dto.CampaignSellerDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity CampaignSeller and its DTO CampaignSellerDTO.
 */
@Mapper(componentModel = "spring", uses = {CampaignMapper.class, SellerMapper.class})
public interface CampaignSellerMapper extends EntityMapper<CampaignSellerDTO, CampaignSeller> {

    @Mapping(source = "campaign.id", target = "campaignId")
    @Mapping(source = "seller.id", target = "sellerId")
    CampaignSellerDTO toDto(CampaignSeller campaignSeller);

    @Mapping(source = "campaignId", target = "campaign")
    @Mapping(source = "sellerId", target = "seller")
    CampaignSeller toEntity(CampaignSellerDTO campaignSellerDTO);

    default CampaignSeller fromId(Long id) {
        if (id == null) {
            return null;
        }
        CampaignSeller campaignSeller = new CampaignSeller();
        campaignSeller.setId(id);
        return campaignSeller;
    }
}
