package com.mvoicu.sales.benefits.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the CampaignSeller entity.
 */
public class SupplierCampaignSellerDTO implements Serializable {

    private Long id;

    private Long campaignId;

    private Long sellerId;

    private Boolean isActive;

    private String description;

    private String firstName;

    private String lastName;

    private String email;

    private String imageUrl;

    public SupplierCampaignSellerDTO(Long id, Long campaignId, Long sellerId, Boolean isActive,
                                     String description, String firstName, String lastName,
                                     String email, String imageUrl) {
        this.id = id;
        this.campaignId = campaignId;
        this.sellerId = sellerId;
        this.isActive = isActive;
        this.description = description;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.imageUrl = imageUrl;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCampaignId() {
        return campaignId;
    }

    public void setCampaignId(Long campaignId) {
        this.campaignId = campaignId;
    }

    public Long getSellerId() {
        return sellerId;
    }

    public void setSellerId(Long sellerId) {
        this.sellerId = sellerId;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SupplierCampaignSellerDTO campaignSellerDTO = (SupplierCampaignSellerDTO) o;
        if (campaignSellerDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), campaignSellerDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "CampaignSellerDTO{" +
            "id=" + getId() +
            ", campaign=" + getCampaignId() +
            ", seller=" + getSellerId() +
            ", firstName=" + getFirstName() +
            ", lastName=" + getLastName() +
            ", email=" + getEmail() +
            ", imageUrl=" + getImageUrl() +
            "}";
    }
}
