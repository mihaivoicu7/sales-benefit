package com.mvoicu.sales.benefits.service;

import com.mvoicu.sales.benefits.service.dto.SellerSupplierRelationDTO;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing SellerSupplierRelation.
 */
public interface SellerSupplierRelationService {

    /**
     * Save a sellerSupplierRelation.
     *
     * @param sellerSupplierRelationDTO the entity to save
     * @return the persisted entity
     */
    SellerSupplierRelationDTO save(SellerSupplierRelationDTO sellerSupplierRelationDTO);

    /**
     * Get all the sellerSupplierRelations.
     *
     * @return the list of entities
     */
    List<SellerSupplierRelationDTO> findAll();


    /**
     * Get the "id" sellerSupplierRelation.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<SellerSupplierRelationDTO> findOne(Long id);

    /**
     * Delete the "id" sellerSupplierRelation.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
