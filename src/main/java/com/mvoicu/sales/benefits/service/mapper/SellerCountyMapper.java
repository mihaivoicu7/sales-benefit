package com.mvoicu.sales.benefits.service.mapper;

import com.mvoicu.sales.benefits.domain.*;
import com.mvoicu.sales.benefits.service.dto.SellerCountyDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity SellerCounty and its DTO SellerCountyDTO.
 */
@Mapper(componentModel = "spring", uses = {SellerMapper.class, CountyMapper.class})
public interface SellerCountyMapper extends EntityMapper<SellerCountyDTO, SellerCounty> {

    @Mapping(source = "seller.id", target = "sellerId")
    @Mapping(source = "county.id", target = "countyId")
    SellerCountyDTO toDto(SellerCounty sellerCounty);

    @Mapping(source = "sellerId", target = "seller")
    @Mapping(source = "countyId", target = "county")
    SellerCounty toEntity(SellerCountyDTO sellerCountyDTO);

    default SellerCounty fromId(Long id) {
        if (id == null) {
            return null;
        }
        SellerCounty sellerCounty = new SellerCounty();
        sellerCounty.setId(id);
        return sellerCounty;
    }
}
