package com.mvoicu.sales.benefits.service.dto;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the SellerCounty entity.
 */
public class SellerCountyDTO implements Serializable {

    private Long id;


    private Long sellerId;

    private Long countyId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getSellerId() {
        return sellerId;
    }

    public void setSellerId(Long sellerId) {
        this.sellerId = sellerId;
    }

    public Long getCountyId() {
        return countyId;
    }

    public void setCountyId(Long countyId) {
        this.countyId = countyId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SellerCountyDTO sellerCountyDTO = (SellerCountyDTO) o;
        if (sellerCountyDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), sellerCountyDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "SellerCountyDTO{" +
            "id=" + getId() +
            ", seller=" + getSellerId() +
            ", county=" + getCountyId() +
            "}";
    }
}
