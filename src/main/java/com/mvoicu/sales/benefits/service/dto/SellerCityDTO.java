package com.mvoicu.sales.benefits.service.dto;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the SellerCity entity.
 */
public class SellerCityDTO implements Serializable {

    private Long id;


    private Long sellerId;

    private Long cityId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getSellerId() {
        return sellerId;
    }

    public void setSellerId(Long sellerId) {
        this.sellerId = sellerId;
    }

    public Long getCityId() {
        return cityId;
    }

    public void setCityId(Long cityId) {
        this.cityId = cityId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SellerCityDTO sellerCityDTO = (SellerCityDTO) o;
        if (sellerCityDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), sellerCityDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "SellerCityDTO{" +
            "id=" + getId() +
            ", seller=" + getSellerId() +
            ", city=" + getCityId() +
            "}";
    }
}
