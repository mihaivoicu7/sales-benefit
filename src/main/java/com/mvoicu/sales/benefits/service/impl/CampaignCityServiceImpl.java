package com.mvoicu.sales.benefits.service.impl;

import com.mvoicu.sales.benefits.service.CampaignCityService;
import com.mvoicu.sales.benefits.domain.CampaignCity;
import com.mvoicu.sales.benefits.repository.CampaignCityRepository;
import com.mvoicu.sales.benefits.service.dto.CampaignCityDTO;
import com.mvoicu.sales.benefits.service.mapper.CampaignCityMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing CampaignCity.
 */
@Service
@Transactional
public class CampaignCityServiceImpl implements CampaignCityService {

    private final Logger log = LoggerFactory.getLogger(CampaignCityServiceImpl.class);

    private final CampaignCityRepository campaignCityRepository;

    private final CampaignCityMapper campaignCityMapper;

    public CampaignCityServiceImpl(CampaignCityRepository campaignCityRepository, CampaignCityMapper campaignCityMapper) {
        this.campaignCityRepository = campaignCityRepository;
        this.campaignCityMapper = campaignCityMapper;
    }

    /**
     * Save a campaignCity.
     *
     * @param campaignCityDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public CampaignCityDTO save(CampaignCityDTO campaignCityDTO) {
        log.debug("Request to save CampaignCity : {}", campaignCityDTO);
        CampaignCity campaignCity = campaignCityMapper.toEntity(campaignCityDTO);
        campaignCity = campaignCityRepository.save(campaignCity);
        return campaignCityMapper.toDto(campaignCity);
    }

    /**
     * Get all the campaignCities.
     *
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<CampaignCityDTO> findAll() {
        log.debug("Request to get all CampaignCities");
        return campaignCityRepository.findAll().stream()
            .map(campaignCityMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    /**
     * Get one campaignCity by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<CampaignCityDTO> findOne(Long id) {
        log.debug("Request to get CampaignCity : {}", id);
        return campaignCityRepository.findById(id)
            .map(campaignCityMapper::toDto);
    }

    /**
     * Delete the campaignCity by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete CampaignCity : {}", id);        campaignCityRepository.deleteById(id);
    }
}
