package com.mvoicu.sales.benefits.service.dto;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the CampaignCity entity.
 */
public class CampaignCityDTO implements Serializable {

    private Long id;


    private Long camapignId;

    private Long cityId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCamapignId() {
        return camapignId;
    }

    public void setCamapignId(Long campaignId) {
        this.camapignId = campaignId;
    }

    public Long getCityId() {
        return cityId;
    }

    public void setCityId(Long cityId) {
        this.cityId = cityId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        CampaignCityDTO campaignCityDTO = (CampaignCityDTO) o;
        if (campaignCityDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), campaignCityDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "CampaignCityDTO{" +
            "id=" + getId() +
            ", camapign=" + getCamapignId() +
            ", city=" + getCityId() +
            "}";
    }
}
