package com.mvoicu.sales.benefits.service.mapper;

import com.mvoicu.sales.benefits.domain.*;
import com.mvoicu.sales.benefits.service.dto.CampaignCityDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity CampaignCity and its DTO CampaignCityDTO.
 */
@Mapper(componentModel = "spring", uses = {CampaignMapper.class, CityMapper.class})
public interface CampaignCityMapper extends EntityMapper<CampaignCityDTO, CampaignCity> {

    @Mapping(source = "camapign.id", target = "camapignId")
    @Mapping(source = "city.id", target = "cityId")
    CampaignCityDTO toDto(CampaignCity campaignCity);

    @Mapping(source = "camapignId", target = "camapign")
    @Mapping(source = "cityId", target = "city")
    CampaignCity toEntity(CampaignCityDTO campaignCityDTO);

    default CampaignCity fromId(Long id) {
        if (id == null) {
            return null;
        }
        CampaignCity campaignCity = new CampaignCity();
        campaignCity.setId(id);
        return campaignCity;
    }
}
