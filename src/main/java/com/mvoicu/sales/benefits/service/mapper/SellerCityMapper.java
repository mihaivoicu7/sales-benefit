package com.mvoicu.sales.benefits.service.mapper;

import com.mvoicu.sales.benefits.domain.*;
import com.mvoicu.sales.benefits.service.dto.SellerCityDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity SellerCity and its DTO SellerCityDTO.
 */
@Mapper(componentModel = "spring", uses = {SellerMapper.class, CityMapper.class})
public interface SellerCityMapper extends EntityMapper<SellerCityDTO, SellerCity> {

    @Mapping(source = "seller.id", target = "sellerId")
    @Mapping(source = "city.id", target = "cityId")
    SellerCityDTO toDto(SellerCity sellerCity);

    @Mapping(source = "sellerId", target = "seller")
    @Mapping(source = "cityId", target = "city")
    SellerCity toEntity(SellerCityDTO sellerCityDTO);

    default SellerCity fromId(Long id) {
        if (id == null) {
            return null;
        }
        SellerCity sellerCity = new SellerCity();
        sellerCity.setId(id);
        return sellerCity;
    }
}
