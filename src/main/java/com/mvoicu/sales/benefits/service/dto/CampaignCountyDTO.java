package com.mvoicu.sales.benefits.service.dto;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the CampaignCounty entity.
 */
public class CampaignCountyDTO implements Serializable {

    private Long id;


    private Long campaignId;

    private Long countyId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCampaignId() {
        return campaignId;
    }

    public void setCampaignId(Long campaignId) {
        this.campaignId = campaignId;
    }

    public Long getCountyId() {
        return countyId;
    }

    public void setCountyId(Long countyId) {
        this.countyId = countyId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        CampaignCountyDTO campaignCountyDTO = (CampaignCountyDTO) o;
        if (campaignCountyDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), campaignCountyDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "CampaignCountyDTO{" +
            "id=" + getId() +
            ", campaign=" + getCampaignId() +
            ", county=" + getCountyId() +
            "}";
    }
}
