package com.mvoicu.sales.benefits.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the SellerWallet entity.
 */
public class SellerWalletBalanceDTO implements Serializable {

    private Double availableAmount;

    private Double inPendingAmount;

    public SellerWalletBalanceDTO(Double availableAmount, Double inPendingAmount) {
        this.availableAmount = availableAmount;
        this.inPendingAmount = inPendingAmount;
    }

    public Double getAvailableAmount() {
        return availableAmount;
    }

    public void setAvailableAmount(Double availableAmount) {
        this.availableAmount = availableAmount;
    }

    public Double getInPendingAmount() {
        return inPendingAmount;
    }

    public void setInPendingAmount(Double inPendingAmount) {
        this.inPendingAmount = inPendingAmount;
    }
}
