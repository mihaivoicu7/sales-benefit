package com.mvoicu.sales.benefits.service;

import com.mvoicu.sales.benefits.service.dto.SellerCityDTO;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing SellerCity.
 */
public interface SellerCityService {

    /**
     * Save a sellerCity.
     *
     * @param sellerCityDTO the entity to save
     * @return the persisted entity
     */
    SellerCityDTO save(SellerCityDTO sellerCityDTO);

    /**
     * Get all the sellerCities.
     *
     * @return the list of entities
     */
    List<SellerCityDTO> findAll();


    /**
     * Get the "id" sellerCity.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<SellerCityDTO> findOne(Long id);

    /**
     * Delete the "id" sellerCity.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
