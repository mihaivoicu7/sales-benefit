package com.mvoicu.sales.benefits.service;

import com.mvoicu.sales.benefits.service.dto.CampaignCityDTO;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing CampaignCity.
 */
public interface CampaignCityService {

    /**
     * Save a campaignCity.
     *
     * @param campaignCityDTO the entity to save
     * @return the persisted entity
     */
    CampaignCityDTO save(CampaignCityDTO campaignCityDTO);

    /**
     * Get all the campaignCities.
     *
     * @return the list of entities
     */
    List<CampaignCityDTO> findAll();


    /**
     * Get the "id" campaignCity.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<CampaignCityDTO> findOne(Long id);

    /**
     * Delete the "id" campaignCity.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
