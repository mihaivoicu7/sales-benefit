package com.mvoicu.sales.benefits.repository;

import com.mvoicu.sales.benefits.domain.CampaignCounty;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the CampaignCounty entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CampaignCountyRepository extends JpaRepository<CampaignCounty, Long> {

}
