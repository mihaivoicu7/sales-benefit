package com.mvoicu.sales.benefits.repository;

import com.mvoicu.sales.benefits.domain.CampaignProduct;
import com.mvoicu.sales.benefits.service.dto.SupplierCampaignProductDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * Spring Data  repository for the CampaignProduct entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CampaignProductRepository extends JpaRepository<CampaignProduct, Long> {

    Page<CampaignProduct> findAll(Pageable pageable);

    List<CampaignProduct> findByCampaignIdAndProductId(Long campaignId, Long productId);

    List<CampaignProduct> findByCampaignIdAndCampaignSupplierId(Long campaignId, Long supplierId);

    Page<CampaignProduct> findByCampaignIdAndCampaignSupplierId(Long campaignId, Long supplierId, Pageable pageable);

    Page<CampaignProduct> findByCampaignIdAndCampaignSupplierIdAndProductNameContainingIgnoreCase(Long
                                                                                                             campaignId,
                                                                                                         Long supplierId,
                                                                                                         String name,
                                                                                                         Pageable pageable);

}
