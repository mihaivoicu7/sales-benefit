package com.mvoicu.sales.benefits.repository;

import com.mvoicu.sales.benefits.domain.CampaignSeller;
import com.mvoicu.sales.benefits.service.dto.SupplierCampaignSellerDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the CampaignSeller entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CampaignSellerRepository extends JpaRepository<CampaignSeller, Long> {

    @Query(value = "SELECT new com.mvoicu.sales.benefits.service.dto.SupplierCampaignSellerDTO" +
        "(cs.id, cs.campaign.id, s.id, s.isActive, s.description, u.firstName, u.lastName, u.email, u.imageUrl) " +
        "FROM Seller as s " +
        "LEFT JOIN CampaignSeller AS cs " +
        "  ON cs.seller.id = s.id AND (cs.campaign.id IS NULL OR cs.campaign.id = :campaignId) " +
        "JOIN s.user u ",
        countQuery = "SELECT COUNT(s) FROM Seller AS s")
    Page<SupplierCampaignSellerDTO> findAllCampaignSellersByCampaignId(@Param("campaignId") Long campaignId,
                                                                       Pageable pageable);

}
