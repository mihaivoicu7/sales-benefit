package com.mvoicu.sales.benefits.repository;

import com.mvoicu.sales.benefits.domain.SellerCity;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the SellerCity entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SellerCityRepository extends JpaRepository<SellerCity, Long> {

}
