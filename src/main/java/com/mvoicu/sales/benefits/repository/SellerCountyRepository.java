package com.mvoicu.sales.benefits.repository;

import com.mvoicu.sales.benefits.domain.SellerCounty;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the SellerCounty entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SellerCountyRepository extends JpaRepository<SellerCounty, Long> {

}
