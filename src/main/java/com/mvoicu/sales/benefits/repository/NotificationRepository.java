package com.mvoicu.sales.benefits.repository;

import com.mvoicu.sales.benefits.domain.Notification;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data  repository for the Notification entity.
 */
@SuppressWarnings("unused")
@Repository
public interface NotificationRepository extends JpaRepository<Notification, Long>, JpaSpecificationExecutor<Notification> {

    @Query("select notification from Notification notification " +
        "   where notification.user.login = ?#{principal.username}" +
        "   order by createDate desc")
    List<Notification> findByUserIsCurrentUser(Pageable pageable);

    Integer countNotificationsByUserLogin(String login);

    Integer countNotificationsByUserLoginAndHasBeenReadFalse(String login);

}
