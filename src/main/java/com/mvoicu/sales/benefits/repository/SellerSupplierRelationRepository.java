package com.mvoicu.sales.benefits.repository;

import com.mvoicu.sales.benefits.domain.SellerSupplierRelation;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the SellerSupplierRelation entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SellerSupplierRelationRepository extends JpaRepository<SellerSupplierRelation, Long> {

}
