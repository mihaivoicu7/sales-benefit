package com.mvoicu.sales.benefits.repository;

import com.mvoicu.sales.benefits.domain.CampaignCity;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the CampaignCity entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CampaignCityRepository extends JpaRepository<CampaignCity, Long> {

}
