package com.mvoicu.sales.benefits.supplier.controller;

import com.codahale.metrics.annotation.Timed;
import com.mvoicu.sales.benefits.service.dto.CampaignSellerDTO;
import com.mvoicu.sales.benefits.service.dto.SupplierCampaignSellerDTO;
import com.mvoicu.sales.benefits.supplier.service.SupplierCampaignSellerService;
import com.mvoicu.sales.benefits.web.rest.errors.BadRequestAlertException;
import com.mvoicu.sales.benefits.web.rest.util.HeaderUtil;
import com.mvoicu.sales.benefits.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing CampaignSeller.
 */
@RestController
@RequestMapping("/api/supplier")
public class SupplierCampaignSellerController {

    private final Logger log = LoggerFactory.getLogger(SupplierCampaignSellerController.class);

    private static final String ENTITY_NAME = "campaignSeller";

    private final SupplierCampaignSellerService supplierCampaignSellerService;

    public SupplierCampaignSellerController(SupplierCampaignSellerService supplierCampaignSellerService) {
        this.supplierCampaignSellerService = supplierCampaignSellerService;
    }

    /**
     * POST  /campaign-sellers : Create a new campaignSeller.
     *
     * @param campaignSellerDTO the campaignSellerDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new campaignSellerDTO, or with status 400 (Bad Request) if the campaignSeller has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/campaign-sellers")
    @Timed
    public ResponseEntity<CampaignSellerDTO>
    createCampaignSeller(@RequestBody CampaignSellerDTO campaignSellerDTO) throws URISyntaxException {
        log.debug("REST request to save CampaignSeller : {}", campaignSellerDTO);
        if (campaignSellerDTO.getId() != null) {
            throw new BadRequestAlertException("A new campaignSeller cannot already have an ID", ENTITY_NAME, "idexists");
        }
        if(campaignSellerDTO.getCampaignId() == null) {
            throw new BadRequestAlertException("A new campaignSeller has to have campaignId", ENTITY_NAME, "campaignId");
        }
        if(campaignSellerDTO.getSellerId() == null) {
            throw new BadRequestAlertException("A new campaignSeller has to have sellerId", ENTITY_NAME, "sellerId");
        }
        CampaignSellerDTO result = supplierCampaignSellerService.save(campaignSellerDTO);
        return ResponseEntity.created(new URI("/api/campaign-sellers/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /campaign-sellers : Updates an existing campaignSeller.
     *
     * @param campaignSellerDTO the campaignSellerDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated campaignSellerDTO,
     * or with status 400 (Bad Request) if the campaignSellerDTO is not valid,
     * or with status 500 (Internal Server Error) if the campaignSellerDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/campaign-sellers")
    @Timed
    public ResponseEntity<CampaignSellerDTO>
    updateCampaignSeller(@RequestBody CampaignSellerDTO campaignSellerDTO) throws URISyntaxException {
        log.debug("REST request to update CampaignSeller : {}", campaignSellerDTO);
        if (campaignSellerDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        CampaignSellerDTO result = supplierCampaignSellerService.save(campaignSellerDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, campaignSellerDTO.getId().toString()))
            .body(result);
    }

    /**
     * DELETE  /campaign-sellers/:id : delete the "id" campaignSeller.
     *
     * @param id the id of the campaignSellerDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/campaign-sellers/{id}")
    @Timed
    public ResponseEntity<Void> deleteCampaignSeller(@PathVariable Long id) {
        log.debug("REST request to delete CampaignSeller : {}", id);
        supplierCampaignSellerService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * GET  /campaign-sellers/:id : get the "id" campaignSeller.
     *
     * @param id the id of the campaignSellerDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the campaignSellerDTO, or with status 404 (Not Found)
     */
    @GetMapping("/campaign-sellers/{id}")
    @Timed
    public ResponseEntity<CampaignSellerDTO> getCampaignSeller(@PathVariable Long id) {
        log.debug("REST request to get CampaignSeller : {}", id);
        Optional<CampaignSellerDTO> campaignSellerDTO = supplierCampaignSellerService.findOne(id);
        return ResponseUtil.wrapOrNotFound(campaignSellerDTO);
    }


    @GetMapping("/campaign-sellers")
    @Timed
    ResponseEntity<List<SupplierCampaignSellerDTO>> getAllCampaignSellers(@RequestParam("campaignId") Long campaignId,
                                                                          Pageable pageable) {
        log.debug("REST request to get a page of CampaignSellers");
        Page<SupplierCampaignSellerDTO> page = supplierCampaignSellerService.findForCampaign(campaignId, pageable);
        HttpHeaders httpHeaders = PaginationUtil.generatePaginationHttpHeaders(
            page, "/api/supplier/campaign-sellers"
        );
        return new ResponseEntity<>(page.getContent(), httpHeaders, HttpStatus.OK);
    }
}
