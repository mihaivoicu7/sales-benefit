package com.mvoicu.sales.benefits.supplier.service;

import com.mvoicu.sales.benefits.domain.CampaignSeller;
import com.mvoicu.sales.benefits.repository.CampaignSellerRepository;
import com.mvoicu.sales.benefits.security.SecurityUtils;
import com.mvoicu.sales.benefits.service.dto.CampaignSellerDTO;
import com.mvoicu.sales.benefits.service.dto.SupplierCampaignSellerDTO;
import com.mvoicu.sales.benefits.service.mapper.CampaignSellerMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing CampaignSeller.
 */
@Service
@Transactional
public class SupplierCampaignSellerService {

    private final Logger log = LoggerFactory.getLogger(SupplierCampaignSellerService.class);

    private final CampaignSellerRepository campaignSellerRepository;
    private final CampaignSellerMapper campaignSellerMapper;

    public SupplierCampaignSellerService(CampaignSellerRepository campaignSellerRepository,
                                         CampaignSellerMapper campaignSellerMapper) {
        this.campaignSellerRepository = campaignSellerRepository;
        this.campaignSellerMapper = campaignSellerMapper;
    }

    /**
     * Save a campaignSeller.
     *
     * @param campaignSellerDTO the entity to save
     * @return the persisted entity
     */
    public CampaignSellerDTO save(CampaignSellerDTO campaignSellerDTO) {
        log.debug("Request to save CampaignSeller : {}", campaignSellerDTO);
        CampaignSeller campaignSeller = campaignSellerMapper.toEntity(campaignSellerDTO);
        campaignSeller = campaignSellerRepository.save(campaignSeller);
        return campaignSellerMapper.toDto(campaignSeller);
    }

    /**
     * Delete the campaignSeller by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete CampaignSeller : {}", id);

        Optional<CampaignSeller> campaignSeller = campaignSellerRepository.findById(id);
        if (campaignSeller.isPresent()) {
            if (!SecurityUtils.getCurrentUserLogin().get().equalsIgnoreCase(
                campaignSeller.get().getCampaign().getSupplier().getUser().getLogin())) {
                throw new AccessDeniedException("This supplier cannot delete the campaign product " +
                    "because it is not it's owner");
            }
        }

        campaignSellerRepository.deleteById(id);
    }

    /**
     * Get one campaignSeller by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public Optional<CampaignSellerDTO> findOne(Long id) {
        log.debug("Request to get CampaignSeller : {}", id);
        return campaignSellerRepository.findById(id)
            .map(campaignSellerMapper::toDto);
    }

    public Page<SupplierCampaignSellerDTO> findForCampaign(Long campaignId, Pageable pageable) {
        log.debug("Request to find all campaign products for campaign id: {}", campaignId);
        return campaignSellerRepository.findAllCampaignSellersByCampaignId(campaignId, pageable);
    }
}
