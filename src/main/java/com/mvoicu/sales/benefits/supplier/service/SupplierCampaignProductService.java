package com.mvoicu.sales.benefits.supplier.service;

import com.mvoicu.sales.benefits.domain.CampaignProduct;
import com.mvoicu.sales.benefits.domain.Product;
import com.mvoicu.sales.benefits.repository.CampaignProductRepository;
import com.mvoicu.sales.benefits.repository.ProductRepository;
import com.mvoicu.sales.benefits.security.SecurityUtils;
import com.mvoicu.sales.benefits.service.SupplierService;
import com.mvoicu.sales.benefits.service.dto.CampaignProductDTO;
import com.mvoicu.sales.benefits.service.dto.SupplierCampaignProductDTO;
import com.mvoicu.sales.benefits.service.mapper.CampaignProductMapper;
import com.mvoicu.sales.benefits.service.mapper.ProductMapper;
import com.mvoicu.sales.benefits.service.mapper.SupplierCampaignProductMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing CampaignProduct.
 */
@Service
@Transactional
public class SupplierCampaignProductService {

    private final Logger log = LoggerFactory.getLogger(SupplierCampaignProductService.class);

    private final CampaignProductRepository campaignProductRepository;

    private final SupplierProductService supplierProductService;

    private final SupplierCampaignProductMapper supplierCampaignProductMapper;

    private final CampaignProductMapper campaignProductMapper;

    private final ProductRepository productRepository;

    private final ProductMapper productMapper;

    private final SupplierService supplierService;

    public SupplierCampaignProductService(CampaignProductRepository campaignProductRepository,
                                          SupplierProductService supplierProductService, SupplierCampaignProductMapper supplierCampaignProductMapper,
                                          CampaignProductMapper campaignProductMapper, ProductRepository productRepository,
                                          ProductMapper productMapper,
                                          SupplierService supplierService) {
        this.campaignProductRepository = campaignProductRepository;
        this.supplierProductService = supplierProductService;
        this.supplierCampaignProductMapper = supplierCampaignProductMapper;
        this.campaignProductMapper = campaignProductMapper;
        this.productRepository = productRepository;
        this.productMapper = productMapper;
        this.supplierService = supplierService;
    }

    /**
     * Save a campaignProduct.
     *
     * @param campaignProductDTO the entity to save
     * @return the persisted entity
     */
    public CampaignProductDTO save(CampaignProductDTO campaignProductDTO) {
        log.debug("Request to save CampaignProduct : {}", campaignProductDTO);
        CampaignProduct campaignProduct = campaignProductMapper.toEntity(campaignProductDTO);
        campaignProduct = campaignProductRepository.save(campaignProduct);
        return campaignProductMapper.toDto(campaignProduct);
    }

    /**
     * Get all the campaignProducts.
     *
     * @param pageable
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<CampaignProductDTO> findAll(Pageable pageable) {
        log.debug("Request to get all CampaignProducts");
        return null;
    }


    /**
     * Get one campaignProduct by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public Optional<CampaignProductDTO> findOne(Long id) {
        log.debug("Request to get CampaignProduct : {}", id);
        return campaignProductRepository.findById(id)
            .map(campaignProductMapper::toDto);
    }

    /**
     * Delete the campaignProduct by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete CampaignProduct : {}", id);

        Optional<CampaignProduct> campaignProduct = campaignProductRepository.findById(id);
        if (campaignProduct.isPresent()) {
            if (!SecurityUtils.getCurrentUserLogin().get().equalsIgnoreCase(
                campaignProduct.get().getCampaign().getSupplier().getUser().getLogin())) {
                throw new AccessDeniedException("This supplier cannot delete the campaign product " +
                    "because it is not it's owner");
            }
        }

        campaignProductRepository.deleteById(id);
    }

    public Page<SupplierCampaignProductDTO> findAllForCampaign(Long campaignId, String name, Boolean inCampaign,
                                                                Pageable pageable) {
        log.debug("Request to find all campaign products for campaign id: {}", campaignId);
        if (!inCampaign) {
            return productRepository.findBySupplierUserLoginAndNameContainingIgnoreCase(SecurityUtils.getCurrentUserLogin()
                .get(), name, pageable).map(product -> this.mapProductToCampaignProduct(product, campaignId));
        }
        return productRepository.findBySupplierUserLoginAndNameContainingIgnoreCaseAndCampaignProductsCampaignId(SecurityUtils.getCurrentUserLogin()
            .get(), name, campaignId, pageable).map(product -> this.mapProductToCampaignProduct(product, campaignId));
    }

    public List<SupplierCampaignProductDTO> findForCampaign(Long campaignId) {
        log.debug("Request to find all campaign products for campaign id: {}", campaignId);
        return campaignProductRepository.findByCampaignIdAndCampaignSupplierId(
            campaignId, supplierService.findCurrentSupplier().get().getId()
        ).stream().map(
            campaignProduct -> {
                SupplierCampaignProductDTO supplierCampaignProductDTO = new SupplierCampaignProductDTO();
                supplierCampaignProductDTO.setProductName(campaignProduct.getProduct().getName());
                return supplierCampaignProductDTO;
            }
        ).collect(Collectors.toList());
    }

    private SupplierCampaignProductDTO mapProductToCampaignProduct(Product product, Long campaignId) {
        if (product.getCampaignProducts()!=null) {
            return product.getCampaignProducts()
                .stream()
                .filter(campaignProduct -> campaignProduct.getCampaign().getId().equals(campaignId))
                .findAny()
                .map(supplierCampaignProductMapper::toDto)
                .orElse(mapProductToCampaignProduct(product));
        }
        return mapProductToCampaignProduct(product);
    }

    private SupplierCampaignProductDTO mapProductToCampaignProduct(Product product) {
        SupplierCampaignProductDTO dto = new SupplierCampaignProductDTO();
        dto.setProductName(product.getName());
        dto.setIsActive(true);
        dto.setPrice(product.getPrice());
        dto.setProductDescription(product.getDescription());
        dto.setProductId(product.getId());
        return dto;
    }

}
