/* tslint:disable max-line-length */
import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { of } from 'rxjs';
import { take, map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { SellerWalletTransactionSalesBenService } from 'app/entities/seller-wallet-transaction-sales-ben/seller-wallet-transaction-sales-ben.service';
import {
    ISellerWalletTransactionSalesBen,
    SellerWalletTransactionSalesBen,
    WalletTransactionType
} from 'app/shared/model/seller-wallet-transaction-sales-ben.model';

describe('Service Tests', () => {
    describe('SellerWalletTransactionSalesBen Service', () => {
        let injector: TestBed;
        let service: SellerWalletTransactionSalesBenService;
        let httpMock: HttpTestingController;
        let elemDefault: ISellerWalletTransactionSalesBen;
        let currentDate: moment.Moment;
        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [HttpClientTestingModule]
            });
            injector = getTestBed();
            service = injector.get(SellerWalletTransactionSalesBenService);
            httpMock = injector.get(HttpTestingController);
            currentDate = moment();

            elemDefault = new SellerWalletTransactionSalesBen(0, 0, WalletTransactionType.IN_PENDING, currentDate);
        });

        describe('Service methods', async () => {
            it('should find an element', async () => {
                const returnedFromService = Object.assign(
                    {
                        transactionDate: currentDate.format(DATE_TIME_FORMAT)
                    },
                    elemDefault
                );
                service
                    .find(123)
                    .pipe(take(1))
                    .subscribe(resp => expect(resp).toMatchObject({ body: elemDefault }));

                const req = httpMock.expectOne({ method: 'GET' });
                req.flush(JSON.stringify(returnedFromService));
            });

            it('should create a SellerWalletTransactionSalesBen', async () => {
                const returnedFromService = Object.assign(
                    {
                        id: 0,
                        transactionDate: currentDate.format(DATE_TIME_FORMAT)
                    },
                    elemDefault
                );
                const expected = Object.assign(
                    {
                        transactionDate: currentDate
                    },
                    returnedFromService
                );
                service
                    .create(new SellerWalletTransactionSalesBen(null))
                    .pipe(take(1))
                    .subscribe(resp => expect(resp).toMatchObject({ body: expected }));
                const req = httpMock.expectOne({ method: 'POST' });
                req.flush(JSON.stringify(returnedFromService));
            });

            it('should update a SellerWalletTransactionSalesBen', async () => {
                const returnedFromService = Object.assign(
                    {
                        amount: 1,
                        transactionType: 'BBBBBB',
                        transactionDate: currentDate.format(DATE_TIME_FORMAT)
                    },
                    elemDefault
                );

                const expected = Object.assign(
                    {
                        transactionDate: currentDate
                    },
                    returnedFromService
                );
                service
                    .update(expected)
                    .pipe(take(1))
                    .subscribe(resp => expect(resp).toMatchObject({ body: expected }));
                const req = httpMock.expectOne({ method: 'PUT' });
                req.flush(JSON.stringify(returnedFromService));
            });

            it('should return a list of SellerWalletTransactionSalesBen', async () => {
                const returnedFromService = Object.assign(
                    {
                        amount: 1,
                        transactionType: 'BBBBBB',
                        transactionDate: currentDate.format(DATE_TIME_FORMAT)
                    },
                    elemDefault
                );
                const expected = Object.assign(
                    {
                        transactionDate: currentDate
                    },
                    returnedFromService
                );
                service
                    .query(expected)
                    .pipe(
                        take(1),
                        map(resp => resp.body)
                    )
                    .subscribe(body => expect(body).toContainEqual(expected));
                const req = httpMock.expectOne({ method: 'GET' });
                req.flush(JSON.stringify([returnedFromService]));
                httpMock.verify();
            });

            it('should delete a SellerWalletTransactionSalesBen', async () => {
                const rxPromise = service.delete(123).subscribe(resp => expect(resp.ok));

                const req = httpMock.expectOne({ method: 'DELETE' });
                req.flush({ status: 200 });
            });
        });

        afterEach(() => {
            httpMock.verify();
        });
    });
});
