/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { SalesBenefitTestModule } from '../../../test.module';
import { CampaignCountySalesBenDeleteDialogComponent } from 'app/entities/campaign-county-sales-ben/campaign-county-sales-ben-delete-dialog.component';
import { CampaignCountySalesBenService } from 'app/entities/campaign-county-sales-ben/campaign-county-sales-ben.service';

describe('Component Tests', () => {
    describe('CampaignCountySalesBen Management Delete Component', () => {
        let comp: CampaignCountySalesBenDeleteDialogComponent;
        let fixture: ComponentFixture<CampaignCountySalesBenDeleteDialogComponent>;
        let service: CampaignCountySalesBenService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [SalesBenefitTestModule],
                declarations: [CampaignCountySalesBenDeleteDialogComponent]
            })
                .overrideTemplate(CampaignCountySalesBenDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(CampaignCountySalesBenDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(CampaignCountySalesBenService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete', inject(
                [],
                fakeAsync(() => {
                    // GIVEN
                    spyOn(service, 'delete').and.returnValue(of({}));

                    // WHEN
                    comp.confirmDelete(123);
                    tick();

                    // THEN
                    expect(service.delete).toHaveBeenCalledWith(123);
                    expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                })
            ));
        });
    });
});
