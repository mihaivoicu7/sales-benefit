/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { SalesBenefitTestModule } from '../../../test.module';
import { CampaignCountySalesBenDetailComponent } from 'app/entities/campaign-county-sales-ben/campaign-county-sales-ben-detail.component';
import { CampaignCountySalesBen } from 'app/shared/model/campaign-county-sales-ben.model';

describe('Component Tests', () => {
    describe('CampaignCountySalesBen Management Detail Component', () => {
        let comp: CampaignCountySalesBenDetailComponent;
        let fixture: ComponentFixture<CampaignCountySalesBenDetailComponent>;
        const route = ({ data: of({ campaignCounty: new CampaignCountySalesBen(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [SalesBenefitTestModule],
                declarations: [CampaignCountySalesBenDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(CampaignCountySalesBenDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(CampaignCountySalesBenDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.campaignCounty).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
