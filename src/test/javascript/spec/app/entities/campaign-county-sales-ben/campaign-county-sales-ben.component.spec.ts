/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { SalesBenefitTestModule } from '../../../test.module';
import { CampaignCountySalesBenComponent } from 'app/entities/campaign-county-sales-ben/campaign-county-sales-ben.component';
import { CampaignCountySalesBenService } from 'app/entities/campaign-county-sales-ben/campaign-county-sales-ben.service';
import { CampaignCountySalesBen } from 'app/shared/model/campaign-county-sales-ben.model';

describe('Component Tests', () => {
    describe('CampaignCountySalesBen Management Component', () => {
        let comp: CampaignCountySalesBenComponent;
        let fixture: ComponentFixture<CampaignCountySalesBenComponent>;
        let service: CampaignCountySalesBenService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [SalesBenefitTestModule],
                declarations: [CampaignCountySalesBenComponent],
                providers: []
            })
                .overrideTemplate(CampaignCountySalesBenComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(CampaignCountySalesBenComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(CampaignCountySalesBenService);
        });

        it('Should call load all on init', () => {
            // GIVEN
            const headers = new HttpHeaders().append('link', 'link;link');
            spyOn(service, 'query').and.returnValue(
                of(
                    new HttpResponse({
                        body: [new CampaignCountySalesBen(123)],
                        headers
                    })
                )
            );

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.query).toHaveBeenCalled();
            expect(comp.campaignCounties[0]).toEqual(jasmine.objectContaining({ id: 123 }));
        });
    });
});
