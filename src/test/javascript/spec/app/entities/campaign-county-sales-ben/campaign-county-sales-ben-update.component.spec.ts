/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { SalesBenefitTestModule } from '../../../test.module';
import { CampaignCountySalesBenUpdateComponent } from 'app/entities/campaign-county-sales-ben/campaign-county-sales-ben-update.component';
import { CampaignCountySalesBenService } from 'app/entities/campaign-county-sales-ben/campaign-county-sales-ben.service';
import { CampaignCountySalesBen } from 'app/shared/model/campaign-county-sales-ben.model';

describe('Component Tests', () => {
    describe('CampaignCountySalesBen Management Update Component', () => {
        let comp: CampaignCountySalesBenUpdateComponent;
        let fixture: ComponentFixture<CampaignCountySalesBenUpdateComponent>;
        let service: CampaignCountySalesBenService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [SalesBenefitTestModule],
                declarations: [CampaignCountySalesBenUpdateComponent]
            })
                .overrideTemplate(CampaignCountySalesBenUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(CampaignCountySalesBenUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(CampaignCountySalesBenService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new CampaignCountySalesBen(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.campaignCounty = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new CampaignCountySalesBen();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.campaignCounty = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
