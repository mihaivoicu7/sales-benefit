/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { SalesBenefitTestModule } from '../../../test.module';
import { CampaignCitySalesBenDetailComponent } from 'app/entities/campaign-city-sales-ben/campaign-city-sales-ben-detail.component';
import { CampaignCitySalesBen } from 'app/shared/model/campaign-city-sales-ben.model';

describe('Component Tests', () => {
    describe('CampaignCitySalesBen Management Detail Component', () => {
        let comp: CampaignCitySalesBenDetailComponent;
        let fixture: ComponentFixture<CampaignCitySalesBenDetailComponent>;
        const route = ({ data: of({ campaignCity: new CampaignCitySalesBen(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [SalesBenefitTestModule],
                declarations: [CampaignCitySalesBenDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(CampaignCitySalesBenDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(CampaignCitySalesBenDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.campaignCity).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
