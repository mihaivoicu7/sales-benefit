/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { SalesBenefitTestModule } from '../../../test.module';
import { CampaignCitySalesBenUpdateComponent } from 'app/entities/campaign-city-sales-ben/campaign-city-sales-ben-update.component';
import { CampaignCitySalesBenService } from 'app/entities/campaign-city-sales-ben/campaign-city-sales-ben.service';
import { CampaignCitySalesBen } from 'app/shared/model/campaign-city-sales-ben.model';

describe('Component Tests', () => {
    describe('CampaignCitySalesBen Management Update Component', () => {
        let comp: CampaignCitySalesBenUpdateComponent;
        let fixture: ComponentFixture<CampaignCitySalesBenUpdateComponent>;
        let service: CampaignCitySalesBenService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [SalesBenefitTestModule],
                declarations: [CampaignCitySalesBenUpdateComponent]
            })
                .overrideTemplate(CampaignCitySalesBenUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(CampaignCitySalesBenUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(CampaignCitySalesBenService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new CampaignCitySalesBen(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.campaignCity = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new CampaignCitySalesBen();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.campaignCity = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
