/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { SalesBenefitTestModule } from '../../../test.module';
import { CampaignCitySalesBenDeleteDialogComponent } from 'app/entities/campaign-city-sales-ben/campaign-city-sales-ben-delete-dialog.component';
import { CampaignCitySalesBenService } from 'app/entities/campaign-city-sales-ben/campaign-city-sales-ben.service';

describe('Component Tests', () => {
    describe('CampaignCitySalesBen Management Delete Component', () => {
        let comp: CampaignCitySalesBenDeleteDialogComponent;
        let fixture: ComponentFixture<CampaignCitySalesBenDeleteDialogComponent>;
        let service: CampaignCitySalesBenService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [SalesBenefitTestModule],
                declarations: [CampaignCitySalesBenDeleteDialogComponent]
            })
                .overrideTemplate(CampaignCitySalesBenDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(CampaignCitySalesBenDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(CampaignCitySalesBenService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete', inject(
                [],
                fakeAsync(() => {
                    // GIVEN
                    spyOn(service, 'delete').and.returnValue(of({}));

                    // WHEN
                    comp.confirmDelete(123);
                    tick();

                    // THEN
                    expect(service.delete).toHaveBeenCalledWith(123);
                    expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                })
            ));
        });
    });
});
