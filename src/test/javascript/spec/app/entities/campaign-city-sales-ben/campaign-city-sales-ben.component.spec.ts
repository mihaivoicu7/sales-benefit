/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { SalesBenefitTestModule } from '../../../test.module';
import { CampaignCitySalesBenComponent } from 'app/entities/campaign-city-sales-ben/campaign-city-sales-ben.component';
import { CampaignCitySalesBenService } from 'app/entities/campaign-city-sales-ben/campaign-city-sales-ben.service';
import { CampaignCitySalesBen } from 'app/shared/model/campaign-city-sales-ben.model';

describe('Component Tests', () => {
    describe('CampaignCitySalesBen Management Component', () => {
        let comp: CampaignCitySalesBenComponent;
        let fixture: ComponentFixture<CampaignCitySalesBenComponent>;
        let service: CampaignCitySalesBenService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [SalesBenefitTestModule],
                declarations: [CampaignCitySalesBenComponent],
                providers: []
            })
                .overrideTemplate(CampaignCitySalesBenComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(CampaignCitySalesBenComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(CampaignCitySalesBenService);
        });

        it('Should call load all on init', () => {
            // GIVEN
            const headers = new HttpHeaders().append('link', 'link;link');
            spyOn(service, 'query').and.returnValue(
                of(
                    new HttpResponse({
                        body: [new CampaignCitySalesBen(123)],
                        headers
                    })
                )
            );

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.query).toHaveBeenCalled();
            expect(comp.campaignCities[0]).toEqual(jasmine.objectContaining({ id: 123 }));
        });
    });
});
