/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { SalesBenefitTestModule } from '../../../test.module';
import { SellerSupplierRelationSalesBenDeleteDialogComponent } from 'app/entities/seller-supplier-relation-sales-ben/seller-supplier-relation-sales-ben-delete-dialog.component';
import { SellerSupplierRelationSalesBenService } from 'app/entities/seller-supplier-relation-sales-ben/seller-supplier-relation-sales-ben.service';

describe('Component Tests', () => {
    describe('SellerSupplierRelationSalesBen Management Delete Component', () => {
        let comp: SellerSupplierRelationSalesBenDeleteDialogComponent;
        let fixture: ComponentFixture<SellerSupplierRelationSalesBenDeleteDialogComponent>;
        let service: SellerSupplierRelationSalesBenService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [SalesBenefitTestModule],
                declarations: [SellerSupplierRelationSalesBenDeleteDialogComponent]
            })
                .overrideTemplate(SellerSupplierRelationSalesBenDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(SellerSupplierRelationSalesBenDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(SellerSupplierRelationSalesBenService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete', inject(
                [],
                fakeAsync(() => {
                    // GIVEN
                    spyOn(service, 'delete').and.returnValue(of({}));

                    // WHEN
                    comp.confirmDelete(123);
                    tick();

                    // THEN
                    expect(service.delete).toHaveBeenCalledWith(123);
                    expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                })
            ));
        });
    });
});
