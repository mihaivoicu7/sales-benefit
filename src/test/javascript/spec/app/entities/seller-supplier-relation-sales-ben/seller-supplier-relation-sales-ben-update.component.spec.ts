/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { SalesBenefitTestModule } from '../../../test.module';
import { SellerSupplierRelationSalesBenUpdateComponent } from 'app/entities/seller-supplier-relation-sales-ben/seller-supplier-relation-sales-ben-update.component';
import { SellerSupplierRelationSalesBenService } from 'app/entities/seller-supplier-relation-sales-ben/seller-supplier-relation-sales-ben.service';
import { SellerSupplierRelationSalesBen } from 'app/shared/model/seller-supplier-relation-sales-ben.model';

describe('Component Tests', () => {
    describe('SellerSupplierRelationSalesBen Management Update Component', () => {
        let comp: SellerSupplierRelationSalesBenUpdateComponent;
        let fixture: ComponentFixture<SellerSupplierRelationSalesBenUpdateComponent>;
        let service: SellerSupplierRelationSalesBenService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [SalesBenefitTestModule],
                declarations: [SellerSupplierRelationSalesBenUpdateComponent]
            })
                .overrideTemplate(SellerSupplierRelationSalesBenUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(SellerSupplierRelationSalesBenUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(SellerSupplierRelationSalesBenService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new SellerSupplierRelationSalesBen(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.sellerSupplierRelation = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new SellerSupplierRelationSalesBen();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.sellerSupplierRelation = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
