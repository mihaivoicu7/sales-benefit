/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { SalesBenefitTestModule } from '../../../test.module';
import { SellerSupplierRelationSalesBenDetailComponent } from 'app/entities/seller-supplier-relation-sales-ben/seller-supplier-relation-sales-ben-detail.component';
import { SellerSupplierRelationSalesBen } from 'app/shared/model/seller-supplier-relation-sales-ben.model';

describe('Component Tests', () => {
    describe('SellerSupplierRelationSalesBen Management Detail Component', () => {
        let comp: SellerSupplierRelationSalesBenDetailComponent;
        let fixture: ComponentFixture<SellerSupplierRelationSalesBenDetailComponent>;
        const route = ({ data: of({ sellerSupplierRelation: new SellerSupplierRelationSalesBen(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [SalesBenefitTestModule],
                declarations: [SellerSupplierRelationSalesBenDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(SellerSupplierRelationSalesBenDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(SellerSupplierRelationSalesBenDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.sellerSupplierRelation).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
