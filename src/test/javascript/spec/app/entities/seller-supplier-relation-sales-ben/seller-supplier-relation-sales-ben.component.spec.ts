/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { SalesBenefitTestModule } from '../../../test.module';
import { SellerSupplierRelationSalesBenComponent } from 'app/entities/seller-supplier-relation-sales-ben/seller-supplier-relation-sales-ben.component';
import { SellerSupplierRelationSalesBenService } from 'app/entities/seller-supplier-relation-sales-ben/seller-supplier-relation-sales-ben.service';
import { SellerSupplierRelationSalesBen } from 'app/shared/model/seller-supplier-relation-sales-ben.model';

describe('Component Tests', () => {
    describe('SellerSupplierRelationSalesBen Management Component', () => {
        let comp: SellerSupplierRelationSalesBenComponent;
        let fixture: ComponentFixture<SellerSupplierRelationSalesBenComponent>;
        let service: SellerSupplierRelationSalesBenService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [SalesBenefitTestModule],
                declarations: [SellerSupplierRelationSalesBenComponent],
                providers: []
            })
                .overrideTemplate(SellerSupplierRelationSalesBenComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(SellerSupplierRelationSalesBenComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(SellerSupplierRelationSalesBenService);
        });

        it('Should call load all on init', () => {
            // GIVEN
            const headers = new HttpHeaders().append('link', 'link;link');
            spyOn(service, 'query').and.returnValue(
                of(
                    new HttpResponse({
                        body: [new SellerSupplierRelationSalesBen(123)],
                        headers
                    })
                )
            );

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.query).toHaveBeenCalled();
            expect(comp.sellerSupplierRelations[0]).toEqual(jasmine.objectContaining({ id: 123 }));
        });
    });
});
