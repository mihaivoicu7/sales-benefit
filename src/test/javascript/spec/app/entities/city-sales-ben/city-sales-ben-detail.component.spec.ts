/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { SalesBenefitTestModule } from '../../../test.module';
import { CitySalesBenDetailComponent } from 'app/entities/city-sales-ben/city-sales-ben-detail.component';
import { CitySalesBen } from 'app/shared/model/city-sales-ben.model';

describe('Component Tests', () => {
    describe('CitySalesBen Management Detail Component', () => {
        let comp: CitySalesBenDetailComponent;
        let fixture: ComponentFixture<CitySalesBenDetailComponent>;
        const route = ({ data: of({ city: new CitySalesBen(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [SalesBenefitTestModule],
                declarations: [CitySalesBenDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(CitySalesBenDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(CitySalesBenDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.city).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
