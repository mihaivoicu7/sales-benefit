/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { SalesBenefitTestModule } from '../../../test.module';
import { CitySalesBenComponent } from 'app/entities/city-sales-ben/city-sales-ben.component';
import { CitySalesBenService } from 'app/entities/city-sales-ben/city-sales-ben.service';
import { CitySalesBen } from 'app/shared/model/city-sales-ben.model';

describe('Component Tests', () => {
    describe('CitySalesBen Management Component', () => {
        let comp: CitySalesBenComponent;
        let fixture: ComponentFixture<CitySalesBenComponent>;
        let service: CitySalesBenService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [SalesBenefitTestModule],
                declarations: [CitySalesBenComponent],
                providers: []
            })
                .overrideTemplate(CitySalesBenComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(CitySalesBenComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(CitySalesBenService);
        });

        it('Should call load all on init', () => {
            // GIVEN
            const headers = new HttpHeaders().append('link', 'link;link');
            spyOn(service, 'query').and.returnValue(
                of(
                    new HttpResponse({
                        body: [new CitySalesBen(123)],
                        headers
                    })
                )
            );

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.query).toHaveBeenCalled();
            expect(comp.cities[0]).toEqual(jasmine.objectContaining({ id: 123 }));
        });
    });
});
