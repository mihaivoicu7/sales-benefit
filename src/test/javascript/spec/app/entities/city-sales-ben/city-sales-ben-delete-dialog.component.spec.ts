/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { SalesBenefitTestModule } from '../../../test.module';
import { CitySalesBenDeleteDialogComponent } from 'app/entities/city-sales-ben/city-sales-ben-delete-dialog.component';
import { CitySalesBenService } from 'app/entities/city-sales-ben/city-sales-ben.service';

describe('Component Tests', () => {
    describe('CitySalesBen Management Delete Component', () => {
        let comp: CitySalesBenDeleteDialogComponent;
        let fixture: ComponentFixture<CitySalesBenDeleteDialogComponent>;
        let service: CitySalesBenService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [SalesBenefitTestModule],
                declarations: [CitySalesBenDeleteDialogComponent]
            })
                .overrideTemplate(CitySalesBenDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(CitySalesBenDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(CitySalesBenService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete', inject(
                [],
                fakeAsync(() => {
                    // GIVEN
                    spyOn(service, 'delete').and.returnValue(of({}));

                    // WHEN
                    comp.confirmDelete(123);
                    tick();

                    // THEN
                    expect(service.delete).toHaveBeenCalledWith(123);
                    expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                })
            ));
        });
    });
});
