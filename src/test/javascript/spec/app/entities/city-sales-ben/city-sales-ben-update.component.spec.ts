/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { SalesBenefitTestModule } from '../../../test.module';
import { CitySalesBenUpdateComponent } from 'app/entities/city-sales-ben/city-sales-ben-update.component';
import { CitySalesBenService } from 'app/entities/city-sales-ben/city-sales-ben.service';
import { CitySalesBen } from 'app/shared/model/city-sales-ben.model';

describe('Component Tests', () => {
    describe('CitySalesBen Management Update Component', () => {
        let comp: CitySalesBenUpdateComponent;
        let fixture: ComponentFixture<CitySalesBenUpdateComponent>;
        let service: CitySalesBenService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [SalesBenefitTestModule],
                declarations: [CitySalesBenUpdateComponent]
            })
                .overrideTemplate(CitySalesBenUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(CitySalesBenUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(CitySalesBenService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new CitySalesBen(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.city = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new CitySalesBen();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.city = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
