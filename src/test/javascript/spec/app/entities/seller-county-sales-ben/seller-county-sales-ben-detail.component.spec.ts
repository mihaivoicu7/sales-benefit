/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { SalesBenefitTestModule } from '../../../test.module';
import { SellerCountySalesBenDetailComponent } from 'app/entities/seller-county-sales-ben/seller-county-sales-ben-detail.component';
import { SellerCountySalesBen } from 'app/shared/model/seller-county-sales-ben.model';

describe('Component Tests', () => {
    describe('SellerCountySalesBen Management Detail Component', () => {
        let comp: SellerCountySalesBenDetailComponent;
        let fixture: ComponentFixture<SellerCountySalesBenDetailComponent>;
        const route = ({ data: of({ sellerCounty: new SellerCountySalesBen(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [SalesBenefitTestModule],
                declarations: [SellerCountySalesBenDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(SellerCountySalesBenDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(SellerCountySalesBenDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.sellerCounty).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
