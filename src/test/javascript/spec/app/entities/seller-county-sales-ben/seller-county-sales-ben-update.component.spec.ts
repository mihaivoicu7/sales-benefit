/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { SalesBenefitTestModule } from '../../../test.module';
import { SellerCountySalesBenUpdateComponent } from 'app/entities/seller-county-sales-ben/seller-county-sales-ben-update.component';
import { SellerCountySalesBenService } from 'app/entities/seller-county-sales-ben/seller-county-sales-ben.service';
import { SellerCountySalesBen } from 'app/shared/model/seller-county-sales-ben.model';

describe('Component Tests', () => {
    describe('SellerCountySalesBen Management Update Component', () => {
        let comp: SellerCountySalesBenUpdateComponent;
        let fixture: ComponentFixture<SellerCountySalesBenUpdateComponent>;
        let service: SellerCountySalesBenService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [SalesBenefitTestModule],
                declarations: [SellerCountySalesBenUpdateComponent]
            })
                .overrideTemplate(SellerCountySalesBenUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(SellerCountySalesBenUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(SellerCountySalesBenService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new SellerCountySalesBen(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.sellerCounty = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new SellerCountySalesBen();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.sellerCounty = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
