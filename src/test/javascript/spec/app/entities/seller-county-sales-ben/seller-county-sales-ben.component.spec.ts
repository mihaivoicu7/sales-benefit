/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { SalesBenefitTestModule } from '../../../test.module';
import { SellerCountySalesBenComponent } from 'app/entities/seller-county-sales-ben/seller-county-sales-ben.component';
import { SellerCountySalesBenService } from 'app/entities/seller-county-sales-ben/seller-county-sales-ben.service';
import { SellerCountySalesBen } from 'app/shared/model/seller-county-sales-ben.model';

describe('Component Tests', () => {
    describe('SellerCountySalesBen Management Component', () => {
        let comp: SellerCountySalesBenComponent;
        let fixture: ComponentFixture<SellerCountySalesBenComponent>;
        let service: SellerCountySalesBenService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [SalesBenefitTestModule],
                declarations: [SellerCountySalesBenComponent],
                providers: []
            })
                .overrideTemplate(SellerCountySalesBenComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(SellerCountySalesBenComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(SellerCountySalesBenService);
        });

        it('Should call load all on init', () => {
            // GIVEN
            const headers = new HttpHeaders().append('link', 'link;link');
            spyOn(service, 'query').and.returnValue(
                of(
                    new HttpResponse({
                        body: [new SellerCountySalesBen(123)],
                        headers
                    })
                )
            );

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.query).toHaveBeenCalled();
            expect(comp.sellerCounties[0]).toEqual(jasmine.objectContaining({ id: 123 }));
        });
    });
});
