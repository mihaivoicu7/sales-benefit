/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { SalesBenefitTestModule } from '../../../test.module';
import { SellerCountySalesBenDeleteDialogComponent } from 'app/entities/seller-county-sales-ben/seller-county-sales-ben-delete-dialog.component';
import { SellerCountySalesBenService } from 'app/entities/seller-county-sales-ben/seller-county-sales-ben.service';

describe('Component Tests', () => {
    describe('SellerCountySalesBen Management Delete Component', () => {
        let comp: SellerCountySalesBenDeleteDialogComponent;
        let fixture: ComponentFixture<SellerCountySalesBenDeleteDialogComponent>;
        let service: SellerCountySalesBenService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [SalesBenefitTestModule],
                declarations: [SellerCountySalesBenDeleteDialogComponent]
            })
                .overrideTemplate(SellerCountySalesBenDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(SellerCountySalesBenDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(SellerCountySalesBenService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete', inject(
                [],
                fakeAsync(() => {
                    // GIVEN
                    spyOn(service, 'delete').and.returnValue(of({}));

                    // WHEN
                    comp.confirmDelete(123);
                    tick();

                    // THEN
                    expect(service.delete).toHaveBeenCalledWith(123);
                    expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                })
            ));
        });
    });
});
