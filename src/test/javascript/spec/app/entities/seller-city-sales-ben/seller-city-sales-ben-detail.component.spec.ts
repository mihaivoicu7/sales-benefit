/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { SalesBenefitTestModule } from '../../../test.module';
import { SellerCitySalesBenDetailComponent } from 'app/entities/seller-city-sales-ben/seller-city-sales-ben-detail.component';
import { SellerCitySalesBen } from 'app/shared/model/seller-city-sales-ben.model';

describe('Component Tests', () => {
    describe('SellerCitySalesBen Management Detail Component', () => {
        let comp: SellerCitySalesBenDetailComponent;
        let fixture: ComponentFixture<SellerCitySalesBenDetailComponent>;
        const route = ({ data: of({ sellerCity: new SellerCitySalesBen(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [SalesBenefitTestModule],
                declarations: [SellerCitySalesBenDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(SellerCitySalesBenDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(SellerCitySalesBenDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.sellerCity).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
