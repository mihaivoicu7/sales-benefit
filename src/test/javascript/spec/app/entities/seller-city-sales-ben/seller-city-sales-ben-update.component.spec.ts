/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { SalesBenefitTestModule } from '../../../test.module';
import { SellerCitySalesBenUpdateComponent } from 'app/entities/seller-city-sales-ben/seller-city-sales-ben-update.component';
import { SellerCitySalesBenService } from 'app/entities/seller-city-sales-ben/seller-city-sales-ben.service';
import { SellerCitySalesBen } from 'app/shared/model/seller-city-sales-ben.model';

describe('Component Tests', () => {
    describe('SellerCitySalesBen Management Update Component', () => {
        let comp: SellerCitySalesBenUpdateComponent;
        let fixture: ComponentFixture<SellerCitySalesBenUpdateComponent>;
        let service: SellerCitySalesBenService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [SalesBenefitTestModule],
                declarations: [SellerCitySalesBenUpdateComponent]
            })
                .overrideTemplate(SellerCitySalesBenUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(SellerCitySalesBenUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(SellerCitySalesBenService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new SellerCitySalesBen(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.sellerCity = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new SellerCitySalesBen();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.sellerCity = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
