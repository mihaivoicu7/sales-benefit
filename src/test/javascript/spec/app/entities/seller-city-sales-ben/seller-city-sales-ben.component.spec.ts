/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { SalesBenefitTestModule } from '../../../test.module';
import { SellerCitySalesBenComponent } from 'app/entities/seller-city-sales-ben/seller-city-sales-ben.component';
import { SellerCitySalesBenService } from 'app/entities/seller-city-sales-ben/seller-city-sales-ben.service';
import { SellerCitySalesBen } from 'app/shared/model/seller-city-sales-ben.model';

describe('Component Tests', () => {
    describe('SellerCitySalesBen Management Component', () => {
        let comp: SellerCitySalesBenComponent;
        let fixture: ComponentFixture<SellerCitySalesBenComponent>;
        let service: SellerCitySalesBenService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [SalesBenefitTestModule],
                declarations: [SellerCitySalesBenComponent],
                providers: []
            })
                .overrideTemplate(SellerCitySalesBenComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(SellerCitySalesBenComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(SellerCitySalesBenService);
        });

        it('Should call load all on init', () => {
            // GIVEN
            const headers = new HttpHeaders().append('link', 'link;link');
            spyOn(service, 'query').and.returnValue(
                of(
                    new HttpResponse({
                        body: [new SellerCitySalesBen(123)],
                        headers
                    })
                )
            );

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.query).toHaveBeenCalled();
            expect(comp.sellerCities[0]).toEqual(jasmine.objectContaining({ id: 123 }));
        });
    });
});
