/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { SalesBenefitTestModule } from '../../../test.module';
import { CampaignSellerSalesBenUpdateComponent } from 'app/entities/campaign-seller-sales-ben/campaign-seller-sales-ben-update.component';
import { CampaignSellerSalesBenService } from 'app/entities/campaign-seller-sales-ben/campaign-seller-sales-ben.service';
import { CampaignSellerSalesBen } from 'app/shared/model/campaign-seller-sales-ben.model';

describe('Component Tests', () => {
    describe('CampaignSellerSalesBen Management Update Component', () => {
        let comp: CampaignSellerSalesBenUpdateComponent;
        let fixture: ComponentFixture<CampaignSellerSalesBenUpdateComponent>;
        let service: CampaignSellerSalesBenService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [SalesBenefitTestModule],
                declarations: [CampaignSellerSalesBenUpdateComponent]
            })
                .overrideTemplate(CampaignSellerSalesBenUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(CampaignSellerSalesBenUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(CampaignSellerSalesBenService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new CampaignSellerSalesBen(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.campaignSeller = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new CampaignSellerSalesBen();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.campaignSeller = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
