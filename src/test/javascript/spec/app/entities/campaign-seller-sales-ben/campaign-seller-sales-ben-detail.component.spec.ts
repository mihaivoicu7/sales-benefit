/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { SalesBenefitTestModule } from '../../../test.module';
import { CampaignSellerSalesBenDetailComponent } from 'app/entities/campaign-seller-sales-ben/campaign-seller-sales-ben-detail.component';
import { CampaignSellerSalesBen } from 'app/shared/model/campaign-seller-sales-ben.model';

describe('Component Tests', () => {
    describe('CampaignSellerSalesBen Management Detail Component', () => {
        let comp: CampaignSellerSalesBenDetailComponent;
        let fixture: ComponentFixture<CampaignSellerSalesBenDetailComponent>;
        const route = ({ data: of({ campaignSeller: new CampaignSellerSalesBen(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [SalesBenefitTestModule],
                declarations: [CampaignSellerSalesBenDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(CampaignSellerSalesBenDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(CampaignSellerSalesBenDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.campaignSeller).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
