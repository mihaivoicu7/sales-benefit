/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { SalesBenefitTestModule } from '../../../test.module';
import { CountySalesBenDetailComponent } from 'app/entities/county-sales-ben/county-sales-ben-detail.component';
import { CountySalesBen } from 'app/shared/model/county-sales-ben.model';

describe('Component Tests', () => {
    describe('CountySalesBen Management Detail Component', () => {
        let comp: CountySalesBenDetailComponent;
        let fixture: ComponentFixture<CountySalesBenDetailComponent>;
        const route = ({ data: of({ county: new CountySalesBen(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [SalesBenefitTestModule],
                declarations: [CountySalesBenDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(CountySalesBenDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(CountySalesBenDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.county).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
