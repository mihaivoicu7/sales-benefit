/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { SalesBenefitTestModule } from '../../../test.module';
import { CountySalesBenComponent } from 'app/entities/county-sales-ben/county-sales-ben.component';
import { CountySalesBenService } from 'app/entities/county-sales-ben/county-sales-ben.service';
import { CountySalesBen } from 'app/shared/model/county-sales-ben.model';

describe('Component Tests', () => {
    describe('CountySalesBen Management Component', () => {
        let comp: CountySalesBenComponent;
        let fixture: ComponentFixture<CountySalesBenComponent>;
        let service: CountySalesBenService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [SalesBenefitTestModule],
                declarations: [CountySalesBenComponent],
                providers: []
            })
                .overrideTemplate(CountySalesBenComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(CountySalesBenComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(CountySalesBenService);
        });

        it('Should call load all on init', () => {
            // GIVEN
            const headers = new HttpHeaders().append('link', 'link;link');
            spyOn(service, 'query').and.returnValue(
                of(
                    new HttpResponse({
                        body: [new CountySalesBen(123)],
                        headers
                    })
                )
            );

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.query).toHaveBeenCalled();
            expect(comp.counties[0]).toEqual(jasmine.objectContaining({ id: 123 }));
        });
    });
});
