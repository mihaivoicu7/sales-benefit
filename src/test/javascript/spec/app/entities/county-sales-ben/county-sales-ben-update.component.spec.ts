/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { SalesBenefitTestModule } from '../../../test.module';
import { CountySalesBenUpdateComponent } from 'app/entities/county-sales-ben/county-sales-ben-update.component';
import { CountySalesBenService } from 'app/entities/county-sales-ben/county-sales-ben.service';
import { CountySalesBen } from 'app/shared/model/county-sales-ben.model';

describe('Component Tests', () => {
    describe('CountySalesBen Management Update Component', () => {
        let comp: CountySalesBenUpdateComponent;
        let fixture: ComponentFixture<CountySalesBenUpdateComponent>;
        let service: CountySalesBenService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [SalesBenefitTestModule],
                declarations: [CountySalesBenUpdateComponent]
            })
                .overrideTemplate(CountySalesBenUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(CountySalesBenUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(CountySalesBenService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new CountySalesBen(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.county = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new CountySalesBen();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.county = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
