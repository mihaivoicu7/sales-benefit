/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { SalesBenefitTestModule } from '../../../test.module';
import { CountySalesBenDeleteDialogComponent } from 'app/entities/county-sales-ben/county-sales-ben-delete-dialog.component';
import { CountySalesBenService } from 'app/entities/county-sales-ben/county-sales-ben.service';

describe('Component Tests', () => {
    describe('CountySalesBen Management Delete Component', () => {
        let comp: CountySalesBenDeleteDialogComponent;
        let fixture: ComponentFixture<CountySalesBenDeleteDialogComponent>;
        let service: CountySalesBenService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [SalesBenefitTestModule],
                declarations: [CountySalesBenDeleteDialogComponent]
            })
                .overrideTemplate(CountySalesBenDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(CountySalesBenDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(CountySalesBenService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete', inject(
                [],
                fakeAsync(() => {
                    // GIVEN
                    spyOn(service, 'delete').and.returnValue(of({}));

                    // WHEN
                    comp.confirmDelete(123);
                    tick();

                    // THEN
                    expect(service.delete).toHaveBeenCalledWith(123);
                    expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                })
            ));
        });
    });
});
