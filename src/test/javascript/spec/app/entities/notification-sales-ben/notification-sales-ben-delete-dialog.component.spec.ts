/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { SalesBenefitTestModule } from '../../../test.module';
import { NotificationSalesBenDeleteDialogComponent } from 'app/entities/notification-sales-ben/notification-sales-ben-delete-dialog.component';
import { NotificationSalesBenService } from 'app/entities/notification-sales-ben/notification-sales-ben.service';

describe('Component Tests', () => {
    describe('NotificationSalesBen Management Delete Component', () => {
        let comp: NotificationSalesBenDeleteDialogComponent;
        let fixture: ComponentFixture<NotificationSalesBenDeleteDialogComponent>;
        let service: NotificationSalesBenService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [SalesBenefitTestModule],
                declarations: [NotificationSalesBenDeleteDialogComponent]
            })
                .overrideTemplate(NotificationSalesBenDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(NotificationSalesBenDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(NotificationSalesBenService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete', inject(
                [],
                fakeAsync(() => {
                    // GIVEN
                    spyOn(service, 'delete').and.returnValue(of({}));

                    // WHEN
                    comp.confirmDelete(123);
                    tick();

                    // THEN
                    expect(service.delete).toHaveBeenCalledWith(123);
                    expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                })
            ));
        });
    });
});
