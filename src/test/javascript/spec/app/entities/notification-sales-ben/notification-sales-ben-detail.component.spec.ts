/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { SalesBenefitTestModule } from '../../../test.module';
import { NotificationSalesBenDetailComponent } from 'app/entities/notification-sales-ben/notification-sales-ben-detail.component';
import { NotificationSalesBen } from 'app/shared/model/notification-sales-ben.model';

describe('Component Tests', () => {
    describe('NotificationSalesBen Management Detail Component', () => {
        let comp: NotificationSalesBenDetailComponent;
        let fixture: ComponentFixture<NotificationSalesBenDetailComponent>;
        const route = ({ data: of({ notification: new NotificationSalesBen(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [SalesBenefitTestModule],
                declarations: [NotificationSalesBenDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(NotificationSalesBenDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(NotificationSalesBenDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.notification).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
