/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { SalesBenefitTestModule } from '../../../test.module';
import { NotificationSalesBenUpdateComponent } from 'app/entities/notification-sales-ben/notification-sales-ben-update.component';
import { NotificationSalesBenService } from 'app/entities/notification-sales-ben/notification-sales-ben.service';
import { NotificationSalesBen } from 'app/shared/model/notification-sales-ben.model';

describe('Component Tests', () => {
    describe('NotificationSalesBen Management Update Component', () => {
        let comp: NotificationSalesBenUpdateComponent;
        let fixture: ComponentFixture<NotificationSalesBenUpdateComponent>;
        let service: NotificationSalesBenService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [SalesBenefitTestModule],
                declarations: [NotificationSalesBenUpdateComponent]
            })
                .overrideTemplate(NotificationSalesBenUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(NotificationSalesBenUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(NotificationSalesBenService);
        });

        describe('save', () => {
            it('Should call update service on save for existing entity', fakeAsync(() => {
                // GIVEN
                const entity = new NotificationSalesBen(123);
                spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                comp.notification = entity;
                // WHEN
                comp.save();
                tick(); // simulate async

                // THEN
                expect(service.update).toHaveBeenCalledWith(entity);
                expect(comp.isSaving).toEqual(false);
            }));

            it('Should call create service on save for new entity', fakeAsync(() => {
                // GIVEN
                const entity = new NotificationSalesBen();
                spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                comp.notification = entity;
                // WHEN
                comp.save();
                tick(); // simulate async

                // THEN
                expect(service.create).toHaveBeenCalledWith(entity);
                expect(comp.isSaving).toEqual(false);
            }));
        });
    });
});
