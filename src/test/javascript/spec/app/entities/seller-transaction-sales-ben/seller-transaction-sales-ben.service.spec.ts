/* tslint:disable max-line-length */
import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { of } from 'rxjs';
import { take, map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { SellerTransactionSalesBenService } from 'app/entities/seller-transaction-sales-ben/seller-transaction-sales-ben.service';
import { ISellerTransactionSalesBen, SellerTransactionSalesBen } from 'app/shared/model/seller-transaction-sales-ben.model';

describe('Service Tests', () => {
    describe('SellerTransactionSalesBen Service', () => {
        let injector: TestBed;
        let service: SellerTransactionSalesBenService;
        let httpMock: HttpTestingController;
        let elemDefault: ISellerTransactionSalesBen;
        let currentDate: moment.Moment;
        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [HttpClientTestingModule]
            });
            injector = getTestBed();
            service = injector.get(SellerTransactionSalesBenService);
            httpMock = injector.get(HttpTestingController);
            currentDate = moment();

            elemDefault = new SellerTransactionSalesBen(0, false, 0, currentDate);
        });

        describe('Service methods', async () => {
            it('should find an element', async () => {
                const returnedFromService = Object.assign(
                    {
                        transactionDate: currentDate.format(DATE_TIME_FORMAT)
                    },
                    elemDefault
                );
                service
                    .find(123)
                    .pipe(take(1))
                    .subscribe(resp => expect(resp).toMatchObject({ body: elemDefault }));

                const req = httpMock.expectOne({ method: 'GET' });
                req.flush(JSON.stringify(returnedFromService));
            });

            it('should create a SellerTransactionSalesBen', async () => {
                const returnedFromService = Object.assign(
                    {
                        id: 0,
                        transactionDate: currentDate.format(DATE_TIME_FORMAT)
                    },
                    elemDefault
                );
                const expected = Object.assign(
                    {
                        transactionDate: currentDate
                    },
                    returnedFromService
                );
                service
                    .create(new SellerTransactionSalesBen(null))
                    .pipe(take(1))
                    .subscribe(resp => expect(resp).toMatchObject({ body: expected }));
                const req = httpMock.expectOne({ method: 'POST' });
                req.flush(JSON.stringify(returnedFromService));
            });

            it('should update a SellerTransactionSalesBen', async () => {
                const returnedFromService = Object.assign(
                    {
                        confirmed: true,
                        amount: 1,
                        transactionDate: currentDate.format(DATE_TIME_FORMAT)
                    },
                    elemDefault
                );

                const expected = Object.assign(
                    {
                        transactionDate: currentDate
                    },
                    returnedFromService
                );
                service
                    .update(expected)
                    .pipe(take(1))
                    .subscribe(resp => expect(resp).toMatchObject({ body: expected }));
                const req = httpMock.expectOne({ method: 'PUT' });
                req.flush(JSON.stringify(returnedFromService));
            });

            it('should return a list of SellerTransactionSalesBen', async () => {
                const returnedFromService = Object.assign(
                    {
                        confirmed: true,
                        amount: 1,
                        transactionDate: currentDate.format(DATE_TIME_FORMAT)
                    },
                    elemDefault
                );
                const expected = Object.assign(
                    {
                        transactionDate: currentDate
                    },
                    returnedFromService
                );
                service
                    .query(expected)
                    .pipe(
                        take(1),
                        map(resp => resp.body)
                    )
                    .subscribe(body => expect(body).toContainEqual(expected));
                const req = httpMock.expectOne({ method: 'GET' });
                req.flush(JSON.stringify([returnedFromService]));
                httpMock.verify();
            });

            it('should delete a SellerTransactionSalesBen', async () => {
                const rxPromise = service.delete(123).subscribe(resp => expect(resp.ok));

                const req = httpMock.expectOne({ method: 'DELETE' });
                req.flush({ status: 200 });
            });
        });

        afterEach(() => {
            httpMock.verify();
        });
    });
});
