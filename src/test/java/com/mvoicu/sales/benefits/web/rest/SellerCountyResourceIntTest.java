package com.mvoicu.sales.benefits.web.rest;

import com.mvoicu.sales.benefits.SalesBenefitApp;

import com.mvoicu.sales.benefits.domain.SellerCounty;
import com.mvoicu.sales.benefits.repository.SellerCountyRepository;
import com.mvoicu.sales.benefits.service.SellerCountyService;
import com.mvoicu.sales.benefits.service.dto.SellerCountyDTO;
import com.mvoicu.sales.benefits.service.mapper.SellerCountyMapper;
import com.mvoicu.sales.benefits.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;


import static com.mvoicu.sales.benefits.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the SellerCountyResource REST controller.
 *
 * @see SellerCountyResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = SalesBenefitApp.class)
public class SellerCountyResourceIntTest {

    @Autowired
    private SellerCountyRepository sellerCountyRepository;

    @Autowired
    private SellerCountyMapper sellerCountyMapper;

    @Autowired
    private SellerCountyService sellerCountyService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restSellerCountyMockMvc;

    private SellerCounty sellerCounty;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final SellerCountyResource sellerCountyResource = new SellerCountyResource(sellerCountyService);
        this.restSellerCountyMockMvc = MockMvcBuilders.standaloneSetup(sellerCountyResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static SellerCounty createEntity(EntityManager em) {
        SellerCounty sellerCounty = new SellerCounty();
        return sellerCounty;
    }

    @Before
    public void initTest() {
        sellerCounty = createEntity(em);
    }

    @Test
    @Transactional
    public void createSellerCounty() throws Exception {
        int databaseSizeBeforeCreate = sellerCountyRepository.findAll().size();

        // Create the SellerCounty
        SellerCountyDTO sellerCountyDTO = sellerCountyMapper.toDto(sellerCounty);
        restSellerCountyMockMvc.perform(post("/api/seller-counties")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(sellerCountyDTO)))
            .andExpect(status().isCreated());

        // Validate the SellerCounty in the database
        List<SellerCounty> sellerCountyList = sellerCountyRepository.findAll();
        assertThat(sellerCountyList).hasSize(databaseSizeBeforeCreate + 1);
        SellerCounty testSellerCounty = sellerCountyList.get(sellerCountyList.size() - 1);
    }

    @Test
    @Transactional
    public void createSellerCountyWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = sellerCountyRepository.findAll().size();

        // Create the SellerCounty with an existing ID
        sellerCounty.setId(1L);
        SellerCountyDTO sellerCountyDTO = sellerCountyMapper.toDto(sellerCounty);

        // An entity with an existing ID cannot be created, so this API call must fail
        restSellerCountyMockMvc.perform(post("/api/seller-counties")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(sellerCountyDTO)))
            .andExpect(status().isBadRequest());

        // Validate the SellerCounty in the database
        List<SellerCounty> sellerCountyList = sellerCountyRepository.findAll();
        assertThat(sellerCountyList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllSellerCounties() throws Exception {
        // Initialize the database
        sellerCountyRepository.saveAndFlush(sellerCounty);

        // Get all the sellerCountyList
        restSellerCountyMockMvc.perform(get("/api/seller-counties?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(sellerCounty.getId().intValue())));
    }
    
    @Test
    @Transactional
    public void getSellerCounty() throws Exception {
        // Initialize the database
        sellerCountyRepository.saveAndFlush(sellerCounty);

        // Get the sellerCounty
        restSellerCountyMockMvc.perform(get("/api/seller-counties/{id}", sellerCounty.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(sellerCounty.getId().intValue()));
    }

    @Test
    @Transactional
    public void getNonExistingSellerCounty() throws Exception {
        // Get the sellerCounty
        restSellerCountyMockMvc.perform(get("/api/seller-counties/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSellerCounty() throws Exception {
        // Initialize the database
        sellerCountyRepository.saveAndFlush(sellerCounty);

        int databaseSizeBeforeUpdate = sellerCountyRepository.findAll().size();

        // Update the sellerCounty
        SellerCounty updatedSellerCounty = sellerCountyRepository.findById(sellerCounty.getId()).get();
        // Disconnect from session so that the updates on updatedSellerCounty are not directly saved in db
        em.detach(updatedSellerCounty);
        SellerCountyDTO sellerCountyDTO = sellerCountyMapper.toDto(updatedSellerCounty);

        restSellerCountyMockMvc.perform(put("/api/seller-counties")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(sellerCountyDTO)))
            .andExpect(status().isOk());

        // Validate the SellerCounty in the database
        List<SellerCounty> sellerCountyList = sellerCountyRepository.findAll();
        assertThat(sellerCountyList).hasSize(databaseSizeBeforeUpdate);
        SellerCounty testSellerCounty = sellerCountyList.get(sellerCountyList.size() - 1);
    }

    @Test
    @Transactional
    public void updateNonExistingSellerCounty() throws Exception {
        int databaseSizeBeforeUpdate = sellerCountyRepository.findAll().size();

        // Create the SellerCounty
        SellerCountyDTO sellerCountyDTO = sellerCountyMapper.toDto(sellerCounty);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restSellerCountyMockMvc.perform(put("/api/seller-counties")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(sellerCountyDTO)))
            .andExpect(status().isBadRequest());

        // Validate the SellerCounty in the database
        List<SellerCounty> sellerCountyList = sellerCountyRepository.findAll();
        assertThat(sellerCountyList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteSellerCounty() throws Exception {
        // Initialize the database
        sellerCountyRepository.saveAndFlush(sellerCounty);

        int databaseSizeBeforeDelete = sellerCountyRepository.findAll().size();

        // Delete the sellerCounty
        restSellerCountyMockMvc.perform(delete("/api/seller-counties/{id}", sellerCounty.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<SellerCounty> sellerCountyList = sellerCountyRepository.findAll();
        assertThat(sellerCountyList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(SellerCounty.class);
        SellerCounty sellerCounty1 = new SellerCounty();
        sellerCounty1.setId(1L);
        SellerCounty sellerCounty2 = new SellerCounty();
        sellerCounty2.setId(sellerCounty1.getId());
        assertThat(sellerCounty1).isEqualTo(sellerCounty2);
        sellerCounty2.setId(2L);
        assertThat(sellerCounty1).isNotEqualTo(sellerCounty2);
        sellerCounty1.setId(null);
        assertThat(sellerCounty1).isNotEqualTo(sellerCounty2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(SellerCountyDTO.class);
        SellerCountyDTO sellerCountyDTO1 = new SellerCountyDTO();
        sellerCountyDTO1.setId(1L);
        SellerCountyDTO sellerCountyDTO2 = new SellerCountyDTO();
        assertThat(sellerCountyDTO1).isNotEqualTo(sellerCountyDTO2);
        sellerCountyDTO2.setId(sellerCountyDTO1.getId());
        assertThat(sellerCountyDTO1).isEqualTo(sellerCountyDTO2);
        sellerCountyDTO2.setId(2L);
        assertThat(sellerCountyDTO1).isNotEqualTo(sellerCountyDTO2);
        sellerCountyDTO1.setId(null);
        assertThat(sellerCountyDTO1).isNotEqualTo(sellerCountyDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(sellerCountyMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(sellerCountyMapper.fromId(null)).isNull();
    }
}
