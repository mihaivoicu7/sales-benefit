package com.mvoicu.sales.benefits.web.rest;

import com.mvoicu.sales.benefits.SalesBenefitApp;

import com.mvoicu.sales.benefits.domain.CampaignCounty;
import com.mvoicu.sales.benefits.repository.CampaignCountyRepository;
import com.mvoicu.sales.benefits.service.CampaignCountyService;
import com.mvoicu.sales.benefits.service.dto.CampaignCountyDTO;
import com.mvoicu.sales.benefits.service.mapper.CampaignCountyMapper;
import com.mvoicu.sales.benefits.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;


import static com.mvoicu.sales.benefits.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the CampaignCountyResource REST controller.
 *
 * @see CampaignCountyResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = SalesBenefitApp.class)
public class CampaignCountyResourceIntTest {

    @Autowired
    private CampaignCountyRepository campaignCountyRepository;

    @Autowired
    private CampaignCountyMapper campaignCountyMapper;

    @Autowired
    private CampaignCountyService campaignCountyService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restCampaignCountyMockMvc;

    private CampaignCounty campaignCounty;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final CampaignCountyResource campaignCountyResource = new CampaignCountyResource(campaignCountyService);
        this.restCampaignCountyMockMvc = MockMvcBuilders.standaloneSetup(campaignCountyResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CampaignCounty createEntity(EntityManager em) {
        CampaignCounty campaignCounty = new CampaignCounty();
        return campaignCounty;
    }

    @Before
    public void initTest() {
        campaignCounty = createEntity(em);
    }

    @Test
    @Transactional
    public void createCampaignCounty() throws Exception {
        int databaseSizeBeforeCreate = campaignCountyRepository.findAll().size();

        // Create the CampaignCounty
        CampaignCountyDTO campaignCountyDTO = campaignCountyMapper.toDto(campaignCounty);
        restCampaignCountyMockMvc.perform(post("/api/campaign-counties")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(campaignCountyDTO)))
            .andExpect(status().isCreated());

        // Validate the CampaignCounty in the database
        List<CampaignCounty> campaignCountyList = campaignCountyRepository.findAll();
        assertThat(campaignCountyList).hasSize(databaseSizeBeforeCreate + 1);
        CampaignCounty testCampaignCounty = campaignCountyList.get(campaignCountyList.size() - 1);
    }

    @Test
    @Transactional
    public void createCampaignCountyWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = campaignCountyRepository.findAll().size();

        // Create the CampaignCounty with an existing ID
        campaignCounty.setId(1L);
        CampaignCountyDTO campaignCountyDTO = campaignCountyMapper.toDto(campaignCounty);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCampaignCountyMockMvc.perform(post("/api/campaign-counties")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(campaignCountyDTO)))
            .andExpect(status().isBadRequest());

        // Validate the CampaignCounty in the database
        List<CampaignCounty> campaignCountyList = campaignCountyRepository.findAll();
        assertThat(campaignCountyList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllCampaignCounties() throws Exception {
        // Initialize the database
        campaignCountyRepository.saveAndFlush(campaignCounty);

        // Get all the campaignCountyList
        restCampaignCountyMockMvc.perform(get("/api/campaign-counties?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(campaignCounty.getId().intValue())));
    }
    
    @Test
    @Transactional
    public void getCampaignCounty() throws Exception {
        // Initialize the database
        campaignCountyRepository.saveAndFlush(campaignCounty);

        // Get the campaignCounty
        restCampaignCountyMockMvc.perform(get("/api/campaign-counties/{id}", campaignCounty.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(campaignCounty.getId().intValue()));
    }

    @Test
    @Transactional
    public void getNonExistingCampaignCounty() throws Exception {
        // Get the campaignCounty
        restCampaignCountyMockMvc.perform(get("/api/campaign-counties/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCampaignCounty() throws Exception {
        // Initialize the database
        campaignCountyRepository.saveAndFlush(campaignCounty);

        int databaseSizeBeforeUpdate = campaignCountyRepository.findAll().size();

        // Update the campaignCounty
        CampaignCounty updatedCampaignCounty = campaignCountyRepository.findById(campaignCounty.getId()).get();
        // Disconnect from session so that the updates on updatedCampaignCounty are not directly saved in db
        em.detach(updatedCampaignCounty);
        CampaignCountyDTO campaignCountyDTO = campaignCountyMapper.toDto(updatedCampaignCounty);

        restCampaignCountyMockMvc.perform(put("/api/campaign-counties")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(campaignCountyDTO)))
            .andExpect(status().isOk());

        // Validate the CampaignCounty in the database
        List<CampaignCounty> campaignCountyList = campaignCountyRepository.findAll();
        assertThat(campaignCountyList).hasSize(databaseSizeBeforeUpdate);
        CampaignCounty testCampaignCounty = campaignCountyList.get(campaignCountyList.size() - 1);
    }

    @Test
    @Transactional
    public void updateNonExistingCampaignCounty() throws Exception {
        int databaseSizeBeforeUpdate = campaignCountyRepository.findAll().size();

        // Create the CampaignCounty
        CampaignCountyDTO campaignCountyDTO = campaignCountyMapper.toDto(campaignCounty);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCampaignCountyMockMvc.perform(put("/api/campaign-counties")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(campaignCountyDTO)))
            .andExpect(status().isBadRequest());

        // Validate the CampaignCounty in the database
        List<CampaignCounty> campaignCountyList = campaignCountyRepository.findAll();
        assertThat(campaignCountyList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteCampaignCounty() throws Exception {
        // Initialize the database
        campaignCountyRepository.saveAndFlush(campaignCounty);

        int databaseSizeBeforeDelete = campaignCountyRepository.findAll().size();

        // Delete the campaignCounty
        restCampaignCountyMockMvc.perform(delete("/api/campaign-counties/{id}", campaignCounty.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<CampaignCounty> campaignCountyList = campaignCountyRepository.findAll();
        assertThat(campaignCountyList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(CampaignCounty.class);
        CampaignCounty campaignCounty1 = new CampaignCounty();
        campaignCounty1.setId(1L);
        CampaignCounty campaignCounty2 = new CampaignCounty();
        campaignCounty2.setId(campaignCounty1.getId());
        assertThat(campaignCounty1).isEqualTo(campaignCounty2);
        campaignCounty2.setId(2L);
        assertThat(campaignCounty1).isNotEqualTo(campaignCounty2);
        campaignCounty1.setId(null);
        assertThat(campaignCounty1).isNotEqualTo(campaignCounty2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(CampaignCountyDTO.class);
        CampaignCountyDTO campaignCountyDTO1 = new CampaignCountyDTO();
        campaignCountyDTO1.setId(1L);
        CampaignCountyDTO campaignCountyDTO2 = new CampaignCountyDTO();
        assertThat(campaignCountyDTO1).isNotEqualTo(campaignCountyDTO2);
        campaignCountyDTO2.setId(campaignCountyDTO1.getId());
        assertThat(campaignCountyDTO1).isEqualTo(campaignCountyDTO2);
        campaignCountyDTO2.setId(2L);
        assertThat(campaignCountyDTO1).isNotEqualTo(campaignCountyDTO2);
        campaignCountyDTO1.setId(null);
        assertThat(campaignCountyDTO1).isNotEqualTo(campaignCountyDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(campaignCountyMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(campaignCountyMapper.fromId(null)).isNull();
    }
}
