package com.mvoicu.sales.benefits.web.rest;

import com.mvoicu.sales.benefits.SalesBenefitApp;

import com.mvoicu.sales.benefits.domain.SellerCity;
import com.mvoicu.sales.benefits.repository.SellerCityRepository;
import com.mvoicu.sales.benefits.service.SellerCityService;
import com.mvoicu.sales.benefits.service.dto.SellerCityDTO;
import com.mvoicu.sales.benefits.service.mapper.SellerCityMapper;
import com.mvoicu.sales.benefits.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;


import static com.mvoicu.sales.benefits.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the SellerCityResource REST controller.
 *
 * @see SellerCityResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = SalesBenefitApp.class)
public class SellerCityResourceIntTest {

    @Autowired
    private SellerCityRepository sellerCityRepository;

    @Autowired
    private SellerCityMapper sellerCityMapper;

    @Autowired
    private SellerCityService sellerCityService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restSellerCityMockMvc;

    private SellerCity sellerCity;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final SellerCityResource sellerCityResource = new SellerCityResource(sellerCityService);
        this.restSellerCityMockMvc = MockMvcBuilders.standaloneSetup(sellerCityResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static SellerCity createEntity(EntityManager em) {
        SellerCity sellerCity = new SellerCity();
        return sellerCity;
    }

    @Before
    public void initTest() {
        sellerCity = createEntity(em);
    }

    @Test
    @Transactional
    public void createSellerCity() throws Exception {
        int databaseSizeBeforeCreate = sellerCityRepository.findAll().size();

        // Create the SellerCity
        SellerCityDTO sellerCityDTO = sellerCityMapper.toDto(sellerCity);
        restSellerCityMockMvc.perform(post("/api/seller-cities")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(sellerCityDTO)))
            .andExpect(status().isCreated());

        // Validate the SellerCity in the database
        List<SellerCity> sellerCityList = sellerCityRepository.findAll();
        assertThat(sellerCityList).hasSize(databaseSizeBeforeCreate + 1);
        SellerCity testSellerCity = sellerCityList.get(sellerCityList.size() - 1);
    }

    @Test
    @Transactional
    public void createSellerCityWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = sellerCityRepository.findAll().size();

        // Create the SellerCity with an existing ID
        sellerCity.setId(1L);
        SellerCityDTO sellerCityDTO = sellerCityMapper.toDto(sellerCity);

        // An entity with an existing ID cannot be created, so this API call must fail
        restSellerCityMockMvc.perform(post("/api/seller-cities")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(sellerCityDTO)))
            .andExpect(status().isBadRequest());

        // Validate the SellerCity in the database
        List<SellerCity> sellerCityList = sellerCityRepository.findAll();
        assertThat(sellerCityList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllSellerCities() throws Exception {
        // Initialize the database
        sellerCityRepository.saveAndFlush(sellerCity);

        // Get all the sellerCityList
        restSellerCityMockMvc.perform(get("/api/seller-cities?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(sellerCity.getId().intValue())));
    }
    
    @Test
    @Transactional
    public void getSellerCity() throws Exception {
        // Initialize the database
        sellerCityRepository.saveAndFlush(sellerCity);

        // Get the sellerCity
        restSellerCityMockMvc.perform(get("/api/seller-cities/{id}", sellerCity.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(sellerCity.getId().intValue()));
    }

    @Test
    @Transactional
    public void getNonExistingSellerCity() throws Exception {
        // Get the sellerCity
        restSellerCityMockMvc.perform(get("/api/seller-cities/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSellerCity() throws Exception {
        // Initialize the database
        sellerCityRepository.saveAndFlush(sellerCity);

        int databaseSizeBeforeUpdate = sellerCityRepository.findAll().size();

        // Update the sellerCity
        SellerCity updatedSellerCity = sellerCityRepository.findById(sellerCity.getId()).get();
        // Disconnect from session so that the updates on updatedSellerCity are not directly saved in db
        em.detach(updatedSellerCity);
        SellerCityDTO sellerCityDTO = sellerCityMapper.toDto(updatedSellerCity);

        restSellerCityMockMvc.perform(put("/api/seller-cities")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(sellerCityDTO)))
            .andExpect(status().isOk());

        // Validate the SellerCity in the database
        List<SellerCity> sellerCityList = sellerCityRepository.findAll();
        assertThat(sellerCityList).hasSize(databaseSizeBeforeUpdate);
        SellerCity testSellerCity = sellerCityList.get(sellerCityList.size() - 1);
    }

    @Test
    @Transactional
    public void updateNonExistingSellerCity() throws Exception {
        int databaseSizeBeforeUpdate = sellerCityRepository.findAll().size();

        // Create the SellerCity
        SellerCityDTO sellerCityDTO = sellerCityMapper.toDto(sellerCity);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restSellerCityMockMvc.perform(put("/api/seller-cities")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(sellerCityDTO)))
            .andExpect(status().isBadRequest());

        // Validate the SellerCity in the database
        List<SellerCity> sellerCityList = sellerCityRepository.findAll();
        assertThat(sellerCityList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteSellerCity() throws Exception {
        // Initialize the database
        sellerCityRepository.saveAndFlush(sellerCity);

        int databaseSizeBeforeDelete = sellerCityRepository.findAll().size();

        // Delete the sellerCity
        restSellerCityMockMvc.perform(delete("/api/seller-cities/{id}", sellerCity.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<SellerCity> sellerCityList = sellerCityRepository.findAll();
        assertThat(sellerCityList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(SellerCity.class);
        SellerCity sellerCity1 = new SellerCity();
        sellerCity1.setId(1L);
        SellerCity sellerCity2 = new SellerCity();
        sellerCity2.setId(sellerCity1.getId());
        assertThat(sellerCity1).isEqualTo(sellerCity2);
        sellerCity2.setId(2L);
        assertThat(sellerCity1).isNotEqualTo(sellerCity2);
        sellerCity1.setId(null);
        assertThat(sellerCity1).isNotEqualTo(sellerCity2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(SellerCityDTO.class);
        SellerCityDTO sellerCityDTO1 = new SellerCityDTO();
        sellerCityDTO1.setId(1L);
        SellerCityDTO sellerCityDTO2 = new SellerCityDTO();
        assertThat(sellerCityDTO1).isNotEqualTo(sellerCityDTO2);
        sellerCityDTO2.setId(sellerCityDTO1.getId());
        assertThat(sellerCityDTO1).isEqualTo(sellerCityDTO2);
        sellerCityDTO2.setId(2L);
        assertThat(sellerCityDTO1).isNotEqualTo(sellerCityDTO2);
        sellerCityDTO1.setId(null);
        assertThat(sellerCityDTO1).isNotEqualTo(sellerCityDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(sellerCityMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(sellerCityMapper.fromId(null)).isNull();
    }
}
