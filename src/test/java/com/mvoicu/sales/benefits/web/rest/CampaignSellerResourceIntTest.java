package com.mvoicu.sales.benefits.web.rest;

import com.mvoicu.sales.benefits.SalesBenefitApp;

import com.mvoicu.sales.benefits.domain.CampaignSeller;
import com.mvoicu.sales.benefits.repository.CampaignSellerRepository;
import com.mvoicu.sales.benefits.service.CampaignSellerService;
import com.mvoicu.sales.benefits.service.dto.CampaignSellerDTO;
import com.mvoicu.sales.benefits.service.mapper.CampaignSellerMapper;
import com.mvoicu.sales.benefits.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;


import static com.mvoicu.sales.benefits.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the CampaignSellerResource REST controller.
 *
 * @see CampaignSellerResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = SalesBenefitApp.class)
public class CampaignSellerResourceIntTest {

    @Autowired
    private CampaignSellerRepository campaignSellerRepository;

    @Autowired
    private CampaignSellerMapper campaignSellerMapper;

    @Autowired
    private CampaignSellerService campaignSellerService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restCampaignSellerMockMvc;

    private CampaignSeller campaignSeller;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final CampaignSellerResource campaignSellerResource = new CampaignSellerResource(campaignSellerService);
        this.restCampaignSellerMockMvc = MockMvcBuilders.standaloneSetup(campaignSellerResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CampaignSeller createEntity(EntityManager em) {
        CampaignSeller campaignSeller = new CampaignSeller();
        return campaignSeller;
    }

    @Before
    public void initTest() {
        campaignSeller = createEntity(em);
    }

    @Test
    @Transactional
    public void createCampaignSeller() throws Exception {
        int databaseSizeBeforeCreate = campaignSellerRepository.findAll().size();

        // Create the CampaignSeller
        CampaignSellerDTO campaignSellerDTO = campaignSellerMapper.toDto(campaignSeller);
        restCampaignSellerMockMvc.perform(post("/api/campaign-sellers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(campaignSellerDTO)))
            .andExpect(status().isCreated());

        // Validate the CampaignSeller in the database
        List<CampaignSeller> campaignSellerList = campaignSellerRepository.findAll();
        assertThat(campaignSellerList).hasSize(databaseSizeBeforeCreate + 1);
        CampaignSeller testCampaignSeller = campaignSellerList.get(campaignSellerList.size() - 1);
    }

    @Test
    @Transactional
    public void createCampaignSellerWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = campaignSellerRepository.findAll().size();

        // Create the CampaignSeller with an existing ID
        campaignSeller.setId(1L);
        CampaignSellerDTO campaignSellerDTO = campaignSellerMapper.toDto(campaignSeller);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCampaignSellerMockMvc.perform(post("/api/campaign-sellers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(campaignSellerDTO)))
            .andExpect(status().isBadRequest());

        // Validate the CampaignSeller in the database
        List<CampaignSeller> campaignSellerList = campaignSellerRepository.findAll();
        assertThat(campaignSellerList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllCampaignSellers() throws Exception {
        // Initialize the database
        campaignSellerRepository.saveAndFlush(campaignSeller);

        // Get all the campaignSellerList
        restCampaignSellerMockMvc.perform(get("/api/campaign-sellers?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(campaignSeller.getId().intValue())));
    }
    
    @Test
    @Transactional
    public void getCampaignSeller() throws Exception {
        // Initialize the database
        campaignSellerRepository.saveAndFlush(campaignSeller);

        // Get the campaignSeller
        restCampaignSellerMockMvc.perform(get("/api/campaign-sellers/{id}", campaignSeller.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(campaignSeller.getId().intValue()));
    }

    @Test
    @Transactional
    public void getNonExistingCampaignSeller() throws Exception {
        // Get the campaignSeller
        restCampaignSellerMockMvc.perform(get("/api/campaign-sellers/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCampaignSeller() throws Exception {
        // Initialize the database
        campaignSellerRepository.saveAndFlush(campaignSeller);

        int databaseSizeBeforeUpdate = campaignSellerRepository.findAll().size();

        // Update the campaignSeller
        CampaignSeller updatedCampaignSeller = campaignSellerRepository.findById(campaignSeller.getId()).get();
        // Disconnect from session so that the updates on updatedCampaignSeller are not directly saved in db
        em.detach(updatedCampaignSeller);
        CampaignSellerDTO campaignSellerDTO = campaignSellerMapper.toDto(updatedCampaignSeller);

        restCampaignSellerMockMvc.perform(put("/api/campaign-sellers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(campaignSellerDTO)))
            .andExpect(status().isOk());

        // Validate the CampaignSeller in the database
        List<CampaignSeller> campaignSellerList = campaignSellerRepository.findAll();
        assertThat(campaignSellerList).hasSize(databaseSizeBeforeUpdate);
        CampaignSeller testCampaignSeller = campaignSellerList.get(campaignSellerList.size() - 1);
    }

    @Test
    @Transactional
    public void updateNonExistingCampaignSeller() throws Exception {
        int databaseSizeBeforeUpdate = campaignSellerRepository.findAll().size();

        // Create the CampaignSeller
        CampaignSellerDTO campaignSellerDTO = campaignSellerMapper.toDto(campaignSeller);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCampaignSellerMockMvc.perform(put("/api/campaign-sellers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(campaignSellerDTO)))
            .andExpect(status().isBadRequest());

        // Validate the CampaignSeller in the database
        List<CampaignSeller> campaignSellerList = campaignSellerRepository.findAll();
        assertThat(campaignSellerList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteCampaignSeller() throws Exception {
        // Initialize the database
        campaignSellerRepository.saveAndFlush(campaignSeller);

        int databaseSizeBeforeDelete = campaignSellerRepository.findAll().size();

        // Delete the campaignSeller
        restCampaignSellerMockMvc.perform(delete("/api/campaign-sellers/{id}", campaignSeller.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<CampaignSeller> campaignSellerList = campaignSellerRepository.findAll();
        assertThat(campaignSellerList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(CampaignSeller.class);
        CampaignSeller campaignSeller1 = new CampaignSeller();
        campaignSeller1.setId(1L);
        CampaignSeller campaignSeller2 = new CampaignSeller();
        campaignSeller2.setId(campaignSeller1.getId());
        assertThat(campaignSeller1).isEqualTo(campaignSeller2);
        campaignSeller2.setId(2L);
        assertThat(campaignSeller1).isNotEqualTo(campaignSeller2);
        campaignSeller1.setId(null);
        assertThat(campaignSeller1).isNotEqualTo(campaignSeller2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(CampaignSellerDTO.class);
        CampaignSellerDTO campaignSellerDTO1 = new CampaignSellerDTO();
        campaignSellerDTO1.setId(1L);
        CampaignSellerDTO campaignSellerDTO2 = new CampaignSellerDTO();
        assertThat(campaignSellerDTO1).isNotEqualTo(campaignSellerDTO2);
        campaignSellerDTO2.setId(campaignSellerDTO1.getId());
        assertThat(campaignSellerDTO1).isEqualTo(campaignSellerDTO2);
        campaignSellerDTO2.setId(2L);
        assertThat(campaignSellerDTO1).isNotEqualTo(campaignSellerDTO2);
        campaignSellerDTO1.setId(null);
        assertThat(campaignSellerDTO1).isNotEqualTo(campaignSellerDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(campaignSellerMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(campaignSellerMapper.fromId(null)).isNull();
    }
}
