package com.mvoicu.sales.benefits.web.rest;

import com.mvoicu.sales.benefits.SalesBenefitApp;

import com.mvoicu.sales.benefits.domain.SellerSupplierRelation;
import com.mvoicu.sales.benefits.repository.SellerSupplierRelationRepository;
import com.mvoicu.sales.benefits.service.SellerSupplierRelationService;
import com.mvoicu.sales.benefits.service.dto.SellerSupplierRelationDTO;
import com.mvoicu.sales.benefits.service.mapper.SellerSupplierRelationMapper;
import com.mvoicu.sales.benefits.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;


import static com.mvoicu.sales.benefits.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the SellerSupplierRelationResource REST controller.
 *
 * @see SellerSupplierRelationResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = SalesBenefitApp.class)
public class SellerSupplierRelationResourceIntTest {

    @Autowired
    private SellerSupplierRelationRepository sellerSupplierRelationRepository;

    @Autowired
    private SellerSupplierRelationMapper sellerSupplierRelationMapper;

    @Autowired
    private SellerSupplierRelationService sellerSupplierRelationService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restSellerSupplierRelationMockMvc;

    private SellerSupplierRelation sellerSupplierRelation;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final SellerSupplierRelationResource sellerSupplierRelationResource = new SellerSupplierRelationResource(sellerSupplierRelationService);
        this.restSellerSupplierRelationMockMvc = MockMvcBuilders.standaloneSetup(sellerSupplierRelationResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static SellerSupplierRelation createEntity(EntityManager em) {
        SellerSupplierRelation sellerSupplierRelation = new SellerSupplierRelation();
        return sellerSupplierRelation;
    }

    @Before
    public void initTest() {
        sellerSupplierRelation = createEntity(em);
    }

    @Test
    @Transactional
    public void createSellerSupplierRelation() throws Exception {
        int databaseSizeBeforeCreate = sellerSupplierRelationRepository.findAll().size();

        // Create the SellerSupplierRelation
        SellerSupplierRelationDTO sellerSupplierRelationDTO = sellerSupplierRelationMapper.toDto(sellerSupplierRelation);
        restSellerSupplierRelationMockMvc.perform(post("/api/seller-supplier-relations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(sellerSupplierRelationDTO)))
            .andExpect(status().isCreated());

        // Validate the SellerSupplierRelation in the database
        List<SellerSupplierRelation> sellerSupplierRelationList = sellerSupplierRelationRepository.findAll();
        assertThat(sellerSupplierRelationList).hasSize(databaseSizeBeforeCreate + 1);
        SellerSupplierRelation testSellerSupplierRelation = sellerSupplierRelationList.get(sellerSupplierRelationList.size() - 1);
    }

    @Test
    @Transactional
    public void createSellerSupplierRelationWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = sellerSupplierRelationRepository.findAll().size();

        // Create the SellerSupplierRelation with an existing ID
        sellerSupplierRelation.setId(1L);
        SellerSupplierRelationDTO sellerSupplierRelationDTO = sellerSupplierRelationMapper.toDto(sellerSupplierRelation);

        // An entity with an existing ID cannot be created, so this API call must fail
        restSellerSupplierRelationMockMvc.perform(post("/api/seller-supplier-relations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(sellerSupplierRelationDTO)))
            .andExpect(status().isBadRequest());

        // Validate the SellerSupplierRelation in the database
        List<SellerSupplierRelation> sellerSupplierRelationList = sellerSupplierRelationRepository.findAll();
        assertThat(sellerSupplierRelationList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllSellerSupplierRelations() throws Exception {
        // Initialize the database
        sellerSupplierRelationRepository.saveAndFlush(sellerSupplierRelation);

        // Get all the sellerSupplierRelationList
        restSellerSupplierRelationMockMvc.perform(get("/api/seller-supplier-relations?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(sellerSupplierRelation.getId().intValue())));
    }
    
    @Test
    @Transactional
    public void getSellerSupplierRelation() throws Exception {
        // Initialize the database
        sellerSupplierRelationRepository.saveAndFlush(sellerSupplierRelation);

        // Get the sellerSupplierRelation
        restSellerSupplierRelationMockMvc.perform(get("/api/seller-supplier-relations/{id}", sellerSupplierRelation.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(sellerSupplierRelation.getId().intValue()));
    }

    @Test
    @Transactional
    public void getNonExistingSellerSupplierRelation() throws Exception {
        // Get the sellerSupplierRelation
        restSellerSupplierRelationMockMvc.perform(get("/api/seller-supplier-relations/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSellerSupplierRelation() throws Exception {
        // Initialize the database
        sellerSupplierRelationRepository.saveAndFlush(sellerSupplierRelation);

        int databaseSizeBeforeUpdate = sellerSupplierRelationRepository.findAll().size();

        // Update the sellerSupplierRelation
        SellerSupplierRelation updatedSellerSupplierRelation = sellerSupplierRelationRepository.findById(sellerSupplierRelation.getId()).get();
        // Disconnect from session so that the updates on updatedSellerSupplierRelation are not directly saved in db
        em.detach(updatedSellerSupplierRelation);
        SellerSupplierRelationDTO sellerSupplierRelationDTO = sellerSupplierRelationMapper.toDto(updatedSellerSupplierRelation);

        restSellerSupplierRelationMockMvc.perform(put("/api/seller-supplier-relations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(sellerSupplierRelationDTO)))
            .andExpect(status().isOk());

        // Validate the SellerSupplierRelation in the database
        List<SellerSupplierRelation> sellerSupplierRelationList = sellerSupplierRelationRepository.findAll();
        assertThat(sellerSupplierRelationList).hasSize(databaseSizeBeforeUpdate);
        SellerSupplierRelation testSellerSupplierRelation = sellerSupplierRelationList.get(sellerSupplierRelationList.size() - 1);
    }

    @Test
    @Transactional
    public void updateNonExistingSellerSupplierRelation() throws Exception {
        int databaseSizeBeforeUpdate = sellerSupplierRelationRepository.findAll().size();

        // Create the SellerSupplierRelation
        SellerSupplierRelationDTO sellerSupplierRelationDTO = sellerSupplierRelationMapper.toDto(sellerSupplierRelation);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restSellerSupplierRelationMockMvc.perform(put("/api/seller-supplier-relations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(sellerSupplierRelationDTO)))
            .andExpect(status().isBadRequest());

        // Validate the SellerSupplierRelation in the database
        List<SellerSupplierRelation> sellerSupplierRelationList = sellerSupplierRelationRepository.findAll();
        assertThat(sellerSupplierRelationList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteSellerSupplierRelation() throws Exception {
        // Initialize the database
        sellerSupplierRelationRepository.saveAndFlush(sellerSupplierRelation);

        int databaseSizeBeforeDelete = sellerSupplierRelationRepository.findAll().size();

        // Delete the sellerSupplierRelation
        restSellerSupplierRelationMockMvc.perform(delete("/api/seller-supplier-relations/{id}", sellerSupplierRelation.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<SellerSupplierRelation> sellerSupplierRelationList = sellerSupplierRelationRepository.findAll();
        assertThat(sellerSupplierRelationList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(SellerSupplierRelation.class);
        SellerSupplierRelation sellerSupplierRelation1 = new SellerSupplierRelation();
        sellerSupplierRelation1.setId(1L);
        SellerSupplierRelation sellerSupplierRelation2 = new SellerSupplierRelation();
        sellerSupplierRelation2.setId(sellerSupplierRelation1.getId());
        assertThat(sellerSupplierRelation1).isEqualTo(sellerSupplierRelation2);
        sellerSupplierRelation2.setId(2L);
        assertThat(sellerSupplierRelation1).isNotEqualTo(sellerSupplierRelation2);
        sellerSupplierRelation1.setId(null);
        assertThat(sellerSupplierRelation1).isNotEqualTo(sellerSupplierRelation2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(SellerSupplierRelationDTO.class);
        SellerSupplierRelationDTO sellerSupplierRelationDTO1 = new SellerSupplierRelationDTO();
        sellerSupplierRelationDTO1.setId(1L);
        SellerSupplierRelationDTO sellerSupplierRelationDTO2 = new SellerSupplierRelationDTO();
        assertThat(sellerSupplierRelationDTO1).isNotEqualTo(sellerSupplierRelationDTO2);
        sellerSupplierRelationDTO2.setId(sellerSupplierRelationDTO1.getId());
        assertThat(sellerSupplierRelationDTO1).isEqualTo(sellerSupplierRelationDTO2);
        sellerSupplierRelationDTO2.setId(2L);
        assertThat(sellerSupplierRelationDTO1).isNotEqualTo(sellerSupplierRelationDTO2);
        sellerSupplierRelationDTO1.setId(null);
        assertThat(sellerSupplierRelationDTO1).isNotEqualTo(sellerSupplierRelationDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(sellerSupplierRelationMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(sellerSupplierRelationMapper.fromId(null)).isNull();
    }
}
