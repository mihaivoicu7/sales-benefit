package com.mvoicu.sales.benefits.web.rest;

import com.mvoicu.sales.benefits.SalesBenefitApp;

import com.mvoicu.sales.benefits.domain.CampaignCity;
import com.mvoicu.sales.benefits.repository.CampaignCityRepository;
import com.mvoicu.sales.benefits.service.CampaignCityService;
import com.mvoicu.sales.benefits.service.dto.CampaignCityDTO;
import com.mvoicu.sales.benefits.service.mapper.CampaignCityMapper;
import com.mvoicu.sales.benefits.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;


import static com.mvoicu.sales.benefits.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the CampaignCityResource REST controller.
 *
 * @see CampaignCityResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = SalesBenefitApp.class)
public class CampaignCityResourceIntTest {

    @Autowired
    private CampaignCityRepository campaignCityRepository;

    @Autowired
    private CampaignCityMapper campaignCityMapper;

    @Autowired
    private CampaignCityService campaignCityService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restCampaignCityMockMvc;

    private CampaignCity campaignCity;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final CampaignCityResource campaignCityResource = new CampaignCityResource(campaignCityService);
        this.restCampaignCityMockMvc = MockMvcBuilders.standaloneSetup(campaignCityResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CampaignCity createEntity(EntityManager em) {
        CampaignCity campaignCity = new CampaignCity();
        return campaignCity;
    }

    @Before
    public void initTest() {
        campaignCity = createEntity(em);
    }

    @Test
    @Transactional
    public void createCampaignCity() throws Exception {
        int databaseSizeBeforeCreate = campaignCityRepository.findAll().size();

        // Create the CampaignCity
        CampaignCityDTO campaignCityDTO = campaignCityMapper.toDto(campaignCity);
        restCampaignCityMockMvc.perform(post("/api/campaign-cities")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(campaignCityDTO)))
            .andExpect(status().isCreated());

        // Validate the CampaignCity in the database
        List<CampaignCity> campaignCityList = campaignCityRepository.findAll();
        assertThat(campaignCityList).hasSize(databaseSizeBeforeCreate + 1);
        CampaignCity testCampaignCity = campaignCityList.get(campaignCityList.size() - 1);
    }

    @Test
    @Transactional
    public void createCampaignCityWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = campaignCityRepository.findAll().size();

        // Create the CampaignCity with an existing ID
        campaignCity.setId(1L);
        CampaignCityDTO campaignCityDTO = campaignCityMapper.toDto(campaignCity);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCampaignCityMockMvc.perform(post("/api/campaign-cities")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(campaignCityDTO)))
            .andExpect(status().isBadRequest());

        // Validate the CampaignCity in the database
        List<CampaignCity> campaignCityList = campaignCityRepository.findAll();
        assertThat(campaignCityList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllCampaignCities() throws Exception {
        // Initialize the database
        campaignCityRepository.saveAndFlush(campaignCity);

        // Get all the campaignCityList
        restCampaignCityMockMvc.perform(get("/api/campaign-cities?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(campaignCity.getId().intValue())));
    }
    
    @Test
    @Transactional
    public void getCampaignCity() throws Exception {
        // Initialize the database
        campaignCityRepository.saveAndFlush(campaignCity);

        // Get the campaignCity
        restCampaignCityMockMvc.perform(get("/api/campaign-cities/{id}", campaignCity.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(campaignCity.getId().intValue()));
    }

    @Test
    @Transactional
    public void getNonExistingCampaignCity() throws Exception {
        // Get the campaignCity
        restCampaignCityMockMvc.perform(get("/api/campaign-cities/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCampaignCity() throws Exception {
        // Initialize the database
        campaignCityRepository.saveAndFlush(campaignCity);

        int databaseSizeBeforeUpdate = campaignCityRepository.findAll().size();

        // Update the campaignCity
        CampaignCity updatedCampaignCity = campaignCityRepository.findById(campaignCity.getId()).get();
        // Disconnect from session so that the updates on updatedCampaignCity are not directly saved in db
        em.detach(updatedCampaignCity);
        CampaignCityDTO campaignCityDTO = campaignCityMapper.toDto(updatedCampaignCity);

        restCampaignCityMockMvc.perform(put("/api/campaign-cities")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(campaignCityDTO)))
            .andExpect(status().isOk());

        // Validate the CampaignCity in the database
        List<CampaignCity> campaignCityList = campaignCityRepository.findAll();
        assertThat(campaignCityList).hasSize(databaseSizeBeforeUpdate);
        CampaignCity testCampaignCity = campaignCityList.get(campaignCityList.size() - 1);
    }

    @Test
    @Transactional
    public void updateNonExistingCampaignCity() throws Exception {
        int databaseSizeBeforeUpdate = campaignCityRepository.findAll().size();

        // Create the CampaignCity
        CampaignCityDTO campaignCityDTO = campaignCityMapper.toDto(campaignCity);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCampaignCityMockMvc.perform(put("/api/campaign-cities")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(campaignCityDTO)))
            .andExpect(status().isBadRequest());

        // Validate the CampaignCity in the database
        List<CampaignCity> campaignCityList = campaignCityRepository.findAll();
        assertThat(campaignCityList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteCampaignCity() throws Exception {
        // Initialize the database
        campaignCityRepository.saveAndFlush(campaignCity);

        int databaseSizeBeforeDelete = campaignCityRepository.findAll().size();

        // Delete the campaignCity
        restCampaignCityMockMvc.perform(delete("/api/campaign-cities/{id}", campaignCity.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<CampaignCity> campaignCityList = campaignCityRepository.findAll();
        assertThat(campaignCityList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(CampaignCity.class);
        CampaignCity campaignCity1 = new CampaignCity();
        campaignCity1.setId(1L);
        CampaignCity campaignCity2 = new CampaignCity();
        campaignCity2.setId(campaignCity1.getId());
        assertThat(campaignCity1).isEqualTo(campaignCity2);
        campaignCity2.setId(2L);
        assertThat(campaignCity1).isNotEqualTo(campaignCity2);
        campaignCity1.setId(null);
        assertThat(campaignCity1).isNotEqualTo(campaignCity2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(CampaignCityDTO.class);
        CampaignCityDTO campaignCityDTO1 = new CampaignCityDTO();
        campaignCityDTO1.setId(1L);
        CampaignCityDTO campaignCityDTO2 = new CampaignCityDTO();
        assertThat(campaignCityDTO1).isNotEqualTo(campaignCityDTO2);
        campaignCityDTO2.setId(campaignCityDTO1.getId());
        assertThat(campaignCityDTO1).isEqualTo(campaignCityDTO2);
        campaignCityDTO2.setId(2L);
        assertThat(campaignCityDTO1).isNotEqualTo(campaignCityDTO2);
        campaignCityDTO1.setId(null);
        assertThat(campaignCityDTO1).isNotEqualTo(campaignCityDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(campaignCityMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(campaignCityMapper.fromId(null)).isNull();
    }
}
